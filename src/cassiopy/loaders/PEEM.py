"""
PEEM.py

Ce fichier contient les ébauches de routines qui permettent d'analyser les fichiers PEEM
generes a NANOESCA. La portee finale du projet est de creer une biblotheque de fonction generique 
pour traiter les donnees automatiquement.
"""

import re
import sys
import os

from PIL import Image

import datetime as dt

from scipy.special import erf, erfcinv
from scipy.optimize import curve_fit, OptimizeWarning
from scipy.ndimage import gaussian_filter
from scipy.signal import find_peaks
from scipy.spatial import ConvexHull

from scipy.interpolate import RegularGridInterpolator as RGI

import numpy as np
import pandas as pd

m_e = 0.51099895000 #MeV/c^2
h_bar_c = 197.3269805 #MeV.fm
ARPES_CONST = np.sqrt(2*1e6*m_e)/(h_bar_c*10)

#----------------------------------------String management

def truncate(f, n):
    """Truncates/pads a float f to n decimal places without rounding

    Parameters:
    	f (float):
    		Number to be truncated
    	n (int):
    		Number of decimals

    Returns:
    	(str):
    		Truncated number as a string

    Examples:
    	```
    	truncate(10.00134259353, 4)
    	>'10.0013'
    	```
    """
    s = '{}'.format(f)
    if 'e' in s or 'E' in s:
        return '{0:.{1}f}'.format(f, n)
    i, p, d = s.partition('.')
    return '.'.join([i, (d+'0'*n)[:n]])

#----------------------------------------File loading related

rule1 = re.compile('(\d+)_(\S+)_(-?\d+.?\d*)_(\d+)',re.I)
rule2 = re.compile('([a-z]+)_(\-?\d*\.?\d*)', re.I)
errors = re.compile('\t\d')

def list_singles(dir, ext):
	"""
	Lists all the files in a directory

	Parameters:
		dir (string):
			path of a directory
		ext (string):
			extension of the researched files

	Returns:
		filelist (dataframe):
			list of filenames (not filepaths) matching the extension

	"""
	expr0 = re.compile('\.'+ext, re.I)
	filelist = sorted(os.listdir(dir))
	filelist = np.array(filelist)
	#print(filelist)
	return pd.DataFrame(filelist[[bool(expr0.search(i)) for i in filelist]])

def order_list(rule, filelist, index):
	"""
	Function that gives an order to the files found

	Parameters:
		rule (regular expression):
			a regular expression that models the rule of naming of the filename
		filelist (list | array | dataframe):
			array containing filenames
		index (int): 
			index of the ordering feature to be used
	Returns :
		newfilelist (list):
			list containing ordered names
	"""
	toorder = []
	for i in filelist[0] :
		res = rule.findall(i)
		res = {i[0]:float(i[1]) for i in res}
		toorder.append(res[index])

	ordered = np.sort(list(set(toorder)))
	#print(ordered)
	
	newfilelist = []
	for i in ordered :
		for j in filelist[0] :
			res = rule.findall(j)
			res = {i[0]:float(i[1]) for i in res}
			if float(res[index]) == i:
				if 'idx' in res.keys():
					if res['idx'] == 0:
						newfilelist.append([j])
					else:
						newfilelist[-1].append(j)
				else:
					newfilelist.append([j])
	return newfilelist

def parse_mesoxscope(path, infofile):
	"""


	"""
	print(path+infofile)
	content = pd.read_csv(path+infofile)

	metadata = {'MEASUREMENT_NAME':None,'AXIS_START':None,'AXIS_STOP':None,'AXIS_STEP':None}
	AXIS_START = None
	AXIS_STOP = None
	AXIS_STEP = None
	MEASUREMENT_NAME = None

	for i in content['[HEAD]']:
		item = re.match('(.+) = (.+)',i)
		if item:
			if item[1] in metadata.keys():
				try:
					metadata[item[1]] = float(item[2])
				except:
					metadata[item[1]] = item[2].replace('\"','')
	return metadata

def open_filelist(path, filelist):
	"""
	Loads all the files in filelist

	Parameters:
		path (string):
			path of the files
		filelist (list):
			names of the files
	Returns:
		stack (list)
			list containing all pictures in filelist
	"""
	stack = []
	for liste in filelist:
		current = np.zeros([600,600])
		if type(liste) == str:
			current = np.array(Image.open(path+liste))
		else:
			for i in liste:
				current += np.array(Image.open(path+i))/len(liste)
		#for i in liste[1:]:
		#		current += np.array(Image.open(path+i))/len(liste)
		#print(current)
		stack.append(current)
	return stack

def correct_flatfield(stack, flatfield, mask):
	"""
	Corrects a stack with a flatfield

	Parameters:
		stack (list):
			list containing all pictures
		flatfield (ndarray):
			2D array flatfield of the detector
		mask (boolean array):
			2D array mask of the unwanted regions

	Returns:
		corrected_stack (array):
			stack with flatfield removed
	"""
	corrected_stack = [i*mask/flatfield for i in stack]
	count = 0
	for i in corrected_stack :
		#plt.imshow(i)
		try:
			#os.makedirs('./corrected_test/')
			pass
		except FileExistsError:
			pass
		#plt.savefig('./corrected_test/'+str(count))
		#plt.close()
		count +=1
	count = 0
	for i in stack :
		#plt.imshow(i)
		try:
			#os.makedirs('./pristine_test/')
			pass
		except FileExistsError:
			pass
		#plt.savefig('./pristine_test/'+str(count))
		#plt.close()
		count +=1
	return np.array(corrected_stack)

def generate_spectra(imstack):
	"""
	Rotates the stack to get axis

	Parameters:
		imstack (array):
			image stack
	Returns:
		spectral_stack (array):
			stack of spectrum with energy in absciss
	"""
	return np.transpose(np.array([[np.sum(i[j]) for j in range(i.shape[0])] for i in imstack]))

def load_file(path, flat_field, mask, **kwargs):
	"""
	Load all the files in path, applying, flat_field and masking.
	Auto detects if data was measured at NanoESCA or MesoXScope.

	Parameters:
		path (string):
			path of the files

		flat_field (ndarray):
			2D array flatfield of the detector
		
		mask (boolean array):
			2D array mask of the unwanted regions

		mode (string):
			loading type : 'XPEEM' or 'KPEEM'

	Returns:
		corrected_stack (ndarray):
			loaded images
		E (array):
			Energy axis (eV)
		axes (list):
			list containing the two axes of the images

	"""
	print('----------Loading data at '+str(path))

	if 'mode' in kwargs.keys():
		if kwargs['mode'] == 'XPEEM':
			K_PX = 1
		else :
			K_PX = 7.57*10**(-3)
	else:
		K_PX = 7.57*10**(-3)

	BEAMLINES = {True:'NanoESCA', False:'MesoXscope'}
	
	filelist = list_singles(path, 'tif')

	infofilelist = list_singles(path, 'tab')
	
	if len(infofilelist) == 0:
		infofilelist = list_singles(path, 'dat')
		#print(infofilelist)

		metadata = parse_mesoxscope(path, infofilelist[0][0])
		E = np.arange(metadata['AXIS_START'],
					  metadata['AXIS_STOP']+metadata['AXIS_STEP'],
					  metadata['AXIS_STEP'])

		newfilelist = np.array(filelist)

		BEAMLINE = BEAMLINES[False]

	else :
		#print(infofilelist)
		newfilelist = np.array(order_list(rule2, filelist, 'E'))
		E = np.flip(pd.read_csv(path+infofilelist[0][0], skiprows = 1).to_numpy().T[0])
		#print(E)
		if E[0]>E[1]:
			E = np.flip(E)
		if type(E[0]) == type(''):
			E = np.array([ float(errors.sub('',i)) for i in E])

		BEAMLINE = BEAMLINES[True]

	#print(filelist)

	print(pd.DataFrame(newfilelist))
	
	original = open_filelist(path, newfilelist)
	corrected = correct_flatfield(original, flat_field, mask)
	PE_spectra = generate_spectra(corrected)

	print('Idx averaged')
	print('Flatfield correction applied')

	X,Y = np.meshgrid(np.arange(-corrected.shape[1]/2,corrected.shape[1]/2,1),
					  np.arange(-corrected.shape[2]/2,corrected.shape[2]/2,1))

	kX = X*K_PX
	kY = Y*K_PX

	#print(corrected.shape)
	#print(E.shape)

	L_E = min(len(E), len(corrected[0]))

	return corrected[:L_E], E[:L_E], [kX, kY]

#----------------------------------------Shape geometry related

def create_mask(shape, center, radius) :
	"""
	Create a circular mask

	Parameters:
		shape (list):
			list of two elements
		center (list):
			coordinates of the center [x_c,y_c]
		radius (float):
			radius of the circle

	Returns:
		mask (2D boolean array):
			circular boolean mask
	"""
	return np.array([[True if (i-center[0])**2 + (j-center[1])**2 < radius**2 else np.nan for i in range(shape[0])] for j in range(shape[1])])

def rect_mask_1D(lims, grid):
	"""
	Create a stripe mask in one dimension

	Parameters:
		lims (list):
			boundaries of the mask
		grid (ndarray):
			2D array containing the coordinates of the points in the grid

	Returns:
		mask (2D boolean array):
			rectangular boolean mask
	"""
	return (grid>min(lims))*(grid<max(lims))

def rect_mask(lims, grid):
	"""
	Create a rectangular mask in one dimension

	Parameters:
		lims (list):
			boundaries of the mask with syntax: [[lim_x_1, lim_x_2],[lim_y_1, lim_y_2]]

		grid (ndarray):
			2D array containing the coordinates of the points in the grid

	Returns:
		mask (2D boolean array):
			rectangular boolean mask
	"""
	mask1 = rect_mask_1D(lims[0], grid[0])
	mask2 = rect_mask_1D(lims[1], grid[1])
	return mask1*mask2
