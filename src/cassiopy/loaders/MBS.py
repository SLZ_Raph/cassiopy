"""
MBS.py
"""

try:
    from krxloader import open_krx
except Exception as e: 
    print('\'MBS.py\' error, will not be able to load .krx files',e)
import numpy as np
import os
import re
import itertools

def check_labels(label_list, reference_list):
    return all([i in reference_list for i in label_list])

def parse_header(header):
    """
    Parse the header file obtained from the krx

    Parameters:
        header (string) :
            Header containing metadata

    Returns:
        extracted_meta (dict):
            Metadata contained in the header with proper types applied (string/float)

    """
    expr = re.compile('([a-z .0-9#]+)\t(.+)\r?',re.I)
    metadata = {}
    extracted_meta = dict(expr.findall(header))
    #print(extracted_meta.keys())
    for key in extracted_meta:
        if re.match('-?[0-9]+\.[0-9]+\r?$', extracted_meta[key]):
            extracted_meta[key] = float(extracted_meta[key])
        elif re.match('-?[0-9]+\r?$', extracted_meta[key]):
            extracted_meta[key] = int(extracted_meta[key])
        #print(str(key)+'\t'+str(extracted_meta[key]))
    return extracted_meta

def build_axes(metadata):
    """
    Creates the axes of the file from the content in metadata

    Parameters:
        metadata (dict):
            metadata of the file

    Returns:
        axes (dict):
            dictionary containing the main axes (array) available from the data
            with keys: KE, ANGLE, X, Y, SPIN

    Notes:
        Because of the number of digits in the values from the metadata,
        the length of the axes might be wrong of one or two units at this stage.
        The axes need to be corrected with correct_axes afterwards.
    """

    KE =[0]                 #Kinetic energy axis
    X = [0]                 #Deflector X axis
    Y = [0]                 #Deflector Y axis
    ANGLE = [0]             #Angular range of the detector (along Y)
    SPIN = []               #Spin components measured

    N_KE = (metadata['End K.E.']-metadata['Start K.E.'])//metadata['Step Size']
    KE = np.linspace(metadata['Start K.E.'], 
                     metadata['End K.E.'], 
                     int(N_KE))
    SPIN = [re.findall('[+-][XYZ]', metadata[i]) for i in metadata if 'SpinComp' in i]

    if check_labels(('MapStartX','MapEndX', 'MapNoXSteps'), metadata.keys()):
        X = np.linspace(metadata['MapStartX'], 
                        metadata['MapEndX'], 
                        metadata['MapNoXSteps'])

    if check_labels(('MapStartY','MapEndY', 'MapNoYSteps'), metadata.keys()):
        Y = np.linspace(metadata['MapStartY'], 
                        metadata['MapEndY'], 
                        metadata['MapNoYSteps'])

    if check_labels(('ScaleMin','ScaleMax','ScaleMult'), metadata.keys()):

        N_ANGLE = (metadata['ScaleMax']-metadata['ScaleMin'])//metadata['ScaleMult']
        ANGLE = np.linspace(metadata['ScaleMin'], 
                            metadata['ScaleMax'], 
                            int(N_ANGLE))

    # if 'ScaleMult' in metadata.keys() and any(X != [0]) and any(Y != [0]):
    #   X = X*metadata['ScaleMult']
    #   Y = Y*metadata['ScaleMult']

    axes = {'KE':KE,'ANGLE':ANGLE, 'X':X, 'Y':Y, 'SPIN':SPIN}
    
    if 'MapCoordinate' in metadata.keys():
        axes['TYPE'] = metadata['MapCoordinate']

    return axes

def check_integrity(metadata, data, axes):
    """
    Shows errors if the length of axes and data do not match

    Parameters:
        metadata (dict):
            metadata from the header
        data (array):
            flattened data obtained from the file
        axes (dict):
            axes reconstructed with build_axes
    """

    try:
        if metadata['No Scans'] != 0: 
            N_images = metadata['MapNoXSteps']*metadata['MapNoYSteps']*metadata['No Scans']
        else :
            N_images = metadata['MapNoXSteps']*metadata['MapNoYSteps']


        if len(data)/(metadata['MapNoXSteps']*metadata['MapNoYSteps']) != len(axes['SPIN']):
            print('Problem with data : number of spin components')
            print('\tNo Scans : {} , No Spin : {}'.format(len(data)/(metadata['MapNoXSteps']*metadata['MapNoXSteps']*metadata['MapNoYSteps']),len(axes['SPIN'])))

    except KeyError:
        N_images = 'Undefined'

        if len(data) != N_images:
            print('Problem with data : len(data) != N_images')
            print('\tlen(data) : {} , Calculated length : {}'.format(len(data), N_images))

        if metadata['NoS'] != len(axes['ANGLE']):
            print('Problem with angular axis')
            print('\tNoS : {} , len(angle) : {}'.format(metadata['NoS'], len(axes['ANGLE'])))

#def check_integrity(metadata, data, axes, sizes):
#       pass

def check_meta(metadata, data, axes, sizes):
    """
    Displays the extracted metadata vs the reconstructed axes

    Parameters:
        metadata (dict):
            metadata from the header
        data (array):
            flattened data obtained from the file
        axes (dict):
            axes reconstructed with build_axes
        sizes (dict):
            lengths of the axes

    """
    print('______DATA______')

    print('Data length : {}'.format(len(data)))

    print('_____SIZES______')
    for i in sizes:
        print('{} : {}'.format(i,sizes[i]))

    print('____METADATA____')
    print('NoS : {}'.format(metadata['NoS']))
    print('TotSteps : {}'.format(metadata['TotSteps']))
    print('No. Steps : {}'.format(metadata['No. Steps']))
    try :
        print('MapNoXSteps : {}'.format(metadata['MapNoXSteps']))
        print('MapNoYSteps : {}'.format(metadata['MapNoYSteps']))
    except KeyError:
        pass

    print('______AXES______')
    for i in axes :
        print(('{} : {}').format(i, np.shape(axes[i])))

def correct_axes(axes, sizes):
    """
    Corrects the extracted metadata vs the reconstructed axes

    Parameters:
        metadata (dict):
            metadata from the header
        data (array):
            flattened data obtained from the file
        axes (dict):
            axes reconstructed with build_axes
        sizes (dict):
            lengths of the axes

    Returns:
        axes (dict):
            axes reconstructed with proper length
    
    Notes:
        Each axe in axes is compared to all sizes.
        If the absolute length difference them falls into the threshold,
        the axe is reformed to match the size.

    """
    threshold = 5

    for j in axes:
        for i in sizes:
            if (np.abs(len(axes[j]) - sizes[i]) < threshold) and (len(axes[j]) > 0):
                axes[j] = np.linspace(axes[j][0], axes[j][-1], sizes[i])
    return axes

def find_init_shape(metadata, data, axes, sizes):
    """
    Finds the initial shape of the data block by comparing
    
    Parameters:
        metadata (dict):
            metadata from the header
        data (array):
            flattened data obtained from the file
        axes (dict):
            axes reconstructed with build_axes
        sizes (dict):
            lengths of the axes

    Returns:
        dimensions (list):
            appropriate shape to reshape the data
    """
    L_data = len(data)
    dimensions = []
    for i in sizes:
        dimensions.append(sizes[i])
    for i in axes:
        if i != 'TYPE':
            dimensions.append(len(axes[i]))

    #print('All dimensions : '+str(dimensions))

    dimensions = list(set(dimensions))              #removes redundancy
    if 'X' in axes.keys() and 'Y' in axes.keys():   #restores missing axis if any
        if len(axes['X']) == len(axes['Y']):
            dimensions.append(len(axes['X']))
    #print('Removed redundancy : '+str(dimensions))
    for value in 0,1:
        while value in dimensions:                  #removes dim size of all unidimensional Parameters: (size 0/1)
            dimensions.remove(value)
    #print('Removed 0 and 1 : '+str(dimensions))

    to_be_removed = []
    for i in dimensions:                            #lists dim sizes that are not divisor of total size         
        if L_data%i != 0:
            to_be_removed.append(i)
        if i >= L_data:
            to_be_removed.append(i)

    for i in to_be_removed :                        #removes dim sizes that are not divisor of total size   
        while i in dimensions:
            dimensions.remove(i)
    #print('Removed non divisors : '+str(dimensions))

    N = sizes['n_images']
    if len(dimensions) > 2:                         #Removes the number of measurements if stack has more than two dimensions
        for sets in itertools.permutations(dimensions):
            if (sets[0]*sets[1] == N)  and (sets[0] != N) and (sets[1] != N):
                try:
                    dimensions.remove(N)
                    break
                except ValueError:
                    break

    if len(dimensions) == 1 and L_data%dimensions[0] == 0 :     #Restores the missing dimension if data became unidimensionnal
        dimensions.append(L_data//dimensions[0])

    from copy import copy
    for i in dimensions:
        test_dimensions = copy(dimensions)
        test_dimensions.remove(i)
        if np.prod(test_dimensions) == len(data):
            dimensions = test_dimensions

    #print('Found init shape : '+str(dimensions))

    return dimensions