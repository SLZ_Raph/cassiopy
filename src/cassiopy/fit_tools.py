"""
fit_tools.py

Contains definition functions for fitting
"""

import numpy as np

from scipy.special import erf, erfcinv
from scipy.signal import find_peaks
from scipy.optimize import curve_fit, OptimizeWarning, lsq_linear
from scipy.interpolate import interp1d, approximate_taylor_polynomial as taylor
from scipy.ndimage import gaussian_filter as gf
from scipy.ndimage import gaussian_filter

#----------------------------------------Real analysis

# def gaussian_filter(U, sigma, *args):

# 	M = np.max(U)

# 	V=U.copy()
# 	V[np.isnan(U)]=0
# 	VV=gf(V,sigma=sigma,*args)

# 	W=0*U.copy()+1
# 	W[np.isnan(U)]=0
# 	WW=gf(W,sigma=sigma,*args)
# 	np.seterr(divide='ignore', invalid='ignore')
# 	Z=VV/WW
# 	np.seterr(divide='raise', invalid='raise')
# 	Z = M*Z/np.max(Z)

# 	return Z

def find_value(value, xdata, ydata):
	"""
	Find *one* x for which f(x) = value or gives the closest datapoint
		
	Parameters:
		value : float
		xdata : list or ndarray
			array containing x values
		ydata : list or ndarray
			array containing y values

	Returns:
		x_m : float
		Antecedant of for which f(x) = value

	Warnings
		The procedure will only work properly with monotone functions
	"""
	dist = (ydata-value)**2
	x_m = xdata[np.argmin(dist)]
	return x_m

#----------------------------------------Related to gaussian fit

def gaussian_peak(x, A, m, s, B):
	return A*np.exp(-((x-m)/(np.sqrt(2)*s))**2)+B

def estimate_FWHM(xdata, ydata, m, ratio):
	"""
	Parameters:
		xdata : 1d ndarray
		ydata : 1d ndarray
		m : float
			Maximum/center of the distriution
		ratio : float

	Returns:
		Estimate of the FWHM
	"""
	indexmax = np.argmin(np.abs(m-xdata))
	i = indexmax
	while ydata[i]>ratio*ydata[indexmax] and i>=0:				#Descending the peak 
																#from the right
		i-=1
	widthLeft = np.abs(xdata[indexmax]-(xdata[i]+xdata[i+1])/2)

	while ydata[i]>ratio*ydata[indexmax] and i<len(ydata)-1:	#Descending the peak 
																#from the left
		i+=1
	widthRight = np.abs(xdata[indexmax]-(xdata[i]+xdata[i-1])/2)

	return 2*max(widthLeft, widthRight)

def estimate_m(xdata, ydata):
	"""
	Applies a Wiener filter to the ydata then compute a finite difference 
	approximation of the derivative
	Roots of the derivative is found between given emin, emax for whom 
	f(emin) and f(emax) should have different signs.
	
	Garanteed in smooth data because of proeminent peaks.
	Garanteed in noisy data since the derivative is equally noisy and then has a 
	definite maximum
	The method should find the biggest peak.

	Parameters:
		xdata : ndarray or list
		1d ndarray like object containing the x coordinate axis
	ydata : ndarray or list
		1d ndarray like object containing the y values y(x)
	
	Returns:
		en : float
		

	"""
	Ekin = xdata[:]
	#w = wiener(ydata)						#Applies a Wiener filter to the ydata
	diff = np.gradient(ydata)					#Finite difference approximation of derivative
	interpWDiff = interp1d(xdata, diff)  #Interpolated version of the derivative
	xmin = xdata[np.argmin(diff)]
	xmax = xdata[np.argmax(diff)]
	en = brentq(interpWDiff, emin, emax)
	#Finds the point where the derivative cancels.
	#f(emin) and f(emax) should have different signs.
	#Garanteed in smooth data because of proeminent peaks.
	#Garanteed in noisy data since the derivative is equally noisy.
	#The method should find the biggest peak.

	return en

def estimate_params_g(xdata, ydata, method = 'Max'):
	"""
	Find a first estimation of optimisation Parameters: (gaussian fit)
	
	Parameters:
		xdata : ndarray or list
		1d ndarray like object containing the x coordinate axis
	ydata : ndarray or list
		1d ndarray like object containing the y values y(x)
	method : string
		The name of the method that is used to detect the biggest peak
		Should be'Max' or 'Deriv'

	Returns:
		A, m, s, B : floats
		Parameters: of the gaussian peak A*exp(((x-m)/(np.sqrt(2)*s))^2)+B
	reduced_ydata : ndarray
		New image domain of definition for the fit
	reduced_xdata : ndarray
		New domain of definition for the fit
	"""
	if method == 'Max':
		m = xdata[np.argmax(ydata)]
	elif method == 'Deriv':
		m = estimate_m(xdata, ydata)
	else:
		raise NameError('No method with name '+method)

	ratio= 1/2
	FWHM = estimate_FWHM(xdata, ydata, m, ratio)
	s = FWHM/3

	window = (xdata<m+2*s)*(xdata>m-2*s)
	reduced_ydata = ydata[window]
	reduced_xdata = xdata[window]

	B = (reduced_ydata[0]+reduced_ydata[-1])/2
	A = np.max(reduced_ydata)-B

	return A, m, s, B, reduced_ydata, reduced_xdata

#----------------------------------------Work function fit related

def erfi(x):
	return erfcinv(1-x)

def gen_erf(x, W, A, I, s):
	"""
	Generalised error function
		
	Parameters:
		x : float or ndarray
		variable
	W : float
		work function
	A : float
		amplitude
	I : float
		bottom shift
	s : float
		dispersion

	Returns:
	
	"""
	return A*erf((x-W)/(np.sqrt(2)*s))+I

def estimate_params_erf(xdata, ydata):
	"""
	Estimate the Parameters: for the generalised error function fit
		
	Parameters:
		xdata : ndarray or list
		1d ndarray like object containing the x coordinate axis
	ydata : ndarray or list
		1d ndarray like object containing the y values y(x)
		
	Returns:
		W : work function
	A : amplitude
	I : bottom shift
	s : dispersion
	lims : ndarray
		Limits of acceptable fit in a array of boolean
	"""
	Dy = np.gradient(ydata, xdata)
	W = xdata[np.argmax(np.abs(Dy))]
	I = (ydata.max()+ydata.min())/2

	A = np.sign(xdata[np.argmax(ydata)]-xdata[np.argmin(ydata)])*(ydata.max()-ydata.min())/2

	Dy_i = interp1d(xdata, Dy)

	alpha = 1.15											#We want to get the x for which y = alpha*min(y)
	x_m = find_value(alpha*ydata.min(), xdata, ydata)		#We find that x value as x_m

	#s = (W-x_m)/(np.sqrt(2)*erfi((alpha*ydata.min()-I)/A))	#We use the inverse of the fit function to find a value of s
	s = 1/(Dy_i(W)*np.abs(xdata[np.argmax(ydata)]-xdata[np.argmin(ydata)]))

	lims = (xdata<=xdata[np.argmax(ydata)])					#We generate the array of boolean for the acceptable fitting zone
	return W, A, I, np.abs(s), lims

def fit_erf(xdata, ydata):
	"""
	Function that performs the fitting of a generalised error function to a couple of 
	arrays xdata and ydata
	"""
	estimation = estimate_params_erf(xdata, ydata)
	lims = estimation[4]
	#print('Estimate: '+str(estimation))
	opt, res = curve_fit(gen_erf, xdata[lims], ydata[lims], estimation[:-1])
	return opt

#----------------------------------------XPS specific

def shirley_BG(df, N):
	"""
	Computes the Shirley background of a spetrum

	Parameters:
		df : list or ndarray
		1D spectrum data
	N : integer
		Number of iterations
	
	Returns:
		corrected_df : ndarray 
		Spectrum data with Shirley BG removed
	"""
	temporaire = df - df[-1]
	temp = temporaire/2

	for n in range(N):
		temp = [temporaire[i]-temp[0]*np.sum(temporaire[i:])/np.sum(temporaire) for i in range(len(df))]
		temporaire = temp
		temp[0] = temporaire[0]/2

	corrected_df = temporaire
	return corrected_df

#----------------------------------------Related to polynomial fit

def designmat(x,n):
	return np.array([np.array(x)**i for i in range(int(n+1))]).T

def create_bounds(n, bounds):
	boundaries = []
	event = False
	for i in range(int(n+1),0,-1):
		for bound in bounds :
			if bound[0] == i-1:
				event = True
		if event:
			boundaries.append(bound[1:])
		else:
			boundaries.append([-np.inf, np.inf])
		event = False
	boundaries = np.transpose(boundaries)
	return boundaries

def constrained_polyfit(xdata, ydata, n, bounds = None):
	deg =int(n)
	mat = np.flip(designmat(xdata,4),1)
	boundaries = create_bounds(deg, bounds)
	print(boundaries)
	coeff = lsq_linear(mat,ydata, bounds = boundaries)
	return coeff.x