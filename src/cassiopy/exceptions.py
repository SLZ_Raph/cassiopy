"""
exceptions.py

Contains custom error classes
"""

#------------------------Custom error classes

class Error(Exception):
    """Base class for other exceptions"""
    pass

class UserQuittingError(Error):
	"""Waits for the user to input quit"""
	pass