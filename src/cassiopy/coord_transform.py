"""
coord_transform.py

Contains all functions that convert angular and kinetic energy data to wavevector and binding energy.
All angular quantities are assumed to be in degrees
All energy quantities are assumed to be in eV

The model used is the free electron final state in which :
	k[A^{-1}] = ARPES_CONST*sqrt(E [eV])*sin(angle[degree]*pi/180)
with 
	ARPES_CONST = sqrt(m_e)/hbar

Supported operations:
	hv -> kz
	ang <-> k
	2D ang <-> 2D k

"""

import numpy as np

#----------------------------------------Physical constants
m_e = 0.51099895000 #MeV/c^2 					# Electron mass
h_bar_c = 197.3269805 #MeV.fm					# Planck constant
ARPES_CONST = np.sqrt(2*1e6*m_e)/(h_bar_c*10)	# Constant for converting 
												# k[A^{-1}] ~ ARPES_CONST*sqrt(E [eV])

def hv_to_kz(Ekin,angle,Rz,Rz_offset,phi,tilt,work,hv,V0,Ef,mode = None):
	"""
	Transform photon energy hv into kz.

	Parameters:
		Ekin (array, float):
			Kinetic energy (eV)
		angle (array, float):
			Detector angle (deg)
		Rz (array, float):
			Polar angle (deg)
		Rz_offset (float):
			Polar angle offset (deg)
		phi (float):
			Azimutal angle (deg)
		tilt (float):
			Detector angle offset (deg)
		V0 (float):
			Step potential value (eV)
		Ef (float):
			Fermi Energy (eV)
		mode (str)
			mode='simple' makes the calculation on arrays instead of creating a grid

	Returns:
		ky (ndarray, array, float):
			2D-grids of new coordinates
		kz (ndarray, array, float):
			2D-grids of new coordinates
	"""
	if mode == 'simple':
		X, Y = Ekin, angle
		Z = 0
	else:
		X, Y = np.meshgrid(Ekin,angle)
		#X, Z = np.meshgrid(Ekin,Rz)

	kx = ARPES_CONST*np.sqrt(X)*np.sin((Y-tilt)*np.pi/180)
	kz = ARPES_CONST*np.sqrt(X*np.cos((Y-tilt)*np.pi/180)**2+V0)

	return kx, kz

def kz_to_hv(Eb, kx, kz, tilt = 0, V0 = 0):

	Ekin = (kx**2 + kz**2)/ARPES_CONST**2 - V0 #-Eb
	#angle = np.arcsin(kx/(ARPES_CONST*np.sqrt(Ekin)))*180/np.pi+tilt
	angle = np.arctan(kx/np.sqrt((kz**2) - V0*ARPES_CONST**2))*180/np.pi + tilt
	hv = np.abs(Ekin + Eb)

	return hv, Eb, angle

def ang_to_k(Ekin,angle,Rz,Rz_offset,phi,tilt,work,hv,V0,Ef,mode = None):
	"""
	Transform angle into wavevector, kinetic energy to binding energy

	Parameters:
		Ekin (array, float):
			Kinetic energy (eV)
		angle (array, float):
			Detector angle (deg)
		Rz (array, float):
			Polar angle (deg)
		Rz_offset (float):
			Polar angle offset (deg)
		phi (float):
			Azimutal angle (deg)
		tilt (float):
			Detector angle offset (deg)
		V0 (float):
			Step potential value (eV)
		Ef (float):
			Fermi Energy (eV)
		mode (str)
			mode='simple' makes the calculation on arrays instead of creating a grid

	Returns:
		Eb (ndarray, array, float):
			2D-grids of new coordinates
		kx (ndarray, array, float):
			2D-grids of new coordinates
	"""
	if mode == 'simple':
		X, Y = Ekin, angle
		Z = 0
	else:
		X, Y = np.meshgrid(Ekin,angle)
		#X, Z = np.meshgrid(Ekin,Rz)
	#print(type(X))
	#print(type(Y))
	#nprint(type(tilt))
	kx = ARPES_CONST*np.sqrt(X)*np.sin((Y-tilt)*np.pi/180)
	Eb = Ef-X
	return Eb, kx

def k_to_ang(E,k,Ef=0,tilt=0,Rz_offset=0):
	"""
	Inverse transform wavevector to angle

	Parameters:
		Ekin (array, float):
			Kinetic energy (eV)
		k (array, float):
			Wavevector (Angstrom^(-1))
		Rz_offset (float):
			Polar angle offset (deg)
		tilt (float):
			Detector angle offset (deg)
		Ef (float):
			Fermi Energy (eV)

	Returns:
		Ekin (array, float):
			Kinetic energy
		angle (array, float):
			angle
	"""
	Ekin = E+Ef
	angle = np.arcsin(k/(ARPES_CONST*np.sqrt(Ekin)))*180/np.pi+tilt
	#Rz = np.arcsin(ky/(ARPES_CONST*np.sqrt(Ekin)))*180/np.pi+Rz_offset
	return Ekin, angle

#Old
# def ang_to_k3D(Ekin,theta,Rz,Rz_offset,phi,tilt,work,hv,V0,Ef,mode = None):
# 	"""
# 	Transform two angles into wavevectors
# 	"""
# 	if mode == 'simple':
# 		X, Y, Z = Ekin, theta, Rz
# 	else:
# 		X, Y, Z = np.meshgrid(Ekin,theta,Rz)
# 	kx = ARPES_CONST*np.sqrt(X)*np.sin((Z-Rz_offset)*np.pi/180)
# 	ky = ARPES_CONST*np.sqrt(X)*np.sin((Y-tilt)*np.pi/180)

# 	Eb = Ef-X
# 	return Eb, ky, kx

def ang_to_k3D(Ekin,angle,Rz,Rz_offset,phi,tilt,work,hv,V0,Ef,mode = None):
	"""
	Transform two angles into wavevectors, kinetic energy to binding energy

	Parameters:
		Ekin (array, float):
			Kinetic energy (eV)
		angle (array, float):
			Detector angle (deg)
		Rz (array, float):
			Polar angle (deg)
		Rz_offset (float):
			Polar angle offset (deg)
		phi (float):
			Azimutal angle (deg)
		tilt (float):
			Detector angle offset (deg)
		V0 (float):
			Step potential value (eV)
		Ef (float):
			Fermi Energy (eV)
		mode (str)
			mode='simple' makes the calculation on arrays instead of creating a grid

	Returns:
		Eb (ndarray, array, float):
			3D-grids of new coordinates
		ky (ndarray, array, float):
			3D-grids of new coordinates
		kx (ndarray, array, float):
			3D-grids of new coordinates
	"""
	if mode == 'simple':
		X, Y, Z = Ekin, angle, Rz
	else:
		X, Y, Z = np.meshgrid(Ekin,angle,Rz)
	kx = ARPES_CONST*np.sqrt(X)*np.sin((Y-tilt)*np.pi/180)
	ky = ARPES_CONST*np.sqrt(X)*np.cos((Y-tilt)*np.pi/180)*np.sin((Z-Rz_offset)*np.pi/180)

	Eb = Ef-X
	return Eb, kx, ky



# def k3D_to_ang(E,kx,ky,Ef=0,tilt=0,Rz_offset=0):		#outdated
# 	"""
# 	Inverse transform two wavevectors to angles

# 	Parameters:
# 		Ekin (array, float):
# 			Kinetic energy (eV)
# 		k (array, float):
# 			Wavevector (Angstrom^(-1))
# 		Rz_offset (float):
# 			Polar angle offset (deg)
# 		tilt (float):
# 			Detector angle offset (deg)
# 		Ef (float):
# 			Fermi Energy (eV)

# 	Returns:
# 		Rz (array, float):
# 			angle
# 		Ekin (array, float):
# 			Kinetic energy
# 		theta (array, float):
# 			angle
# 	"""
# 	Ekin = E+Ef
# 	theta = np.arcsin(kx/(ARPES_CONST*np.sqrt(Ekin)))*180/np.pi+tilt
# 	Rz = np.arcsin(ky/(ARPES_CONST*np.sqrt(Ekin)))*180/np.pi+Rz_offset
# 	return Rz, Ekin, theta

def k3D_to_ang(E,kx,ky,Ef=0,tilt=0,Rz_offset=0):
	"""
	Inverse transform two wavevectors to angles

	Parameters:
		Ekin (array, float):
			Kinetic energy (eV)
		k (array, float):
			Wavevector (Angstrom^(-1))
		Rz_offset (float):
			Polar angle offset (deg)
		tilt (float):
			Detector angle offset (deg)
		Ef (float):
			Fermi Energy (eV)

	Returns:
		Rz (array, float):
			angle
		Ekin (array, float):
			Kinetic energy
		theta (array, float):
			angle
	"""
	Ekin = E+Ef
	A = ARPES_CONST*np.sqrt(Ekin)

	angle = np.arcsin(kx/A)*180/np.pi+tilt
	Rz = np.arcsin(A*ky/(A**2 - kx**2))*180/np.pi + Rz_offset

	return Rz, Ekin, angle