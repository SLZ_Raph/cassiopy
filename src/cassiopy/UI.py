"""
UI.py

Contains all functions that interact directly with the user in the terminal
"""

from cassiopy.plotting import *
from cassiopy.load import *
from cassiopy.user_interpret import *

#---------------------Related experiment management

def load_exp(params, name = 'unknown'):
	"""
	Loads a new experiment
	
	Parameters:
		params : dict
		params.keys() = ['path','mode']
		Contains the path and the loading mode loading mode can be "single" or other (hv/FS/XZ...)
	name : string
		Name of the experiment
	
	Returns:
			exp : load.Experiment object 
	"""

	waves = load_waves(params)
	if os.path.isfile(params['path']):
		name = params['path'].split('/')[-1]
	else:
		name = params['path'].split('/')[-2]

	exp = Experiment(mode = params['mode'],
 						 name = name, 
 						 path = params['path'], 
 						 waves = waves,
 						 axes = [])

	# print_formatted('Finding Fermi Edges',2)
	# Efs = [0,0]
	# for i in range(len(waves)):
	# 	if FERMI.findall(waves[i].metadata[0].title + waves[i].metadata[0].comments):
	# 		waves[i].metadata[0] = interpret_comments(waves[i].metadata[0])
	# 		if waves[i].metadata[0].hv in Efs:
	# 			waves[i].metadata[0].Ef = Efs[Efs.index(waves[i].metadata[0].hv)+1]
	# 		else:
	# 			waves[i].metadata[0].Ef = waves[i].metadata[0].hv#-4.2

	# 		Ef = plot_integrated(waves[i], target = waves[i].metadata[0].Ef)
	# 		Efs+=[waves[i].metadata[0].hv, Ef]
	# 		print('In '+waves[i].metadata[0].title+' : found couple [hv, Ef] = '+str(Efs[-2:]))

	# print_formatted('Interpreting comments',2)
	# for i in range(len(waves)):
	# 	print('	'+str(i)+' : '+waves[i].metadata[0].title.replace('\n','; '))#+' ,Sweeps :'+str(waves[i].metadata[0].all['Number of Sweeps']))
	# 	waves[i].metadata[0] = interpret_comments(waves[i].metadata[0])
	# 	if waves[i].metadata[0].hv in Efs:
	# 		waves[i].metadata[0].Ef = Efs[Efs.index(waves[i].metadata[0].hv)+1]
	# 	else:
	# 		waves[i].metadata[0].Ef = waves[i].metadata[0].hv#-4.2
	# 	print(waves[i].metadata[0])

	# print_formatted('Checking metadata',2)
	# for i in range(len(waves)):#+' ,Sweeps :'+str(waves[i].metadata[0].all['Number of Sweeps']))
	# 	if waves[i].metadata[0].hv == 0:
	# 		print('	'+str(i)+' : '+waves[i].metadata[0].title.replace('\n','; '))
	# 		print('The wave has hv = 0, please input a proper photon energy :')
	# 		query = input_safe('hv ? ')
	# 		try :
	# 			waves[i].metadata[0].hv = float(query)
	# 		except ValueError:
	# 			print(query+' is not a number, hv will stay 0 but this can create issues.')
	# 			print('You can always edit the value in edit_mode later.\n')

	# 	if waves[i].metadata[0].hv in Efs:
	# 		waves[i].metadata[0].Ef = Efs[Efs.index(waves[i].metadata[0].hv)+1]
	# 	else :
	# 		waves[i].metadata[0].Ef = waves[i].metadata[0].hv
	# 	#else:
	# 		#waves[i].metadata[0].Ef = waves[i].metadata[0].hv-4.2

	# print_c('_','_')
	# print('\n')

	return exp

# def load_exp(params, name = 'unknown'):
# 	"""
# 	Loads a new experiment
	
# 	Parameters:
# 		params : dict
# 		params.keys() = ['path','mode']
# 		Contains the path and the loading mode loading mode can be "single" or other (hv/FS/XZ...)
# 	name : string
# 		Name of the experiment
	
# 	Returns:
# 			exp : load.Experiment object 
# 	"""
# 	path = params['path']

# 	print('DEBUG:', path)

# 	if SINGLE.match(params['mode']) and path[-1] == '/':
# 		#filepaths = list_singles(path)
# 		waves = load_single_files(path)
# 		exp = Experiment(mode = params['mode'],
# 						 name = path.split('/')[-2],
# 						 path = path,
# 						 waves = waves,
# 						 axes = [])

# 	elif SINGLE.match(params['mode']) and path[-1] != '/':
# 		waves = [load_single(path)]

# 		exp = Experiment(mode = params['mode'],
# 						 name = path.split('/')[-1],
# 						 path = path,
# 						 waves = waves,
# 						 axes = [])

# 	elif SCIENTA.match(params['mode']):
# 		waves = load_SCIENTA_files(path)
# 		exp = Experiment(mode = 'single',
# 						 name = path.split('/')[-1],
# 						 path = path,
# 						 waves = waves,
# 						 axes = waves[0].axes)
# 	elif PEEM.match(params['mode']):
# 		if ('x' in params['mode']) or ('X' in params['mode']):
# 			mode = 'XPEEM'
# 		else:
# 			mode = 'KPEEM'
# 		waves = load_PEEM_files(path,  mode= mode, flatpath= params['flatpath'])
# 		exp = Experiment(mode = 'FS', 
# 						 name = path.split('/')[-2], 
# 						 path = path, 
# 						 waves = waves, 
# 						 axes = waves[0].axes)
# 	elif SPECS.match(params['mode']) or ('.itx' in path):
# 		waves = load_SPECS_files(path)
# 		exp = Experiment(mode = 'FS', 
# 						 name = path.split('/')[-1], 
# 						 path = path, 
# 						 waves = waves, 
# 						 axes = waves[0].axes)
# 	elif MBS.match(params['mode'])  or ('.krx' in path):
# 		waves = load_MBS_files(path)
# 		exp = Experiment(mode = 'FS', 
# 						 name = path.split('/')[-1], 
# 						 path = path, 
# 						 waves = waves, 
# 						 axes = waves[0].axes)
# 	elif RAMAN.match(params['mode']):
# 		mode = 'RAMAN'
# 		waves = load_PL_files(path)
# 		exp = Experiment(mode = 'RAMAN',
# 						 name = path.split('/')[-1], 
# 						 path = path, 
# 						 waves = waves, 
# 						 axes = waves[0].axes)
# 	else :
# 		#print('DEBUG load_exp')
# 		#filepaths = list_coupled_files(path)
# 		waves = load_coupled_files(path, params['mode'])
# 		exp = Experiment(mode = params['mode'],
# 						 name = path.split('/')[-2], 
# 						 path = path, 
# 						 waves = waves,
# 						 axes = waves[0].axes)

# 	print_formatted('Finding Fermi Edges',2)
# 	Efs = [0,0]
# 	for i in range(len(waves)):
# 		if FERMI.findall(waves[i].metadata[0].title + waves[i].metadata[0].comments):
# 			waves[i].metadata[0] = interpret_comments(waves[i].metadata[0])
# 			if waves[i].metadata[0].hv in Efs:
# 				waves[i].metadata[0].Ef = Efs[Efs.index(waves[i].metadata[0].hv)+1]
# 			else:
# 				waves[i].metadata[0].Ef = waves[i].metadata[0].hv#-4.2

# 			Ef = plot_integrated(waves[i], target = waves[i].metadata[0].Ef)
# 			Efs+=[waves[i].metadata[0].hv, Ef]
# 			print('In '+waves[i].metadata[0].title+' : found couple [hv, Ef] = '+str(Efs[-2:]))

# 	print_formatted('Interpreting comments',2)
# 	for i in range(len(waves)):
# 		print('	'+str(i)+' : '+waves[i].metadata[0].title.replace('\n','; '))#+' ,Sweeps :'+str(waves[i].metadata[0].all['Number of Sweeps']))
# 		waves[i].metadata[0] = interpret_comments(waves[i].metadata[0])
# 		if waves[i].metadata[0].hv in Efs:
# 			waves[i].metadata[0].Ef = Efs[Efs.index(waves[i].metadata[0].hv)+1]
# 		else:
# 			waves[i].metadata[0].Ef = waves[i].metadata[0].hv#-4.2
# 		print(waves[i].metadata[0])

# 	print_formatted('Checking metadata',2)
# 	for i in range(len(waves)):#+' ,Sweeps :'+str(waves[i].metadata[0].all['Number of Sweeps']))
# 		if waves[i].metadata[0].hv == 0:
# 			print('	'+str(i)+' : '+waves[i].metadata[0].title.replace('\n','; '))
# 			print('The wave has hv = 0, please input a proper photon energy :')
# 			query = input_safe('hv ? ')
# 			try :
# 				waves[i].metadata[0].hv = float(query)
# 			except ValueError:
# 				print(query+' is not a number, hv will stay 0 but this can create issues.')
# 				print('You can always edit the value in edit_mode later.\n')

# 		if waves[i].metadata[0].hv in Efs:
# 			waves[i].metadata[0].Ef = Efs[Efs.index(waves[i].metadata[0].hv)+1]
# 		else :
# 			waves[i].metadata[0].Ef = waves[i].metadata[0].hv
# 		#else:
# 			#waves[i].metadata[0].Ef = waves[i].metadata[0].hv-4.2

# 	print_c('_','_')
# 	print('\n')

# 	return exp

def load_mode(exp, experiments, params):
	"""
	Loads another experiment either in the list or new
	
	Parameters:
		exp : load.experiment object
		Currently loaded load.experiment object
	experiments : list 
		Contains all the experiments loaded
	params : dict
		params.keys() = ['path','mode']
		Contains the path and the loading mode loading mode can be "single" or other (hv/FS/XZ...)
	
	Returns:
		exp : load.experiment object
		Newly loaded load.experiment object
	"""
	print('There are '+str(len(experiments))+' experiments loaded :\n')
	for i in range(len(experiments)) :
		print('	Experiment['+str(i)+'] :')
		print(experiments[i])

	try:
		query = input_safe('Load new ?(y/n) ')
		if ACCEPT.match(query):
			params['path'] = input_safe('Indicate path: ')

			while not (os.path.exists(params['path'])):

				print('Invalid path')
				query = input_safe('Retry ?(y/n) ')
				if ACCEPT.match(query):
					params['path'] = input_safe('Indicate path: ')
				else:
					raise UserQuittingError

			if params['path'][-1] != '/':
				params['path']+='/'

			params['mode'] = input_safe('Indicate mode: (map/fermi/hv/single/multi/mbs/specs/...) ')

			if os.path.exists(params['path'][:-1]) 		 \
		 	and not(os.path.isdir(params['path'][:-1])) \
		 	and 										 \
			(   RAMAN.match(params['mode'])				 \
		 	or SPECS.match(params['mode'])				 \
		 	or MBS.match(  params['mode'])):
				params['path'] = params['path'][:-1]
				print('coucou', params['mode'])

			name = input_safe('Indicate experiment name: ')

			exp = load_exp(params, params)
			experiments.append(exp)
			
		else:
			query = input_safe('Load old ?(y/n) ')
			if ACCEPT.match(query):
				number = int(input_safe('Indicate experiment number : '))+1
				if number == False:
					raise UserQuittingError
				number -=1
				if number > len(experiments):
					print('\t\tQuery > number of experiments')
					raise UserQuittingError
				else:
					exp = experiments[number]
					print_c('Loaded successfully')
					raise UserQuittingError
			else:
				exp = exp

	except (UserQuittingError, KeyboardInterrupt, ValueError):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return exp, experiments, params

	print_c('_','_')
	print('\n')
	return exp, experiments, params

def create_exp(exp, experiments, params):
	"""
	Converts an array of 2D waves of type into a 3D wave with a new axis.
	The new axis can be created from any variable parameter.
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment 
		experiments (list):
			Contains all the experiments loaded
		params : dict
			params.keys() = ['path','mode']
			Contains the path and the loading mode loading mode can be "single" or other (hv/FS/XZ...)
	
	Returns:
		exp (Experiment)
			Newly loaded experiment of type 'MULTI'
	"""
	try:
		query = input_safe('Create new experiment?(y/n) ')
		if ACCEPT.match(query):
			print('Available variables:')
			labels = list(exp.waves[0].metadata[0].__dict__.keys())
			for label_0, label_1, label_2 in zip(labels[0::3], labels[1::3], labels[2::3]) :
				print('\t- '+label_0+', '+label_1+', '+label_2)
			params['attributes_names'] = input_safe('Indicate the variable from which the wave will be created:\n')
			name = input_safe('Indicate experiment name:')
			new_exp = []

			wave = create_ND_wave(exp.waves, params['attributes_names'], mode='FS')

			# try:
			# 	axis3 = [getattr(exp.waves[i].metadata[0], params['mode']) for i in range(len(exp.waves))]
			# except:
			# 	print('Attribute ',params['mode'],' not found')
			# 	print('Arbitrary index used instead')
			# 	axis3 = np.arange(len(exp.waves))



			# dfs = [exp.waves[i].data for i in range(len(exp.waves))]
			# n_meta = [exp.waves[i].metadata[0] for i in range(len(exp.waves))]

			# dfs = np.array([x for _,x in sorted(zip(axis3,dfs))])
			# n_meta = np.array([x for _,x in sorted(zip(axis3,n_meta))])

			# axis3 = np.array(sorted(axis3))

			# Ascending = all(axis3[:-1] < axis3[1:])
			# if not(Ascending):
			# 	print('Warning : Wave axis is not stritly ascending')
			# 	print('Arbitrary index used instead')
			# 	axis3 = np.arange(len(exp.waves))
			
			# wave = Wave(n_meta, dfs, axis3, params['mode'], axes_names = [params['mode'], *exp.waves[0].axes_names])

			new_exp = Experiment(mode = params['mode'],
								 name = name, 
								 path = '', 
								 waves = [wave],
								 axes = wave.axes)
			experiments.append(new_exp)
			print('\n')

			exp = new_exp

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return exp, experiments, params

	print_c('_','_')
	print('\n')

	return exp, experiments, params	

def edit_mode(exp):
	"""
	Changes the metadata of the selected waves in an experiment. The modifications are directly 
	operated on the metadata of the objects.
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment 
	
	Notes:
		Multiple modes are usable:
		* Batch edition will apply the modifications operated on all selected files
		* In file-wise edition, the user will specify the modifications of all files sequentially
	"""

	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1

	try:
		keys = ['title','sample','hv','Ef',
				'tilt','phi','Rz','Rz_offset',
				'angular','kinetic']
		metadata = exp.waves[0].metadata[0]
		entries = collect_entry(metadata, keys)

		query = input_safe('\nEdit files ?(y/n) ')

		if ACCEPT.match(query):
			if not(SINGLE.match(exp.mode)) :
				print('The new settings will be applied to all '
					  +str(len(exp))
					  +' files of the experiment)\n...')

				entries, changed_keys = input_entry(entries)

				for wave in exp.waves :
					for key in changed_keys :
						for metadata in wave.metadata:
						#if key != 'title':
							if key in ['angular','kinetic']:
								setattr(wave.metadata[0], 
										key, 
										re.match('true',entries[key]) != None)
							else:
								setattr(metadata, key, entries[key])	
			else :
				query = input_safe('Batch edit ?(y/n) ')
				if ACCEPT.match(query):
					query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
					indexlist = [i for i in to_indexlist(query) if i<len(exp)]
					print('	Selected :'+str(indexlist))

					entries, changed_keys = input_entry(entries, len(indexlist))
					waves =[exp.waves[i] for i in indexlist]
					running_index = 0
					for wave in waves :
						for key in changed_keys :
							if type(entries[key]) == type(list()):
								setattr(wave.metadata[0], key, entries[key][running_index])
							else:
								if key != 'title':
									setattr(wave.metadata[0], key, entries[key])
								if key in ['angular','kinetic']:
									setattr(wave.metadata[0], 
											key, 
											re.match('true',entries[key]) != None)	
											#equals True (boolean) 
											#if entries['key'] matches with 'true'
						running_index+=1

				else :
					query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
					indexlist = [i for i in to_indexlist(query) if i<len(exp)]
					print('	Selected :'+str(indexlist))

					for wave in [exp.waves[i] for i in indexlist] :
						entries = collect_entry(wave.metadata[0], keys)
						print(entries)
						entries, changed_keys = input_entry(entries)
						for key in changed_keys :
							if key in ['angular','kinetic']:
								setattr(wave.metadata[0], 
										key, 
										re.match('true',entries[key]) != None)	
										#equals True (boolean) 
										#if entries['key'] matches with 'true'
							else:
								setattr(wave.metadata[0], key, entries[key])

			print('\nNew metadata :')
			print(entries)
			print('\n')

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')

def detail_view_mode(exp):
	"""
	Shows a brief summary of the metadata of the selected waves in the currently
	loaded experiment.
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment 

	"""

	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1

	try :
		print_c('Experiment name: '+exp.name)
		print('There are '+str(len(exp))+' files loaded')
		query = input_safe('View all ?(y/n)  ')
		print('\n')
		if ACCEPT.match(query):
			indexlist = range(len(exp))
		else:
			query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
			indexlist = [i for i in to_indexlist(query) if i<len(exp)]
			print('	Selected :'+str(indexlist))
			#indexlist = [int(i) for i in LISTS.findall(query) if int(i)<len(exp)]
		for i in indexlist :
			if SINGLE.findall(exp.mode):
				print('Index:'+str(i))
				print(exp.waves[i].metadata[0])
				print('\n')
			else:
				#print('Index:'+str(i))
				print(exp.waves[0])
				print('\n')

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')

def save_mode(exp):
	"""
	Save mode writes a .jexp type file (json database) containing the data and metadata of
	the selected spectra.
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment 
	
	"""
	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1

	try:
		query = input_safe('\nSave files ?(y/n) ')
		if ACCEPT.match(query):
			query = input_safe('Save all ?(y/n) ')
			if ACCEPT.match(query):
				indexlist = range(len(exp))
			else:
				query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
				indexlist = [i for i in to_indexlist(query) if i<len(exp.waves[0])]
			
			print('	Selected :'+str(indexlist))
			path = input_safe('Please enter path:\n')
			print('\n')

			if SINGLE.findall(exp.mode):
				wavelist = [exp.waves[i] for i in indexlist]
			else:
				wavelist = [Wave(exp.waves[0].metadata[i], exp.waves[0].data[i]) for i in indexlist]

			for wv in wavelist:
				if wv.metadata[0].angular == True:
					df,meta = change_spectrum_coordinates(wv.data,wv.metadata[0])
				else :
					df, meta = wv.data,wv.metadata[0] 
				wvk = Wave(meta,df)
				wvk.save(path+meta.title+'.jexp')
				wvk.save_txt(path+meta.title+'.txt')
				print(meta.title+'.jexp saved in '+path+'\n')

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')

def fermi_mode(exp):
	"""
	Launch the interactive fermi surface viewer.
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment 
	
	Warnings:
		The function will only work properly with experiments containing 3D waves of mode FS/fermi
	"""
	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1
	if SINGLE.match(exp.mode):
		print_c('This experiment is not a fermi suface, mode = '+exp.mode)
		print_c('_','_')
		print('\n')
		return 1

	plot_FS(exp.waves[0])

	print_c('_','_')
	print('\n')

def map_mode(exp):
	"""
	Launch the spatial map viewer

	Parameters:
		exp (Experiment):
			Currently loaded Experiment
	
	Warnings:
		The function will only work properly with experiments containing 3D waves of mode 'XZ' or 'map'
	
	"""
	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1
	if not XZ.match(exp.mode):
		print_c('This experiment is not a XZ map, mode = '+exp.mode)
		print_c('_','_')
		print('\n')
		return 1

	try :
		reduc_funcs = [reduction_peak,
				   	   reduction_fit,
					   reduction_absolute_d]

		reduc_choice = len(reduc_funcs)+1

		print('Choose a reduction function:')
		string = ''
		for i in range(len(reduc_funcs)) :
			string += '--'+reduc_funcs[i].__name__+' ['+str(i)+']\n'
		string += '--Quit [quit]'
		print(string)

		while not(int(reduc_choice) in range(len(reduc_funcs))):
			reduc_choice = input_safe('...? ')
			if QUIT.match(reduc_choice):
				reduc_choice = -1
				print('No reduction function chosen')
				break
			else :
				reduc_choice = int(reduc_choice)

		if reduc_choice != -1 : 
			print('Reduction function :'+reduc_funcs[reduc_choice].__name__+'\n')
			print('Plotting Map')
			plot_mapXZ(exp.waves[0], reduc_funcs[reduc_choice])

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')

def hv_mode(exp, modified = False):
	"""
	Launch the interactive hv scan viewer.
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment
	
	Warnings:
		The function will only work properly with experiments containing 3D waves of mode 'HV'
	"""
	import copy

	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1
	if not HV.match(exp.mode):
		print_c('This experiment is not a hv scan, mode = '+exp.mode)
		print_c('_','_')
		print('\n')
		return 1

	try :
		#The experiment is copied so that no metadata is modified
		expa = copy.deepcopy(exp)

		
		query = input_safe('Shifting all energy scales of -hv ?(y/n)')
		if ACCEPT.match(query):
			for meta in expa.waves[0].metadata:
				meta.Ekin = meta.Ekin - meta.hv
			modified = True

		print_formatted('Background normalisation',1)
		query = input_safe('Normalise ?(y/n) ')
		if ACCEPT.match(query):
			try:
				Emin = float(input_safe('Emin ?'))
				Emax = float(input_safe('Emax ?'))
				Emin, Emax = min(Emin,Emax), max(Emin,Emax)

				for i in range(len(expa)):
					expa.waves[0].data[i] = normalise_background(expa.waves[0].data[i],
															 expa.waves[0].metadata[i], 
												   			 min(Emin,Emax),
												   			 max(Emin,Emax))
			except ValueError:
				print('Invalid input, no normalisation will be done.')

		plot_hv(expa.waves[0])
		#save_hv(expa.dfs, expa.metas)
	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')

def multiview_mode(exp):
	"""
	Launch the multi scan viewer to analyse 'MULTI' type experiments
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment

	"""
	import copy

	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1
	if SINGLE.match(exp.mode):
		print_c('This experiment is not a multi scan, mode = '+exp.mode)
		print_c('_','_')
		print('\n')
		return 1

	try :
		#The experiment is copied so that no metadata is modified
		plot_multi(exp.waves[0])
		print_c('_','_')
		print('\n')
		#save_hv(expa.dfs, expa.metas)
	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

def view_mode(exp):
	"""
	Launch the individual scan viewer for the selected waves
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment
	"""
	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1

	try:
		if SINGLE.match(exp.mode) :
			print('There are '+str(len(exp))+' files loaded')
			query = input_safe('View all ?(y/n)  ')

			if ACCEPT.match(query):
				indexlist = range(len(exp))
			else:
				query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
				indexlist = [i for i in to_indexlist(query) if i<len(exp)]
				print('	Selected :'+str(indexlist))
				#indexlist = [int(i) for i in LISTS.findall(query) if int(i)<len(exp)]

			for i in indexlist :
				plot_single(exp.waves[i])
		else :
			print('There are '+str(len(exp.waves[0].data))+' files loaded')
			query = input_safe('View all ?(y/n)  ')

			if ACCEPT.match(query):
				indexlist = range(len(exp.waves[0].data))
			else:
				query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
				indexlist = [i for i in to_indexlist(query) if i<len(exp.waves[0].data)]
				print('	Selected :'+str(indexlist))
				#indexlist = [int(i) for i in LISTS.findall(query) if int(i)<len(exp)]

			for i in indexlist :
				print('File displayed: '+str(i))
				print(exp.waves[0].metadata[i])
				plot_single(Wave(exp.waves[0].metadata[i], exp.waves[0].data[i]))

			print('Other waves')
			for wave in exp.waves[1:]:
				print(wave.metadata[0])
				plot_single(wave)



	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	except Exception as e:
		print(f"An error occurred: {e}")

	print_c('_','_')
	print('\n')

def peak_mode(exp):
	"""
	Launch peaks analysis tools
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment
	"""
	try :
		if exp == None or len(exp) == 0:
			print_c('This experiment is empty, load one with \'load\'!!')
			print_c('_','_')
			print('\n')
			return 1

		dfs, n_meta, alpha, x , z = exp.dfs, exp.metas, exp.alpha, exp.x, exp.z
		print('There are '+str(len(exp))+' files loaded')
		query = input_safe('View all ?(y/n) ')
		if ACCEPT.match(query):
			indexlist = range(len(exp))
		else:
			query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
			indexlist = [i for i in to_indexlist(query) if i<len(exp)]
			print('	Selected :'+str(indexlist))

		for i in indexlist :
			plot_withCounting(dfs[i], n_meta[i], 
							  [min(n_meta[i].Ekin),max(n_meta[i].Ekin)]
						 )
	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

def band_mode(exp):
	"""
	Launch the band detection and extraction tools.
	The function asks for the location of an external _.dat_ file 
	containing fitting instructions.
	
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment
	
	Returns:
		Writes band detection report and shows supperimposed bands
		over measurements

	Examples:
		The file should have the following syntax
		```
		tlim = value1:value2 value3:value4...
		elim = value5:value6 value3:value4...
		sigma = float1
		a = float2 (optional)
		deg = integer
		method = Laplace or curv2D (optional, default Laplace)
		mode = ang or k (optional, default ang)
		```
		
		```
		#./parameters.dat

		tlim = 9:12
		elim = -1:-5
		sigma = 12.4
		deg = 5
		a = 0.01
		method = curv2D
		mode = k
		```
	"""
	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1
	if not SINGLE.match(exp.mode):
		print_c('A 3D wave is loaded')

	try:
		#dfs, n_meta, alpha, x , z = exp.dfs, exp.metas, exp.alpha, exp.x, exp.z
		print('There are '+str(len(exp))+' files loaded')
		query = input_safe('View all ?(y/n)  ')
		see_all = (ACCEPT.match(query) != None)

		query = input_safe('Batch mode ?(y/n)  ')
		batch = (ACCEPT.match(query) != None)

		if see_all:
			indexlist = range(len(exp))
		else:
			query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
			if not SINGLE.match(exp.mode):
				indexlist = [i for i in to_indexlist(query) if i<len(exp.waves[0])]
			else :
				indexlist = [i for i in to_indexlist(query) if i<len(exp)]
			print('	Selected :'+str(indexlist))

		if batch :
			while not(os.path.exists(query)):
				query = input_safe('Please enter band fitting configuration file location or quit [quit]...\n')
			extracted = read_parameters(query)
			#print(extracted)

		count = 0
		file2 = open('./record.dat','w')
		file2.write('a2;m*;lap;m*(lap)\n')
		file2.close()

		for i in indexlist :
			try:
				if not batch :
					query = input_safe('Please enter band fitting configuration file location or quit [quit]...\n')
					while not(os.path.exists(query)):
						query = input_safe('Please enter band fitting configuration file location or quit [quit]...\n')
					extracted = read_parameters(query)
					#print(extracted)

				lims = [{'tlim':sorted(a[0]), 'elim':sorted(a[1])} 
								for a in zip(extracted['tlim'],extracted['elim'])]
				methods = {'Laplace':laplace, 'curv2D':mean_curvature, 'Id': identity}
				modes = {'ang':True,'k':False}

				if 'a' in extracted.keys():
					a = extracted['a']
				else :
					a = 0

				if 'method' in extracted.keys():
					for key in methods.keys():
						if re.match(key,extracted['method'],re.I):
							method = methods[key]
							break
				else :
					method = methods['Laplace']


				if not SINGLE.match(exp.mode):
					if 'mode' in extracted.keys():
						mode = modes[extracted['mode']]
					elif not(exp.waves[0].metadata[i].angular):
						mode = modes['k']
					else :
						mode = modes['ang']

					wv = Wave(exp.waves[0].metadata[i], exp.waves[0].data[i])

					bands = extract_bands(wv, 
										  sigma = extracted['sigma'], a = a, 
										  lims = lims, method = method)
				else:
					if 'mode' in extracted.keys():
						mode = modes[extracted['mode']]
					elif not(exp.waves[i].metadata[0].angular):
						mode = modes['k']
					else :
						mode = modes['ang']

					bands = extract_bands(exp.waves[i], 
										  sigma = extracted['sigma'], a = a, 
										  lims = lims, method = method)

				# bands = [pd.concat(bands).reset_index(drop=True)]
				# biggest = []
				# for band in bands :
				# 	if len(band) > len(biggest):
				# 		biggest = band

				# bands = [biggest]

				#for band in bands :
					#deriv_band = np.gradient(gaussian_filter(band.band, 5), band.axis)
					#lap_band = np.gradient(deriv_band, band.axis)
					#q = (np.abs(band.axis)<0.3)
					#L = interp1d(band.axis, lap_band)
					#second_deriv = L(0)
					#eff_mass = 2/(second_deriv*0.512**2)

					#plt.plot(band.axis[q], lap_band[q])
					#plt.show()

				if not SINGLE.match(exp.mode):
					if mode == True:
						bandsk = bands_ang_to_k(bands, exp.waves[0].metadata[i])
						fitbandsk, coeffsk = fit_bands(bandsk, extracted['deg'])
						FITbands = fitbandsk
						COEFFs = coeffsk
					else :
						fitbands, coeffs = fit_bands(bands, extracted['deg'])
						FITbands = fitbands
						COEFFs = coeffs
				else:
					if mode == True:
						bandsk = bands_ang_to_k(bands, exp.waves[i].metadata[0])
						fitbandsk, coeffsk = fit_bands(bandsk, extracted['deg'])
						FITbands = fitbandsk
						COEFFs = coeffsk
					else :
						fitbands, coeffs = fit_bands(bands, extracted['deg'])
						FITbands = fitbands
						COEFFs = coeffs

				if batch :
					addendum = '_'+str(count).zfill(2)
				else :
					addendum = ''

				file = open(query+addendum+'_bands', 'a')
				file.write('#N° of bands: '+str(len(FITbands)))
				file.close()

				for k in range(len(FITbands)) :
					file = open(query+addendum+'_bands', 'a')
					file.write('\n#Band n°'+str(k)+'\n____________________________________\n')
					file.write('#')
					for co in COEFFs[k]:
						file.write(repr(co)+', ')
					file.write('\n')
					file.write('#Max mesured '+str(np.min(FITbands[k]['band']))+'\n')
					file.write('#Max fit '+str(np.min(FITbands[k]['fit']))+'\n')
					file.write('#Min mesured '+str(np.max(FITbands[k]['band']))+'\n')
					file.write('#Min fit '+str(np.max(FITbands[k]['fit']))+'\n')
					file.close()
					FITbands[k].to_csv(query+addendum+'_bands', mode = 'a', index = False)
					file = open(query+addendum+'_bands', 'a')
					file.write('\n\n')


				if not SINGLE.match(exp.mode):
					pass
					#plot_bands(exp.waves[0], FITbands,add = addendum+'_poly', show = False, size = 1)
				else:
					wv = Wave(exp.waves[i].metadata,
						  method(gaussian_filter(exp.waves[i].data,
						  						 extracted['sigma']
						  						),
						  		 exp.waves[i].axes,
						  		 a)
						 )

					plot_bands(exp.waves[i], FITbands,add = addendum+'_poly', show = False, size = 1)

				try :
					file2 = open('./record.dat','a')
					index = int(extracted['deg']-2)
					string = str(COEFFs[0][index])+';'+str(2/(2*COEFFs[0][index]*0.512**2))+';'+str(second_deriv)+';'+str(eff_mass)+'\n'
					string = string.replace('.',',')
					file2.write(string)
					file2.close()
				except IndexError:
					file2 = open('./record.dat','a')
					print('Bands are empty\n')
					print(bands)
					FITbands = bands
					file2.write('\n')
					file2.close()

				#plot_bands(exp.waves[i], bands, add = addendum, show = False, size = 1)# state = [mode,mode])
				#plot_bands(wv, bands,add = addendum+'_deriv', show = False, size = 1)
				#plot_bands(exp.waves[i], FITbands,add = addendum+'_poly', show = False, size = 1)
				#plot_bands(exp.waves[i], [k[['axis', 'fit']] for k in FITbands],add = addendum+'_poly_only', show = False, size = 1)
				#plot_bands(exp.waves[i], bands, lims = lims[0], add = addendum+'_red', show = False)
				#plot_bands(wv, bands, lims = lims[0], add = addendum+'_red_deriv', show = False)
				#plot_bands(exp.dfs[i], exp.metas[i], fitbands)
				count +=1
			except FileNotFoundError:
				print('File not found '+query)
				FITbands, COEFFs = None,None

			except FloatingPointError:
				print('Invalid value encountered in sqrt')
				print('Please check and modify angular and kinetic parameters with "edit"')
				raise UserQuittingError

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')

	return FITbands, COEFFs

def dichroism_mode(exp):
	"""
	Launch the dichroism calculation.
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment
	
	Returns:
		Adds a wave to the experiment
	"""
	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1

	try:
		query = input_safe('Indicate CR files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
		indexlist_CR = [i for i in to_indexlist(query) if i<len(exp)]
		print('	Selected for CR:'+str(indexlist_CR))

		query = input_safe('Indicate CL files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
		indexlist_CL = [i for i in to_indexlist(query) if i<len(exp)]
		print('	Selected for CL:'+str(indexlist_CL))

		for indexes in zip(indexlist_CR, indexlist_CL):
			try:
				dfs = exp.waves[indexes[0]].data,exp.waves[indexes[1]].data
				new_df = substract_spectra(dfs)*1000
				new_meta = copy.deepcopy(exp.waves[indexes[0]].metadata)

				new_meta[0].title += '__DICHROISM'

				print('Added to experiment:')
				print(new_meta)

				#exp.dfs.append(new_df)
				#exp.metas.append(new_meta)

				exp.waves.append(Wave(new_meta, new_df))

			except ValueError:
				print('Incompatible shapes for spectra ['+str(indexes[0])+'] and ['+str(indexes[1])+']')

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')

def extended_mapping_mode(exp):
	"""
	Launch merging side by side of several ARPES spectra. Useful for extended scans at different tilt angles.
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment
	
	Returns:
		Adds a wave to the experiment
	"""
	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1

	try:
		if exp.mode == 'single':
			query = input_safe('Indicate files to merge: (Syntax: index1 index2 index3... indexA:indexB ... )\n')
			indexlist_1 = [i for i in to_indexlist(query) if i<len(exp)]
			print('	Selected file:'+str(indexlist_1))

			#query = input_safe('Indicate second file : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
			#indexlist_2 = [i for i in to_indexlist(query) if i<len(exp)]
			#print('	Selected file:'+str(indexlist_2))

			#for indexes in zip(indexlist_1, indexlist_2):
			waves = [exp.waves[i] for i in indexlist_1]
			if len(waves)> 1:
				new_wave = extend_spectra(waves)
				exp.waves.append(new_wave)

				print('Added to experiment:')
				print(new_wave.metadata[0])
			else:
				print('	No file selected ! Please select at least two to merge')
				raise(UserQuittingError)
		else:
			waves = [Wave(met,dat) for dat,met in zip(exp.waves[0].data,exp.waves[0].metadata)]
			waves = list(np.flip(waves))
			new_wave = extend_spectra(waves)
			exp.waves.append(new_wave)

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')

def normalise_mode(exp):
	"""
	Launch the spectrum normalisation tool.
	
	Parameters:
		exp (Experiment):
			Currently loaded Experiment
		Emin (float): lower bound energy to integrate the background 
		Emax (float): upper bound energy to integrate the background
		sigma (float): smoothing factor of the integrated background
	"""
	if exp == None or len(exp) == 0:
		print_c('This experiment is empty, load one with \'load\'!!')
		print_c('_','_')
		print('\n')
		return 1

	try:
		print('There are '+str(len(exp))+' files loaded')
		query = input_safe('\nEdit files ?(y/n) ')

		if ACCEPT.match(query):
			if FS.match(exp.mode) :
				print('Wave mode:'+exp.mode)
				print('The normalisation settings will be applied to all '
					  +str(len(exp.waves[0].data))+' files of the experiment)\n...')

				norm_functions = {0:normalise_stack, 1:normalise_stack_inPlane}

				print('\nData shape (ax_0 (ky), ax_1 (Ek), ax_2 (kx)): '+str(exp.waves[0].data.shape))

				query = len(norm_functions)+1

				while not(int(query) in norm_functions.keys()):
					query = input_safe('\n--Normalise along ax_0 [0] \n--Normalise along ax_1 [1]\n--Quit [quit] : ')
					if QUIT.match(query):
						query = -1
						print('No normalisation has been applied')
						break

				if query != -1:
					Emin = float(input_safe('\nInput Emin value: '))
					Emax = float(input_safe('Input Emax value: '))
					sigma = float(input_safe('Input sigma value: '))
					exp.waves[0].data = norm_functions[int(query)](exp.waves[0].data,
										   					       exp.waves[0].axes[(int(query)-1)%3],
										   					       [Emin, Emax], sigma)

			elif HV.match(exp.mode) :
				print(exp.mode)
				print('The normalisation settings will be applied to all '
					  +str(len(exp.waves[0].data))+' files of the experiment (hv mode))\n...')

				norm_functions = {0:normalise_stack, 1:normalise_stack_inPlane}

				print('\nData shape (ax_0 (hv), ax_1 (Ek), ax_2 (kx)): '+str(exp.waves[0].data.shape))

				query = len(norm_functions)+1

				while not(int(query) in norm_functions.keys()):
					query = input_safe('\n--Normalise along ax_0 [0] \n--Normalise along ax_1 [1]\n--Quit [quit] : ')
					if QUIT.match(query):
						query = -1
						print('No normalisation has been applied')
						break

				if query != -1:
					Emin = float(input_safe('\nInput Emin value :'))
					Emax = float(input_safe('Input Emax value :'))
					sigma = float(input_safe('Input sigma value :'))
					exp.waves[0].data = norm_functions[int(query)](exp.waves[0].data,
										   					       exp.waves[0].axes[(int(query)-1)%3] - exp.waves[0].metadata[0].hv,
										   					       [Emin, Emax], sigma)

			elif not(SINGLE.match(exp.mode)) :
				print(exp.mode)
				print('The normalisation settings will be applied to all '
					  +str(len(exp.waves[0].data))+' files of the experiment)\n...')

				norm_functions = {0:normalise_stack}
				query = 0

				if query != -1:
					Emin = float(input_safe('\nInput Emin value: '))
					Emax = float(input_safe('Input Emax value: '))
					sigma = float(input_safe('Input sigma value: '))
					exp.waves[0].data = norm_functions[int(query)](exp.waves[0].data,
										   					       exp.waves[0].axes[(int(query)-1)%3],
										   					       [Emin, Emax], sigma)

			else :
				query = input_safe('Batch edit ?(y/n) ')
				if ACCEPT.match(query):
					query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
					indexlist = [i for i in to_indexlist(query) if i<len(exp)]
					print('	Selected :'+str(indexlist))

					Emin = float(input_safe('\nInput Emin value: '))
					Emax = float(input_safe('Input Emax value: '))
					sigma = float(input_safe('Input sigma value: '))

					for i in indexlist:
						exp.waves[i].data = normalise_background(exp.waves[i].data,
																 exp.waves[i].axes[0],
														  		 [Emin, Emax], sigma)

				else :
					query = input_safe('Indicate files : (Syntax: index1 index2 index3... indexA:indexB ... )\n')
					indexlist = [i for i in to_indexlist(query) if i<len(exp)]
					print('	Selected :'+str(indexlist))

					for i in indexlist:
						Emin = float(input_safe('\nInput Emin value: '))
						Emax = float(input_safe('Input Emax value: '))
						sigma = float(input_safe('Input sigma value: '))
						exp.waves[i].data = normalise_background(exp.waves[i].data,
																 exp.waves[i].axes[0],
														  		 [Emin, Emax], sigma)

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')

def help():
	"""
	Function designed to help
	"""
	from inspect import isfunction

	try:
		#print('Available functions : (quit/map/fermi/hv/view/norm/dichro/extend/peak/band/load/edit/save/help)')
		functions = list(globals().keys())
		query = input_safe('Type the name of function or mode you need help with: ')
		function_name = ''

		names = []

		for function in functions:
			if re.findall(query, function,re.I):
				function_name = function
				if isfunction(globals()[function_name]):
					print('Matching name : '+function_name)
					names.append(function_name)
					print(globals()[function_name].__doc__)

	except (UserQuittingError, KeyboardInterrupt):
		print('\n')
		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')
		return 1

	print_c('_','_')
	print('\n')