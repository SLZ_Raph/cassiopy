"""
plotting.py

Contains all functions that produce images of the data
	Basic plotting tools
	Plot configuration tools
	Advanced plotting tools for 2D and 3D stacks with interfaces 
	Defintition of the Experiment class
	Saving tools
"""

import tkinter as tk
from tkinter import filedialog

import matplotlib as mpl
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

from matplotlib import cm, colors
from matplotlib.widgets import Slider, Button, TextBox, Cursor
from matplotlib.ticker import (MultipleLocator, FormatStrFormatter,
							   AutoMinorLocator)
#from cassiopy.colormaps import custom_cmaps

#import numpy as np
import os
import json

from cassiopy.analysis import *
from cassiopy.formatting import *
from cassiopy.loaders.parsing import parse_bands

np.seterr(all='raise')

#----------------------------------------General Parameters:

def _get_scale_factor():
	if os.name == 'nt':
		import ctypes
		scaleFactor = ctypes.windll.shcore.GetScaleFactorForDevice(0) / 100
	else:
		scaleFactor = 1
	return scaleFactor

mpl.rcParams['figure.dpi'] = 90/_get_scale_factor()
mpl.rcParams['backend'] = 'QtAgg'

#import matplotlib as mpl
#mpl.rcParams['font.size'] = mpl.rcParams['font.size']/2

#----------------------------------------Basic plotting tools

def plot3D(X,Y,Z):
	fig = plt.figure()
	ax = fig.add_subplot(111, projection='3d')
	ax.plot_surface(X, Y, Z,cmap=cm.hot)
	plt.show()

def plot2D(X,Y,Z):
	plt.pcolormesh(X,Y,Z,cmap=cm.hot, antialiased=False, shading = 'auto')
	plt.draw()

def plot(X,Y,Z):
	fig, ax = plt.subplots(figsize = (5,5), tight_layout = True, shading = 'auto')
	ax.pcolormesh(X,Y,Z)
	ax.axes.set_xlabel('k ('+r'$\AA^{-1}$'+')')
	ax.axes.set_ylabel('$E-E_f$ (eV)')
	configure_plot(ax, 5)
#----------------------------------------Plotting configuration tools

def configure_plot(ax, size, k_axis=[]):
	if str(size).isnumeric():
		size = [size, size]
	else:
		pass
		
	ax.axes.set_xlabel(ax.get_xlabel(), fontsize = 2*size[0]*np.exp(1/size[0]*3))
	ax.axes.set_ylabel(ax.get_ylabel(), fontsize = 2*size[1]*np.exp(1/size[1]*3))
	#ax.axes.set_ylim([min(Elims), max(Elims)])
	#ax.axes.set_xlim([min(klims), max(klims)])

	ax.tick_params(axis='y',
			which='both',
			left=True,
			right=True,
			labelsize = 1.5*size[1]*np.exp(1/size[1]*3))

	ax.tick_params(axis='x',
			which='both',
			bottom=True,
			top=True,
			labelbottom=True,
			labelsize = 1.5*size[0]*np.exp(1/size[0]*3))
	ax.yaxis.set_minor_locator(AutoMinorLocator())
	ax.tick_params(axis='y', which='major', length=7, width=2)

	if k_axis != []:
		ax.xaxis.set_minor_locator(AutoMinorLocator())
		ax.tick_params(axis='x', which='major', length=7, width=2)
	else:
		ax.xaxis.set_minor_locator(AutoMinorLocator())
		ax.tick_params(axis='x', which='major', length=7, width=2)

	plt.setp(ax.spines.values(), linewidth=2)

	ax.figure.tight_layout()

#----------------------------------------Advanced plotting tools for 2D and 3D stacks with interfaces 

def menu(event, graphs, sliders):
	prelabels = [slider.label.get_text() for slider in sliders]
	labels = ['min', 'max', 'step', 'value']
	inits = [[slider.valmin, slider.valmax, slider.valstep, slider.val] for slider in sliders]

	fig = plt.figure(figsize = (10,4))
	gs = fig.add_gridspec(len(sliders),1, wspace = 1)
	subs = [g.subgridspec(1,len(labels), wspace = 1) for g in gs]
	rects = [[fig.add_subplot(i) for i in rec] for rec in subs]

	Texts = []
	for i in range(len(rects)):
		Texts += [[]]
		for j in range(len(rects[i])):
			Texts[-1] += [TextBox(rects[i][j], prelabels[i]+' :'+labels[j], str(inits[i][j]))]

	def change(event):
		for i in range(len(sliders)):
			sliders[i].valmin = float(Texts[i][0].text)
			sliders[i].valmax = float(Texts[i][1].text)
			sliders[i].valstep = float(Texts[i][2].text)
			sliders[i].val = float(Texts[i][3].text)

			sliders[i].ax.set_xlim(sliders[i].valmin, sliders[i].valmax)

	for i in Texts :
		for j in i :
			j.on_submit(change)	

	plt.show()

def plot_single(wave, cmap = cm.gray):
	"""
	Interactive plot of a spectrum with its angular distribution curves (ADC) and 
	energy distribution curves (EDC) on its sides.
	
	Parameters:
		wave (Wave):
			2D spectrum data + metadata
		cmap (cmap):
			Default colormap of the graph
	"""

	def set_Axes(ax, state, text = False):

		ax[0].tick_params(direction='in', top=True, right=True)
		ax[1].tick_params(direction='in', labelbottom=False)
		ax[2].tick_params(direction='in', labelleft=False)

		if text == True:
			ax[1].ticklabel_format(style='sci', scilimits=(-2,3))
		ax[2].ticklabel_format(style='sci', scilimits=(-2,3))

		if state[0]*meta.kinetic == True :
			ax[0].axes.set_ylabel('$E_k$ (eV)')
		else :
			#ax[0].invert_yaxis()
			ax[0].axes.set_ylabel('$E-E_f$ (eV)')

		if state[1]*meta.angular == True :
			ax[0].axes.set_xlabel('$\\theta$ (DEG)')
		else :
			ax[0].axes.set_xlabel('k ('+r'$\AA^{-1}$'+')')

	class Index(object):									#Definition of the event handler object
		state = np.array([True, True, False, False, 1])		#order [Ek, ang, ren, diff]
		prevstate = copy.deepcopy(state)					#Deepcopy necessary to enforce creation of a new object
		derivmodes = [identity, laplace, mean_curvature, curvature]
		derivnames = ['$f_{\\sigma}(x)$','$\\nabla^2 f$', '$C_{2D} f$', '$C_{1D} f$']
		mainPlot = None
		bands = []

		vmin = 0
		vmax = 10000
		old_vmin = vmin
		old_vmax = vmax

		def plot(self):
			lims = [[i.get_xlim(), i.get_ylim()] for i in ax]

			if self.mainPlot != None:
				self.cmap = self.mainPlot.cmap
				if (self.vmin > self.vmax) == (self.old_vmin < self.old_vmax):
					self.cmap = self.cmap.reversed()
			else:
				self.cmap = cmap
				self.kinetic = kinetic
				self.ang = ang
				self.vmin = np.min(graph[0][0])
				self.vmax = np.max(graph[0][0])

			ax[1].clear()
			ADCplot, = ax[1].plot(self.ang[self.state[1]].T[0], integ_energ(graph[self.state[3]][self.state[2]].T))
			ax[2].clear()
			EDCplot, = ax[2].plot(integ_angle(graph[self.state[3]][self.state[2]].T),self.kinetic[self.state[0]][0])
			ax[0].clear()
			self.mainPlot = ax[0].pcolormesh(self.ang[self.state[1]],
											 self.kinetic[self.state[0]],
											 graph[self.state[3]][self.state[2]],
											 cmap=self.cmap,
											 antialiased=False,
											 vmin = min(self.vmin, self.vmax),
											 vmax = max(self.vmin, self.vmax),
											 shading = 'auto')

			for band in self.bands:
				for column in [band.columns[1]]:
					line, = ax[0].plot(band['axis'], band[column], linestyle = 'None', marker = ',')
					self.lines.append(line)

			changes = (self.state != self.prevstate)		#xor operation
			# print('Plotting status update :\nCurrent state')
			# print(self.state)
			# print('prev. state')
			# print(self.prevstate)
			# print('Changes')
			# print(changes)

			if changes[0]:
				ax[0].set_xlim(lims[0][0])				#if energy changed, absciss is conserved
			if changes[1]:
				ax[0].set_ylim(lims[0][1])				#if absciss changed, energy is conserved
			if not(changes[0] or changes[1]):
				ax[0].set_xlim(lims[0][0])
				ax[0].set_ylim(lims[0][1])

			#print_table(self.ang[self.state[1]].T[-1])
			if not(self.state[1]):
				#configure_plot(ax[0],5,self.ang[self.state[1]].T[-1])
				pass

			names = [['$E_B$','$E_k$'],
					 ['$k$','ang'],
					 ['Set $E_F$','Set $E_F$'],
					 ['Set $\\Gamma$','Set $\\Gamma$'],
					 ['Mode\n$\\blacktriangledown$','Mode\n$\\blacktriangledown$'],
					 ['f(x)',self.derivnames[self.state[4]]],	#state[4] is the derivation (cyclic variable instead of bool)
					 ['Save\nimg', 'Save\nimg'],
					 ['Save\ntxt', 'Save\ntxt'],
					 ['Edit\nband', 'Edit\nband'],
					 ['Edit\nsliders','Edit\nsliders']]

			state = [self.state[0], self.state[1], True, True, True, self.state[3], True, True, True, True]
			for i in range(len(bswitchs)): #order [Ek, ang, ren, diff]
				#state = list(self.state)+[True for i in range(len(names)-len(self.state))]
				bswitchs[i]['object'].label.set_text(names[i][state[i]])

			set_Axes(ax, self.state)

		def switch_kinetic(self, event):
			self.prevstate = copy.deepcopy(self.state)
			self.state[0] = not(self.state[0])
			self.plot()
			
		def switch_ang(self, event):
			self.prevstate = copy.deepcopy(self.state)
			self.state[1] = not(self.state[1])
			self.plot()

		def switch_ren(self, event):
			self.prevstate = copy.deepcopy(self.state)
			self.state[2] = not(self.state[2])
			self.plot()

		def switch_deriv(self, event):
			self.prevstate = copy.deepcopy(self.state)
			self.state[3] = not(self.state[3])
			self.plot()

		def switch_mode(self, event):
			self.prevstate = copy.deepcopy(self.state)
			self.state[4] = (self.state[4]+1)%len(self.derivmodes)
			
			deriv =	[self.derivmodes[self.state[4]]( gaussian_filter(df, sigma = sliders[0]['object'].val),	
													 wave.axes[:2], 
													 sliders[1]['object'].val/1000 ).T #functions of derivmodes are transposition sensitive
					, 
					 self.derivmodes[self.state[4]]( gaussian_filter(ren[1].T, sigma = sliders[0]['object'].val),
													 wave.axes[:2], 
													 sliders[1]['object'].val/1000 ).T #functions of derivmodes are transposition sensitive
					]
			graph[1] = deriv			
			self.plot()

		def switch_save_pic(self, event):
			print_formatted('Picture saving mode',1)
			savepath = filedialog.asksaveasfile().name
			save_plot(wave, ax[0].get_xlim(), ax[0].get_ylim(), savepath,
						sigma = sliders[0]['object'].val, 
						diff = self.derivmodes[self.state[4]],
						kinetic = self.state[0],
						angular = self.state[1],
						ren = self.state[2],
						derived = self.state[3],
						state = self.state[:-1], 
						vmin = self.vmin, vmax = self.vmax, cmap = self.cmap, 
						save = True, resolution = [6,6], title = False, filetype = 'png')
			plt.show()

			print('File saved (png) !\n')

		def switch_save_txt(self, event):
			print_formatted('txt saving mode',1)
			savepath = filedialog.asksaveasfile().name
			i = 0

			if self.state[3]:
				basis = savepath+meta.title+'_'+self.derivmodes[self.state[4]].__name__+\
			'_sigma_'+'%.1f'%(sliders[0]['object'].val)+'_'
			else:
				basis = savepath+meta.title+'_'+\
			'_sigma_'+'%.1f'%(sliders[0]['object'].val)+'_'

			name = basis+str(i).zfill(3).replace('\n','__')
			while os.path.exists(name):
				i+=1
				name = basis+str(i).zfill(3).replace('\n','__')
				print(i)
				
			wv_to_sv = Wave(wave.metadata[0], graph[self.state[3]][self.state[2]].T)
			wv_to_sv.save_txt(name)

			print('File saved (txt)!\n')

		def load_band(self, event):
			location = input('Input csv file containing bands : ')
			self.bands = parse_bands(location)
			self.lines = []
			for band in self.bands:
				for column in [band.columns[1]]:
					line, = ax[0].plot(band['axis'], band[column], linestyle = 'None', marker = ',')
					self.lines.append(line)

		def set_gamma(self, event):
			print_formatted('Setting gamma mode',1)
			point = plt.ginput(n = 1, timeout = 0)

			if not(wave.metadata[0].angular):
				tilt = point[0][0]
				#wave.metadata[0].angle = tilt
				wave.axes[1] -= tilt
			else:
				if self.state[1]:
					tilt = point[0][0]
					wave.metadata[0].tilt = tilt
				else:
					tilt = k_to_ang(np.min(self.kinetic[1]),point[0][0],0,0)[1]
					wave.metadata[0].tilt += tilt

			print('Selected point : '+str(point[0][0]))
			print('tilt = '+str(tilt))
			try:
				Eb, k = ang_to_k(meta.Ekin,
								 meta.angle,
								 meta.Rz,
								 meta.Rz_offset,
								 meta.phi,
								 meta.tilt,
								 meta.work,
								 meta.hv,
								 0,
								 meta.Ef)
				self.ang = [k, theta]
				
			except FloatingPointError :
				if wave.metadata[0].angular:
					print('There was an error in wave vector coordinates calculation,\
					 please check kinetic and angular metadatas with "detail" command\n')
				E,k = np.meshgrid(wave.axes[0], wave.axes[1])
				self.ang = [k,k]

			self.plot()

		def set_Ef(self, event):
			print_formatted('Setting Ef mode',1)
			point = plt.ginput(n = 1, timeout = 0)
			if self.state[0] and wave.metadata[0].kinetic :
				wave.metadata[0].Ef = point[0][1]
				E,k = np.meshgrid(wave.axes[0]-wave.metadata[0].Ef, wave.axes[1])
				
				#print('cas 1', wave.metadata[0].Ef)
			else :
				wave.metadata[0].Ef = point[0][1]
				E,k = np.meshgrid(self.kinetic[0][0] - wave.metadata[0].Ef, wave.axes[1])
				#self.kinetic = [E, self.kinetic[1]]
				#print('cas 2', wave.metadata[0].Ef)

			self.kinetic = [E, self.kinetic[1]]
			print('Selected value Ef: '+str(wave.metadata[0].Ef))

			self.plot()

		def change_sigma(self, event):
			self.prevstate = self.state[:]
			deriv =	[self.derivmodes[self.state[4]]( gaussian_filter(df, sigma = sliders[0]['object'].val),	
													 wave.axes[:2], 
													 sliders[1]['object'].val/1000 ).T #functions of derivmodes are transposition sensitive
					, 
					 self.derivmodes[self.state[4]]( gaussian_filter(ren[1].T, sigma = sliders[0]['object'].val),
													 wave.axes[:2], 
													 sliders[1]['object'].val/1000 ).T #functions of derivmodes are transposition sensitive
					]
			graph[1] = deriv
			self.plot()

		def change_contrast(self, event):
			self.prevstate = self.state[:]
			self.old_vmin = self.vmin
			self.old_vmax = self.vmax
			self.vmin = sliders[2]['object'].val
			self.vmax = sliders[3]['object'].val
			self.plot()

	df = wave.data
	meta = wave.metadata[0]

	#-Related to data
	try:
		Eb, k = ang_to_k(meta.Ekin,
					 meta.angle,
					 meta.Rz,
					 meta.Rz_offset,
					 meta.phi,
					 meta.tilt,
					 meta.work,
					 meta.hv,
					 0,
					 meta.Ef)
	except FloatingPointError :
		if wave.metadata[0].angular:
			print('There was an error in wave vector coordinates calculation, please check kinetic and angular metadatas with "detail" command\n')
		Eb,k = np.meshgrid(meta.Ekin, meta.angle)

	Ekin, theta = np.meshgrid(meta.Ekin, meta.angle)

	kinetic = [-Eb, Ekin]
	ang = [k, theta]
	try:
		if np.isnan(df).any():
			ren = [df.T, df.T]
		else:
			ren = [df.T, renormalise(df).T]	#Transposition is necessary to match meshgrid
	except FloatingPointError :
		print('There was an error in renormalisation calculation : impossible to divide by zero\n')
		ren = [df.T, df.T]

	sigma_default = 5

	deriv =	[laplace(gaussian_filter(df, sigma_default), wave.axes[:2]).T,
		     laplace(gaussian_filter(ren[1].T, sigma_default), wave.axes[:2]).T]
	graph = [ren, deriv]

	#-Related to plotting layout
	fig = plt.figure(2, figsize = (10,10))
	fig.suptitle(meta.title+'\n$h\\nu = $'+str(meta.hv)
						   +', $E_f = $'+str(meta.Ef), fontsize=12)

	gs = fig.add_gridspec(3,2, width_ratios=[3.25,1], 			#Defines the main look of the layout
							   height_ratios=[1,3.25,0.5],
							   wspace = 0.10, hspace = 0.5)
	bswitchGrid = gs[0,1].subgridspec(3,3)						#Definition of zones for the buttons
	sliderGrid = gs[2,0].subgridspec(4,1, hspace = 1)			#Definition of zones for the sliders
	sliderMenu = gs[2,1].subgridspec(1,2)[1]

	ax0 = fig.add_subplot(gs[1,0])								#Definition of the main plot zones
	ax1 = fig.add_subplot(gs[0,0], sharex = ax0)				#ax0 : spectra, ax1:energy integrated, ax2:angle integrated
	ax2 = fig.add_subplot(gs[1,1], sharey = ax0)
	ax = [ax0, ax1, ax2]

	set_Axes(ax, [True, True, False, False, 0])

	ADCplot, = ax[1].plot(meta.angle, integ_energ(df))
	EDCplot, = ax[2].plot(integ_angle(df), meta.Ekin)
	mainPlot = ax[0].pcolormesh(meta.angle, meta.Ekin,
								df,
								cmap=cmap,
								antialiased=False,
								shading = 'auto', vmax = 2*np.median(df))

	#-Related to interactivity
	callback = Index()

	rects_sw = [fig.add_subplot(bswitchGrid[0,0]), fig.add_subplot(bswitchGrid[1,0]), 
			 fig.add_subplot(bswitchGrid[0,1]), fig.add_subplot(bswitchGrid[1,1]),
			 fig.add_subplot(bswitchGrid[0,2]),fig.add_subplot(bswitchGrid[1,2]),
			 fig.add_subplot(bswitchGrid[2,0]),fig.add_subplot(bswitchGrid[2,1]),
			 fig.add_subplot(bswitchGrid[2,2]), fig.add_subplot(sliderMenu)]

	rects_sl = [fig.add_subplot(sliderGrid[0]), fig.add_subplot(sliderGrid[1]),
			 fig.add_subplot(sliderGrid[2]), fig.add_subplot(sliderGrid[3])]

	bswitchs = [{'object':Button(rects_sw[0], '$E_k$/$E_b$'), 'function':callback.switch_kinetic},
				{'object':Button(rects_sw[1], '°/k'), 'function':callback.switch_ang},
				{'object':Button(rects_sw[2], 'Set $E_F$'),'function':callback.set_Ef},
				{'object':Button(rects_sw[3], 'Set $\\Gamma$'),'function':callback.set_gamma},
				{'object':Button(rects_sw[4], 'Mode\n$\\blacktriangledown$'),'function':callback.switch_mode},
				{'object':Button(rects_sw[5], 'f(x)'), 'function':callback.switch_deriv},
				{'object':Button(rects_sw[6], 'Save\nimg'),'function':callback.switch_save_pic},
				{'object':Button(rects_sw[7], 'Save\ntxt'),'function':callback.switch_save_txt},
				{'object':Button(rects_sw[8], 'Load\nband'),'function':callback.load_band},
				{'object':Button(rects_sw[9], 'Edit\nsliders'), 'function':lambda event: menu(event, [], [slider['object'] for slider in sliders])}]

	valstep = int(np.max(df[~np.isnan(df)])/1000)


	sliders = [{'object':Slider(rects_sl[0], 
					 label = '$\\sigma$', 
				   	 valmin = 0,valmax = 20, 
				  	 valinit = 5, valstep = 0.2),'function':callback.change_sigma},
			   {'object':Slider(rects_sl[1], 
					 label = '$a\\times 10^3$', 
				   	 valmin = 0,valmax = 0.01*1000, 
				  	 valinit = 0.0001*1000, valstep = 0.0001*1000),'function':callback.change_sigma},
			   {'object':Slider(rects_sl[2], 
					 label = '$v_{min}$', 
				   	 valmin = 0,valmax = int(np.max(df[~np.isnan(df)])), 
				  	 valinit = 0, valstep = abs(int(np.max(df[~np.isnan(df)])/1000))),'function':callback.change_contrast},
			   {'object':Slider(rects_sl[3], 
					 label = '$v_{max}$', 
				   	 valmin = 0,valmax = int(np.max(df[~np.isnan(df)])), 
				  	 valinit = np.max(df[~np.isnan(df)]), valstep = abs(int(np.max(df[~np.isnan(df)])/1000))),'function':callback.change_contrast}]

	cursors = [Cursor(ax[1], horizOn = False, color='r', lw=1, useblit = True), 
			   Cursor(ax[2], vertOn = False, color='r', lw=1, useblit = True),
			   Cursor(ax[0], color='r', lw=1, useblit = True)]

	for bswitch in bswitchs:
		bswitch['object'].on_clicked(bswitch['function'])
	for slider in sliders:
		slider['object'].on_changed(slider['function'])

	plt.show()

def plot_integrated(wave, fermi = True, sigma = 2, target = 21.95, width = 0.5 ):

	df = wave.data
	meta = wave.metadata[0]

	if fermi:
		erange = (meta.Ekin>target - width)
		Ekin = meta.Ekin[erange]
		df_c = df[erange]

	else:
		Ekin = meta.Ekin
		df_c = df

	EDC = integ_angle(df_c)
	try :
		diff = np.gradient(gaussian_filter(EDC, sigma))
		figure, axe = plt.subplots(2,1, sharex=True, constrained_layout = True)
		figure.suptitle(meta.title+'\n$h\\nu = $'+str(meta.hv)+'\n$E_f$ = '+'%.4f'%(Ekin[np.argmax(np.abs(diff))]))
		graph = axe[0].plot(Ekin,EDC)
		#axe[0].set_title(r'$h\nu$ = '+str(meta.hv))
		axe[0].axes.set_xlabel(r'$E_k$ (eV)')
		axe[0].axes.set_ylabel('Intensity')
		axe[0].axvline(Ekin[np.argmax(np.abs(diff))])
		axe[0].tick_params(direction='in', labelbottom=False)
		axe[0].ticklabel_format(style='sci', scilimits=(-2,3))
		axe[0].grid()

		deriv = axe[1].plot(Ekin,diff)
		axe[1].axes.set_xlabel(r'$E_k$ (eV)')
		axe[1].axes.set_ylabel(r'$\frac{dI}{dE_k}$')
		axe[1].axvline(Ekin[np.argmax(np.abs(diff))])
		axe[1].tick_params(direction='in', labelleft=False)
		axe[1].ticklabel_format(style='sci', scilimits=(-2,3))
		axe[1].grid()
		plt.show()
		Ef = Ekin[np.argmax(np.abs(diff))]

	except ValueError:
		Ef = meta.hv-5

	return Ef

def plot_withCounting(wave, elim, diff = laplace):
	class Index(object):
		sigma = 4
		tlim = [8,15]

		def plot(self):
			lims = [[i.get_xlim(), i.get_ylim()] for i in ax]

			opt, mis = count_peaks_threshold(df, meta, sigma = self.sigma, step = 1, tlim = self.tlim, elim = lims[0][1], diff = laplace)
			sig =  integ_angle(diff(gaussian_filter(df, sigma), meta), meta, self.tlim[0], self.tlim[1])
			partition = (min(lims[0][1])<meta.Ekin)*(meta.Ekin<=max(lims[0][1]))
			a = meta.Ekin[partition]
			b = sig[partition]

			ax[0].clear()
			ax[1].clear()
			ax[0].plot(sig,meta.Ekin)
			ax[0].plot(b[mis[2]], a[mis[2]],'.')
			#if type(opt) != type(1):
			#	ax[0].vlines(opt[0], min(meta.Ekin), max(meta.Ekin), 'g')
			ax[1].axvline(self.tlim[0],'r')
			ax[1].axvline(self.tlim[1],'r')
			ax[1].pcolormesh(meta.angle, meta.Ekin,
							 diff(gaussian_filter(df, self.sigma), meta),
							 shading = 'auto')
			ax[1].set_xlim(lims[1][0])
			ax[1].set_ylim(lims[1][1])
			ax[0].tick_params(direction='in', top=True, right=True)
			ax[1].tick_params(direction='in', top=True, right=True)
			ax[0].ticklabel_format(style='sci', scilimits=(-2,3))

			for slider in slider2, slider3 :
				slider.valmin, slider.valmax = lims[1][0] #adapt theta sliders to the view
				slider.ax.set_xlim(slider.valmin, slider.valmax)

			if type(opt) != type(1):
			 	figure, axe = plt.subplots(1,1)
			 	axe.set_xlabel('Threshold Value (%$I_{max}$)')
			 	axe.set_ylabel('Number of peaks')
			 	axe.plot(mis[0], mis[1])
			 	axe.hlines(opt[1], axe.get_xlim()[0], axe.get_xlim()[1])
			 	plt.show()

			print('New elim : '+str(lims[0][1]))
			print('Number of peaks: '+str(opt[1]))

		def change_sigma(self, event):
			self.sigma = slider1.val
			self.plot()

		def change_lims(self, event):
			self.tlim = [slider2.val, slider3.val]
			self.plot()

	df = wave.data
	meta = wave.metadata[0]
	#-Related to data
	sigma = 4
	tlim = [8,15]
	opt, mis = count_peaks_threshold(df, meta, sigma = sigma, step = 1, tlim = tlim, elim = elim, diff = laplace)
	partition = (elim[0]<meta.Ekin)*(meta.Ekin<=elim[1])

	sig =  integ_angle(diff(gaussian_filter(df, sigma), meta), meta, tlim[0], tlim[1])
	a = meta.Ekin[partition]
	b = sig[partition]

	#-Related to plotting Parameters:

	fig = plt.figure()
	gs = fig.add_gridspec(3, 1, height_ratios=[0.02,1, 0.2])
	sub = gs[1].subgridspec(2,2, height_ratios=[1,0.05])
	subsub = sub[1,1].subgridspec(2,1)

	ax = [fig.add_subplot(sub[0,0])]
	ax.append(fig.add_subplot(sub[0,1], sharey = ax[0]))

	ax[0].plot(b,a)
	ax[0].plot(b[mis[2]], a[mis[2]],'.')
	ax[1].axvline(tlim[0],'r')
	ax[1].axvline(tlim[1],'r')
	ax[1].pcolormesh(meta.angle, a, 
					 diff(gaussian_filter(df, sigma), meta)[:][partition],
					 shading = 'auto')

	#-Related to interactivity

	rects = [fig.add_subplot(gs[0]),
			 fig.add_subplot(subsub[0]),
			 fig.add_subplot(subsub[1]),
			 fig.add_subplot(gs[2])]

	slider1 = Slider(rects[0], 
						 label = '$\\sigma$', 
					   	 valmin = 0,valmax = 20, 
					  	 valinit = sigma, valstep = 0.5)
	slider2 = Slider(rects[1], 
						 label = '$\\theta_{min}$', 
					   	 valmin = min(meta.angle),valmax = max(meta.angle), 
					  	 valinit = min(meta.angle), valstep = meta.angle[1]-meta.angle[0])
	slider3 = Slider(rects[2], 
						 label = '$\\theta_{max}$', 
					   	 valmin = min(meta.angle),valmax = max(meta.angle), 
					  	 valinit = max(meta.angle), valstep = meta.angle[1]-meta.angle[0])

	button = Button(rects[3], 'MENU')

	callback = Index()

	slider1.on_changed(callback.change_sigma)
	slider2.on_changed(callback.change_lims)
	slider3.on_changed(callback.change_lims)

	open_menu = lambda event: menu(event, [], [slider1, slider2, slider3])
	button.on_clicked(open_menu)

	plt.show()

def gen_mapXZ(x, z, intensity, ax):
	"""
	Generates the matplotlib axe from intensity and x, z vectors to grid objects.
	
	
	Parameters:
		x (array):
			1D array that contains all x positions
		z (array):
			1D array that contains all z positions
		intensity (array):
			1D array with all intensities
		ax (matplotlib.axes.Axes):
			the matplotlib axe object where the plot is drawn
	Returns:
		plot (matplotlib.axes.Axes.scatter):
			scatter plot of the intensity map
	"""
	#grid_x, grid_z = make_grid(x, z, len(intensity))
	#X, Z = np.meshgrid(grid_x, grid_z)
	#print('Shape grid :'+str(X.shape))
	#ax.pcolormesh(X, Z, intensity.reshape(X.shape),antialiased=False, cmap='hot', edgecolors='k', linewidths=4)
	plot =  ax.scatter(x,z,c = intensity, cmap='hot')
	for i in range(len(x)):
		#ax.text(x[i], z[i], str(i+1), color = 'white')
		pass
	# from scipy.interpolate import Rbf

	# spacingz = find_N(z)

	# diffx = x[1:]-x[:-1]
	# j = 0
	# i = diffx[j]
	# while i>0 :
	# 	i = diffx[j]
	# 	j = j+1

	# spacingx = j+1

	# xi, zi = np.linspace(x.min(), x.max(), spacingx), np.linspace(z.min(), z.max(), spacingz)
	# XI, ZI = np.meshgrid(xi, zi)
	# inter = Rbf(x, z, intensity, method='nearest')
	# Imap = inter(XI,ZI)
	# plot = ax.pcolormesh(XI,ZI,Imap, cmap = cm.hot)

	return plot

def plot_mapXZ(wave, reduc_func):
	"""
	Interactive spatial map summarising 
	
	Parameters:
		wave (Wave):
			3D wave, spectral data + metadata
		x (array):
			1D array that contains all x positions
		z (array):
			1D array that contains all z positions
		reduc_func (function):
			(R**(len(x)+len(z))) -> R+
			Reduction function that maps a positive real value which summarizes the content
			of spectrum
	"""

	dfs = wave.data.reshape(wave.data.shape[0]*wave.data.shape[1], *wave.data.shape[2:])*1000
	metas = wave.metadata[0]

	DOS_E = np.sum([np.sum(data, axis = 1)/np.sum(data, axis = (0,1)) for data in dfs], axis = 0)
	DOS_A = np.sum([np.sum(data, axis = 0)/np.sum(data, axis = (0,1)) for data in dfs], axis = 0)

	x, z = np.array([[i,j] for i in wave.axes[1] for j in wave.axes[0]]).T

	#-Related to data
	#intensity = condense_maps(reduc_func, dfs)
	#grid_xz = [[x[i],z[i]] for i in range(len(dfs))]

	grid_xz = np.meshgrid(wave.axes[0], wave.axes[1])

	#-Related to plotting
	plt.style.use('dark_background')
	fig = plt.figure(1,constrained_layout = True, figsize = (8,8))
	gs = fig.add_gridspec(5, 4, height_ratios = [1, 0.25, 0.05, 0.05, 0.05], 
								width_ratios = [0.05, 0.05, 0.25, 1])
	ax = [fig.add_subplot(gs[0,3]),	#main plot
		  fig.add_subplot(gs[1,3]),	#DOS_E
		  fig.add_subplot(gs[2,3]), #slider Emin
		  fig.add_subplot(gs[3,3]), #slider Emax
		  fig.add_subplot(gs[4,3]), #slider sigma
		  fig.add_subplot(gs[0,2]), #DOS_A
		  fig.add_subplot(gs[0,1]), #slider Amin
		  fig.add_subplot(gs[0,0])] #slider Amax

	emin = wave.axes[-2].min()
	emax = wave.axes[-2].max()
	de = wave.axes[-2][1]- wave.axes[-2][0]

	amin = wave.axes[-1].min()
	amax = wave.axes[-1].max()
	da = wave.axes[-1][1]- wave.axes[-1][0]

	slider1 = Slider(ax[2], 
					 label = '$E_{min}$', 
				   	 valmin = emin, valmax = emax, 
				  	 valinit = emin, valstep = de)
	slider2 = Slider(ax[3], 
					 label = '$E_{max}$', 
				   	 valmin = emin, valmax = emax, 
				  	 valinit = emax, valstep = de)
	slider3 = Slider(ax[4], 
					 label = '$\\sigma$', 
				   	 valmin = 0, valmax = 20, 
				  	 valinit = 5, valstep = 1)

	slider4 = Slider(ax[6], 
					 label = '$\\theta_{min}$', 
				   	 valmin = amin, valmax = amax, 
				  	 valinit = amin, valstep = da, orientation="vertical")

	slider5 = Slider(ax[7], 
					 label = '$\\theta_{max}$', 
				   	 valmin = amin, valmax = amax, 
				  	 valinit = amax, valstep = da, orientation="vertical")

	plt.style.use('default')

	ax[1].plot(wave.axes[-2], DOS_E)
	ax[1].set_xlim(emin,emax)
	lines_E = [ax[1].axvline(slider1.val),
			ax[1].axvline(slider2.val)]

	ax[5].plot(DOS_A, wave.axes[-1])
	ax[5].set_ylim(amin,amax)
	lines_A = [ax[5].axhline(slider4.val),
			ax[5].axhline(slider5.val)]
	#-Related to interactivity

	class Index(object):												#Definition of the event handler object
		
		def __init__(self):
			self.bigplot = None
			self.cmap = cm.hot

		def change_boundaries(self, event):
			if self.bigplot is not None:
				self.cmap = self.bigplot.cmap
			ax[0].clear()
			#reduc = lambda x: reduc_func(x, axes, 
			#							[slider1.val, slider2.val, None, None], 
			#							 slider3.val)
			intensity = []
			for dat in dfs:
				intensity += [reduc_func(dat[::5,::5],	#data
										 [wave.axes[-2][::5],wave.axes[-1][::5]],	#axes
										 [slider1.val, slider2.val, slider4.val, slider5.val],	#limits
										 slider3.val)]	#sigma

			#intensity = condense_maps(reduc, dfs)
			self.bigplot = gen_mapXZ(x, z, intensity, ax[0])
			if self.bigplot is not None:
				self.bigplot.cmap = self.cmap

			for line,val in zip(lines_E, [slider1.val, slider2.val]):
				line.set_xdata(val)

			for line,val in zip(lines_A, [slider4.val, slider5.val]):
				line.set_ydata(val)

	def onclick(event, grid, dfs, figure, ax):							#This part  defines what happens when a click event occurs
		ix, iy = event.xdata, event.ydata 					  			#Coordinates of the pointer on the map (local coordinates)
		
		vec = np.array([ix,iy])
		
		pos_grid = np.abs(grid[0]-vec[1])+np.abs(grid[1]-vec[0]) 			#Distance matrix of the pointer to all the points, has a unique minimum
		distance = np.min(pos_grid)

		plot_threshold = np.abs(np.min(z)-np.max(z))/len(wave.axes[1])
		if distance< 2*plot_threshold:
			print('Coordinates [x, y]: '+str([ix,iy]))
			index = np.argmin(pos_grid)
			print('Minimal distance :'+str(distance))
			print('Index :'+str(index))
			#print('Intensity :'+str(intensity[index]))
			plot_single(Wave(wave.metadata[0], dfs[index]))

		return ix, iy

	callback = Index()

	click_func = lambda event: onclick(event, grid_xz, dfs, fig, ax[0]) #Wrapper of onclick that only needs one parameter
	cid = fig.canvas.mpl_connect('button_press_event', click_func)
	slider1.on_changed(callback.change_boundaries)
	slider2.on_changed(callback.change_boundaries)
	slider3.on_changed(callback.change_boundaries)
	slider4.on_changed(callback.change_boundaries)
	slider5.on_changed(callback.change_boundaries)

	plt.show()

def plot_FS(wave, cmapi = cm.gray):
	"""
	Interactive Fermi surface map
	Possible options :
		- arbitrary cuts in the stack
		- angular/wave vector representation
		- data analysis
	
	Parameters:
		wave (Wave):
			3D wave, spectral data + metadata
		cmapi (cmap):
			Default colormap
	"""

	def set_Axes(ax, state, meta):
		ax[0].tick_params(direction='in', top=True, right=True)

		if state[0]*meta.kinetic == True :
			ax[0].axes.set_ylabel('$E_k$ (eV)')
		else :
			#ax[0].invert_yaxis()
			ax[0].axes.set_ylabel('$E-E_f$ (eV)')

		if state[1]*meta.angular == True :
			ax[0].axes.set_xlabel('$\\theta$ (DEG)')
		else :
			ax[0].axes.set_xlabel('k ('+r'$\AA^{-1}$'+')')
	
	meta = wave.metadata[0]
	dfs = wave.data
	#-Related to data
	if (meta.angular == True) and ('FS' in wave.mode) :
		gridTheta, gridRz = np.meshgrid(wave.axes[2], wave.axes[0])
		Eb,kX,kY=ang_to_k3D(wave.axes[1],
						wave.axes[2],
						wave.axes[0],
						meta.Rz_offset,
						meta.phi,
						meta.tilt,
						meta.work,
						meta.hv,
						0,
						meta.Ef)
	else :
		gridTheta, gridRz = np.meshgrid(wave.axes[2], wave.axes[0])
		Eb,kX,kY=np.meshgrid(wave.axes[1], wave.axes[2], wave.axes[0])

	sigma_default = 8
	graph = [dfs, laplace_all(dfs, wave.axes[1:], sigma_default)]

	#-Related to plot
	fig = plt.figure(figsize = (8,8), tight_layout = True)
	#fig.set_tight_layout(True)
	gs = fig.add_gridspec(2, 1, height_ratios = [1,0.15])
	sub = gs[1].subgridspec(2, 5, width_ratios = [1,0.20, 0.20, 0.20, 0.20])
	sub_cmap = gs[0].subgridspec(1, 3, width_ratios = [20, 1, 1])

	ax = fig.add_subplot(sub_cmap[0])
	rects=[fig.add_subplot(sub[0]),
		   fig.add_subplot(sub[5]),
		   fig.add_subplot(sub[1]), 
		   fig.add_subplot(sub[2]),
		   fig.add_subplot(sub[3]),
		   fig.add_subplot(sub[4]),
		   fig.add_subplot(sub[7]),
		   fig.add_subplot(sub_cmap[1]),
		   fig.add_subplot(sub_cmap[2])]

	valmax = np.max(dfs)

	slider1 = Slider(rects[0], label = '$E_k (eV)$', 
				   valmin = meta.Ekin.min() ,valmax = meta.Ekin.max(), 
				   valinit = meta.Ekin.min(), valstep = meta.Ekin[1]-meta.Ekin[0])
	slider2 = Slider(rects[1], label = '$\\sigma$', 
				   valmin = 0,valmax = 20, 
				   valinit = 0, valstep = 0.1)
	sliders = [Slider(rects[7], 
					 label = '$vmin$', 
				   	 valmin = 0,valmax = valmax, 
				  	 valinit = dfs[0].min(), valstep = 1, orientation = 'vertical'),
			   Slider(rects[8], 
					 label = '$vmax$', 
				   	 valmin = 0,valmax = valmax, 
				  	 valinit = dfs[0].max(), valstep = 1, orientation = 'vertical')]
	button1 = Button(rects[2], 'ang/k')
	button2 = Button(rects[3], 'f(x)')
	button3 = Button(rects[4], 'cut')
	button4 = Button(rects[5], 'set $\\Gamma$')
	button5 = Button(rects[6], 'SAVE')

	#-Related to interactivity
	
	class Index(object):

		state = np.array([False, False, False, 0])	#order [ang, ren, diff, mode]
		i = 0
		current = graph[0]

		names = [['ang','$k$'],
				 ['def','$\\nabla^2$'],
				 ['def','Curv1D']]
		vmin = np.array([0,0])
		vmax = np.array([np.max(current),np.max(current)])
		old_vmin = vmin
		old_vmax = vmax
		first = True
		mainPlot = None
		sigma = sigma_default

		kX_local,kY_local = kX,kY
		#interpolant = generate_interpolant(dfs, Rz, np.meshgrid(metas[0].Ekin,metas[0].angle))
		interpolant = wave.interpolant()

		second_wave = copy.deepcopy(wave)
		second_wave.data = graph[1]

		interpolants = [interpolant, second_wave.interpolant()]

		def plot(self):

			if not(self.first):
				self.lims = [ax.get_xlim(), ax.get_ylim()]

			ax.clear()
			if self.state[1] :
				self.current = graph[1]
			else :
				self.current = graph[0]

			if self.mainPlot != None:
				self.cmap = self.mainPlot.cmap
				if any(self.vmin > self.vmax) == any(self.old_vmin < self.old_vmax):
					self.cmap = self.cmap.reversed()
			else:
				self.cmap = cmapi

			if self.state[0]*meta.angular :
				self.mainPlot = ax.pcolormesh(gridTheta, gridRz,
							  self.current[:,self.i,:],
							  cmap=self.cmap,
							  antialiased=False,
							  vmin=self.vmin[self.state[1]], 
							  vmax=self.vmax[self.state[1]],
							  shading = 'auto')
				ax.axes.set_ylabel('$\\theta$ (DEG)')
				ax.axes.set_xlabel('$\\alpha$ (DEG)')

			else :
				self.mainPlot = ax.pcolormesh(self.kX_local[:,self.i,:], self.kY_local[:,self.i,:], 
							  self.current[:,self.i,:].T,
							  cmap=self.cmap,
							  antialiased=False,
							  vmin=self.vmin[self.state[1]], 
							  vmax=self.vmax[self.state[1]],
							  shading = 'auto')
				ax.axes.set_xlabel(r'$k_x$'+' ('+r'$\AA^{-1}$'+')')
				ax.axes.set_ylabel(r'$k_y$'+' ('+r'$\AA^{-1}$'+')')

			ax.axis('scaled')
			if not(self.first):
				ax.set_xlim(self.lims[0])
				ax.set_ylim(self.lims[1])
			else :
				self.first = False
			fig.canvas.draw()

		def switch_ang(self, event):
			self.prevstate = copy.deepcopy(self.state)
			self.state[0] = not(self.state[0])
			self.first = True
			self.plot()
			button1.label.set_text(self.names[0][self.state[0]*meta.angular])

		def switch_ren(self, event):
			self.prevstate = copy.deepcopy(self.state)
			self.state[1] = not(self.state[1])
			self.plot()
			button2.label.set_text(self.names[1][self.state[1]])

		def set_gamma(self, event):
			print_formatted('Setting gamma mode',1)
			point = plt.ginput(n = 1, timeout = 0)
			if self.state[0]:
				tilt, Rz_offset = point[0]
				for i in wave.metadata:
					i.tilt = tilt
					i.Rz_offset = Rz_offset
			else:
				tilt, Rz_offset = point[0]
				tilt = k_to_ang(wave.axes[2][self.i], tilt, 0, 0)[1]
				Rz_offset = k_to_ang(wave.axes[2][self.i], Rz_offset, 0, 0)[1]

				for i in wave.metadata:
					i.tilt += tilt
					i.Rz_offset += Rz_offset

			print('Selected point : '+str(point[0]))
			print('tilt = '+str(wave.metadata[0].tilt))
			print('Rz_offset = '+str(wave.metadata[0].Rz_offset))
			print('tilt = '+str(meta.tilt))
			print('Rz_offset = '+str(meta.Rz_offset))
			try:
				Eb,self.kX_local,self.kY_local=ang_to_k3D(wave.axes[1],
									wave.axes[2],
									wave.axes[0],
									meta.Rz_offset,
									meta.phi,
									meta.tilt,
									meta.work,
									meta.hv,
									0,
									meta.Ef)
			except :
				print('Error ?')
			self.interpolant = wave.interpolant()
			self.plot()
			self.first = True

		def make_cut(self, event):

			print_formatted('New FS cut',1)
			points = plt.ginput(n = -1, timeout = 0)

			if len(points) != 2:
				points.append(points[0])	#Close the polygon generated by the points
	
			print('\nList of selected points:')
			count = 0
			for point in points :
				print('point('+str(count)+')'+' : '+str(point))
				count += 1
			print('\n')

			self.interpolants[1] = self.second_wave.interpolant()
			names = ['','lap']

			N_points = 100

			count = 0
			savepath = filedialog.asksaveasfile().name
			for interpol,iname in zip(self.interpolants,names):
				offset = 0
				fig2, ax2 = plt.subplots(tight_layout = True, figsize = (6,6))
				cut_waves = []
				for i in range(1,len(points)):
					print('Path from: '+repr(i-1)+' to '+repr(i))
					if self.state[0] or not(meta.angular):
						center = np.array([wave.metadata[0].tilt, wave.metadata[0].Rz_offset])
						path = generate_path(np.array(points[i-1])-center,np.array(points[i])-center,N_points)
					else:
						path = generate_path(points[i-1],points[i],N_points)						

					if self.state[0] or not(meta.angular):#
						new_cut, new_meta = generate_fermi_cut_ang(interpol, meta, path)
					else:
						new_cut, new_meta = generate_fermi_cut(interpol, meta, path)

					# ax2.pcolormesh(new_meta.angle+offset,
					# 			  meta.Ekin-meta.Ef,
					# 			  new_cut,
					# 			  cmap=self.cmap,
					# 			  antialiased=False,
					# 			  vmin=self.vmin[self.state[1]], 
					# 			  vmax=self.vmax[self.state[1]],
					# 			  shading = 'auto')
					# set_Axes([ax2], self.state[:-1], meta)
					# configure_plot(ax2,6)

					offset += new_meta.angle[-1]
					print('Offset for next path: '+str(offset))
					WF = Wave(new_meta, new_cut)
					cut_waves.append(WF)
					#WF.save('./FS_extract_'+str(i)+'.jexp')
					WF.save_txt(savepath+'FS_extract_'+iname+'_'+str(count))
					print('\n')
					count+=1
				final_WV = concatenate_spectra(cut_waves)

				#plot_single(final_WV)

				save_plot(final_WV,
					      [np.min(final_WV.metadata[0].angle), np.max(final_WV.metadata[0].angle)],
					      [np.min(final_WV.metadata[0].Ekin), np.max(final_WV.metadata[0].Ekin)],
					      savepath+'FS_cut_'+iname+'_', sigma = self.sigma, kinetic = True, angular = True, 
					      vmin = 0, vmax = 8*np.mean(final_WV.data), cmap = self.cmap, show = False)

				# ax2.pcolormesh(final_WV.metadata[0].angle,
				# 				  final_WV.metadata[0].Ekin + final_WV.metadata[0].Ef,
				# 				  final_WV.data,
				# 				  cmap=self.cmap,
				# 				  antialiased=False,
				# 				  vmin=self.vmin[self.state[1]], 
				# 				  vmax=self.vmax[self.state[1]],
				# 				  shading = 'auto')
				# set_Axes([ax2], self.state[:-1], final_WV.metadata[0])
				# configure_plot(ax2,6)
				#final_WV.save_txt('./FS_extract_'+iname+'_CONCAT')

				#fig2.show()

		def save_slice(self, event):
			count = 0
			savepath = filedialog.asksaveasfile().name
			for pic in False, True:
				for show_axis in False, True:
					if show_axis:
						fig2, ax2 = plt.subplots(tight_layout = True, figsize = (6,6))
					else:
						fig2 = plt.figure(figsize = (6,6))
						ax2 = fig2.add_axes([0,0,1,1])
					if self.state[0]*meta.angular :
						ax2.pcolormesh(gridTheta, gridRz, 
								   graph[pic][:,self.i,:],
								   shading = 'auto',
								   cmap = self.cmap,
								   vmin=list(self.vmin)[pic], 
								   vmax=list(self.vmax)[pic])
					else:
						ax2.pcolormesh(self.kX_local[:,self.i,:],
								   self.kY_local[:,self.i,:], 
								   graph[pic][:,self.i,:].T,
								   shading = 'auto',
								   cmap = self.cmap,
								   vmin=list(self.vmin)[pic], 
								   vmax=list(self.vmax)[pic])
					xlims = ax.get_xlim()
					ylims = ax.get_ylim()
					ax2.axis('scaled')
					ax2.set_xlim(ax.get_xlim())
					ax2.set_ylim(ax.get_ylim())

					x = np.mean(ax2.get_xlim())
					y = ax2.get_ylim()[1]-0.1*(ax2.get_ylim()[1]-ax2.get_ylim()[0])
					#ax2.text(x,y,'$E_k = '+str(meta.Ekin[self.i])+' eV$',color = 'white')

					name = savepath+'FS_slice_Ek'+str(meta.Ekin[self.i])+'_'+str(self.i)+'_'
					while os.path.exists(savepath+name+str(count)):
						count+=1
					
					if show_axis:
						configure_plot(ax2, 6)
						ax2.axes.set_xlabel(r'$k_x$'+' ('+r'$\AA^{-1}$'+')')
						ax2.axes.set_ylabel(r'$k_y$'+' ('+r'$\AA^{-1}$'+')')
						wave.save_txt(savepath+name+str(count), index = self.i)
					else :
						ax2.axis('scaled')
						ax2.axis('off')
						ax2.set_frame_on(False)

					fig2.savefig(savepath+'FS_E_'+str(meta.Ekin[self.i])+'_lap_'+str(pic)+'_axis_'+str(show_axis)+'.png', bbox_inches="tight")
					plt.close(fig2)

		def change_contrast(self, event):
			self.old_vmax = self.vmax
			self.old_vmin = self.vmin
			self.vmin[self.state[1]] = sliders[0].val
			self.vmax[self.state[1]] = sliders[1].val
			self.plot()

		def change_sigma(self, event):
			#U = dfs.swapaxes(0,1)
			#graph[1] = laplace_all(U, wave.axes[[1,2]], slider2.val).swapaxes(0,1)
			self.sigma = slider2.val
			graph[1] = laplace_all(dfs, np.array(wave.axes, dtype=np.ndarray)[[0,2]],
				self.sigma)
			self.second_wave = copy.deepcopy(wave)
			self.second_wave.data = graph[1]
			self.plot()

		def update(self, event):
			self.i = int(np.argmin((slider1.val-meta.Ekin)**2))
			print('Energy :'+str(meta.Ekin[self.i]))
			self.plot()

	callback = Index()
	slider1.on_changed(callback.update)
	slider2.on_changed(callback.change_sigma)
	button1.on_clicked(callback.switch_ang)
	button2.on_clicked(callback.switch_ren)
	button3.on_clicked(callback.make_cut)
	button4.on_clicked(callback.set_gamma)
	button5.on_clicked(callback.save_slice)
	sliders[0].on_changed(callback.change_contrast)
	sliders[1].on_changed(callback.change_contrast)
	plt.show()

def plot_hv(wave, cmapi = cm.gray):
	"""
	Interactive hv/kz map
	
	Parameters:
		wave (Wave):
			3D wave, spectral data + metadata
		cmapi (cmap):
			Default colormap
	"""

	
	#-Related to data

	metas = wave.metadata
	dfs = wave.data
	hv = np.array(wave.axes[0])
	#gridEk, gridHv = np.meshgrid(metas[0].Ekin, hv)

	#Ekin, theta = np.meshgrid(metas[0].Ekin, metas[0].angle)
	Ek = np.array([i.Ekin for i in metas])
	theta = np.array([i.angle for i in metas])

	graph = dfs

	#-Related to plot
	fig = plt.figure(figsize = (8,8), tight_layout = True)
	gs = fig.add_gridspec(2, 1, height_ratios = [1,0.06])
	sub = gs[1].subgridspec(2, 4, height_ratios = [1,1], width_ratios = [1, 0.20, 0.20, 0.20])
	sub_cmap = gs[0].subgridspec(1, 3, width_ratios = [20, 1, 1])

	ax = fig.add_subplot(sub_cmap[0])
	rects=[fig.add_subplot(sub[0]),
		   fig.add_subplot(sub[1]), 
		   fig.add_subplot(sub[2]),
		   fig.add_subplot(sub[3]),
		   fig.add_subplot(sub[4]),
		   fig.add_subplot(sub_cmap[1]),
		   fig.add_subplot(sub_cmap[2])]

	valmax = np.max(dfs[~np.isnan(dfs)])

	slider1=Slider(rects[0], label = 'Index', 
				   valmin = 0,valmax = len(hv)-1, 
				   valinit = 0, valstep = 1)

	sliders = [Slider(rects[5], 
					 label = '$vmin$', 
				   	 valmin = 0,valmax = valmax, 
				  	 valinit = 0, valstep = 1, orientation = 'vertical'),
			   Slider(rects[6], 
					 label = '$vmax$', 
				   	 valmin = 0,valmax = valmax, 
				  	 valinit = np.mean(dfs), valstep = 1, orientation = 'vertical')]
	button1 = Button(rects[1], 'flip')
	button2 = Button(rects[2], 'k')
	button3 = Button(rects[3], 'save')

	slider2=Slider(rects[4], label = '$V_0$', 
				   valmin = 0,valmax = 100, 
				   valinit = 10, valstep = 0.01)

	#textbox2 = TextBox(rects[4], '$V_0$', 10)

	#-Related to interactivity
	
	class Index(object):

		state = np.array([False, False, False, 0])	#order [ang, ren, diff, mode]
		i = 0
		mode = 0
		current = graph
		image = None
		current_axe_name = '$h\\nu$ (eV)'
		current_axe = hv
		complementary_axes = [None,None]
		mainPlot = None
		vmin = 0
		vmax = 0
		cmap = cmapi

		def plot_arpes(self, plotregion, ax):
			"""
			Axes theta, Ek, moving hv
			"""

			if self.mainPlot != None:
				self.cmap = self.mainPlot.cmap

			ax.clear()
			self.complementary_axes = [theta[self.i], Ek[self.i]]
			plotregion = ax.pcolormesh(theta[self.i], Ek[self.i],
						  self.current[self.i,:,:],
						  cmap=self.cmap,
						  antialiased=False,
						  vmin=self.vmin, vmax=self.vmax,
						  shading = 'auto')
			self.image = self.current[self.i,:,:]
			ax.axes.set_xlabel('$\\theta$ (DEG)')
			ax.axes.set_ylabel('$E_k - h\\nu$ (eV)')

			return plotregion

		def plot_hv_E(self, plotregion, ax):
			"""
			Axes theta, Ek, moving hv
			"""
			ax.clear()

			ax.plot(Ek[self.i],integ_angle(self.current[self.i,:,:]))
			ax.axes.set_ylabel('$Intensity$ (a.u.)')
			ax.axes.set_xlabel('$E_k - h\\nu$ (eV)')

			return plotregion

		def plot_hv_E_2(self, plotregion, ax):
			"""
			Axes Ek, hv, moving k
			"""
			ax.clear()

			if self.mainPlot != None:
				self.cmap = self.mainPlot.cmap

			self.complementary_axes = [hv,Ek[0]]
			plotregion = ax.pcolormesh(hv,Ek.T,self.current[:,:,self.i].T,
						  				  cmap=self.cmap,
						 				  antialiased=False,
						 				  vmin=self.vmin, vmax=self.vmax,
						  				  shading = 'auto')

			self.image = self.current[:,:,self.i].T

			ax.axes.set_ylabel('$E_k - h\\nu$ (eV)')
			ax.axes.set_xlabel('$h\\nu$ (eV)')

			return plotregion

		def plot_fermi(self, plotregion, ax):
			"""
			Axes hv, theta, moving Ek
			"""
			ax.clear()
			grid_hv, grid_theta = np.meshgrid(hv, theta[0])
			if not self.state[0]:
				grid_theta =[]
				for meta in wave.metadata :
					E, k =	ang_to_k(Ek[0][self.i]+meta.hv,
								 theta[0],
								 meta.Rz,
								 meta.Rz_offset,
								 meta.phi,
								 meta.tilt,
								 meta.work,
								 meta.hv,
								 0,
								 meta.Ef)
					grid_theta.append(k.T[0])
				grid_theta = np.array(grid_theta).T

				Ek_real = [Ek[0][self.i]+meta.hv for meta in wave.metadata]
				grid_theta, grid_hv = hv_to_kz(Ek_real,
											   theta[0],
											   wave.metadata[0].Rz,
											   wave.metadata[0].Rz_offset,
											   wave.metadata[0].phi,
											   wave.metadata[0].tilt,
											   wave.metadata[0].work,
											   wave.metadata[0].hv,
											   float(slider2.val),
											   wave.metadata[0].Ef)

				#grid_hv, B = np.meshgrid(hv, theta[0])

			if self.mainPlot != None:
				self.cmap = self.mainPlot.cmap

			plotregion = ax.pcolormesh(grid_hv.T, grid_theta.T,
						  self.current[:,self.i,:],
						  cmap=self.cmap,
						  antialiased=False,
						  vmin=self.vmin, vmax=self.vmax,
						  shading = 'auto')

			self.image = self.current[:,self.i,:]

			ax.axes.set_ylabel('$\\theta$ (DEG)')
			ax.axes.set_xlabel('$h\\nu$ (eV)')

			#ax.axis('equal')

			return plotregion

		plot_function = plot_arpes

		def flip(self, event):
			self.plot_functions = [self.plot_arpes, self.plot_fermi, self.plot_hv_E_2]
			self.mode = (self.mode+1)%len(self.plot_functions)

			print_formatted('Mode changed',1)

			#valm = self.current.shape[self.mode]-1

			valm = len(wave.axes[self.mode])-1
			self.current_axe = wave.axes[self.mode]
			self.current_axe_name = wave.axes_names[self.mode]

			# if self.mode == 0:
			# 	#valm = len(hv)-1
			# 	self.current_axe = hv
			# 	self.current_axe_name = 'hv'
			# # elif self.mode == 1:
			# # 	valm = len(hv)-1
			# # 	self.current_axe = hv
			# # 	self.current_axe_name = 'hv'
			# elif self.mode == 1:
			# 	#valm = len(Ek[0])-1
			# 	self.current_axe = Ek[0]
			# 	self.current_axe_name = 'Ek - hv'
			# elif self.mode == 2:
			# 	self.current_axe = theta[0]
			# 	self.current_axe_name = 'k'

			slider1.val = 0
			slider1.valmax = valm
			slider1.ax.set_xlim(slider1.valmin, slider1.valmax)
			print('New valmax = '+str(slider1.valmax)+'\n')
			self.plot_function = self.plot_functions[self.mode]

		def change_contrast(self, event):
			self.vmin = sliders[0].val
			self.vmax = sliders[1].val
			self.mainPlot = self.plot_function(self.mainPlot, ax)

		def switch_angular(self, event):
			self.state[0] = not(self.state[0])
			self.mainPlot = self.plot_function(self.mainPlot, ax)

		def update(self, event):
			self.i = int(slider1.val)
			print(self.current_axe_name+' : '+str(self.current_axe[self.i]))
			self.mainPlot = self.plot_function(self.mainPlot, ax)

		def save_slice(self, event):
			count = 0
			for pic in False, True:
				for show_axis in False, True:
					if show_axis:
						fig2, ax2 = plt.subplots(tight_layout = True, figsize = (6,6))
					else:
						fig2 = plt.figure(figsize = (6,6))
						ax2 = fig2.add_axes([0,0,1,1])

					saved_plot = None
					saved_plot = self.plot_function(saved_plot, ax2)

					xlims = ax.get_xlim()
					ylims = ax.get_ylim()

					ax2.set_xlim(ax.get_xlim())
					ax2.set_ylim(ax.get_ylim())

					name = './hv_slice'+str(self.current_axe[self.i])+'_'+str(self.i)+'_'
					while os.path.exists(name+str(count)):
						count+=1
					
					if show_axis:
						configure_plot(ax2, 6)
						#ax2.axes.set_xlabel(r'$k_x$'+' ('+r'$\AA^{-1}$'+')')
						#ax2.axes.set_ylabel(r'$k_y$'+' ('+r'$\AA^{-1}$'+')')
						if self.mode != 1:
							_to_igortxt('./'+name+str(count), self.complementary_axes[1], self.complementary_axes[0], self.image)
					else :
						#ax2.axis('scaled')
						ax2.axis('off')
						ax2.set_frame_on(False)

					fig2.savefig('hv_'+self.current_axe_name+'_'+str(self.current_axe[self.i])+'_lap_'+str(pic)+'_axis_'+str(show_axis)+'.png', bbox_inches="tight")
					plt.close(fig2)


	callback = Index()
	button1.on_clicked(callback.flip)
	button2.on_clicked(callback.switch_angular)
	button3.on_clicked(callback.save_slice)
	slider2.on_changed(callback.update)
	slider1.on_changed(callback.update)
	sliders[0].on_changed(callback.change_contrast)
	sliders[1].on_changed(callback.change_contrast)
	plt.show()

def plot_multi(wave, cmapi = cm.gray):
	"""
	Arbitrary 3D wave viewer with rotation of stack
	
	Parameters:
		wave (Wave):
			3D wave, spectral data + metadata
		cmapi (cmap):
			Default colormap
	"""
	#-Related to data

	metas = wave.metadata
	dfs = wave.data

	from cassiopy.user_interpret import XZ
	if XZ.match(wave.mode):
		hv = np.arange(len(wave.axes[0]))
	else:
		hv = wave.axes[0]
	#gridEk, gridHv = np.meshgrid(metas[0].Ekin, hv)

	#Ekin, theta = np.meshgrid(metas[0].Ekin, metas[0].angle)
	Ek = np.array([wave.axes[-2] for i in range(len(dfs))])
	theta = np.array([wave.axes[-1] for i in range(len(dfs))])
	#theta = np.array([i.angle for i in metas])

	graph = dfs

	#-Related to plot
	fig = plt.figure(figsize = (8,8), tight_layout = True)
	gs = fig.add_gridspec(2, 1, height_ratios = [1,0.06])
	sub = gs[1].subgridspec(2, 4, height_ratios = [1,1], width_ratios = [1, 0.20, 0.20, 0.20])
	sub_cmap = gs[0].subgridspec(1, 3, width_ratios = [20, 1, 1])

	ax = fig.add_subplot(sub_cmap[0])
	rects=[fig.add_subplot(sub[0]),
		   fig.add_subplot(sub[1]), 
		   fig.add_subplot(sub[2]),
		   fig.add_subplot(sub[3]),
		   fig.add_subplot(sub[4]),
		   fig.add_subplot(sub_cmap[1]),
		   fig.add_subplot(sub_cmap[2])]

	valmax = np.max(dfs[~np.isnan(dfs)])

	slider1=Slider(rects[0], label = 'Index', 
				   valmin = hv.min(),valmax = hv.max(), 
				   valinit = hv.min(), valstep = hv[1]-hv[0])

	sliders = [Slider(rects[5], 
					 label = '$vmin$', 
				   	 valmin = 0,valmax = valmax, 
				  	 valinit = 0, valstep = 1, orientation = 'vertical'),
			   Slider(rects[6], 
					 label = '$vmax$', 
				   	 valmin = 0,valmax = valmax, 
				  	 valinit = np.mean(dfs[~np.isnan(dfs)]), valstep = 1, orientation = 'vertical')]
	button1 = Button(rects[1], 'flip')
	button2 = Button(rects[2], 'k')
	button3 = Button(rects[3], 'cut')

	slider2=Slider(rects[4], label = '$V_0$', 
				   valmin = 0,valmax = 100, 
				   valinit = 10, valstep = 0.01)

	#textbox2 = TextBox(rects[4], '$V_0$', 10)

	#-Related to interactivity
	
	class Index(object):

		state = np.array([False, False, False, 0])	#order [ang, ren, diff, mode]
		i = 0
		mode = 0
		current = graph
		current_axe_name = wave.axes_names[mode]
		current_axe = hv
		mainPlot = None
		vmin = 0
		vmax = 0
		cmap = cmapi

		def plot_arpes(self):
			"""
			Axes theta, Ek, moving hv
			"""
			if self.mainPlot != None:
				self.cmap = self.mainPlot.cmap

			print(type(theta[self.i]), type(Ek[self.i]), type(self.current[self.i,:,:]),
				type(self.current[self.i][0][0]))

			ax.clear()
			self.mainPlot = ax.pcolormesh(theta[self.i], Ek[self.i],
						  self.current[self.i,:,:],
						  cmap=self.cmap,
						  antialiased=False,
						  vmin=self.vmin, vmax=self.vmax,
						  shading = 'auto')

			ax.axes.set_xlabel(wave.axes_names[(self.mode-1)%len(wave.axes_names)])
			ax.axes.set_ylabel(wave.axes_names[(self.mode+1)%len(wave.axes_names)])

		# def plot_hv_E(self):
		# 	"""
		# 	Axes theta, Ek, moving hv
		# 	"""
		# 	ax.clear()
		# 	ax.plot(Ek[self.i],integ_angle(self.current[self.i,:,:]))
		# 	ax.axes.set_ylabel('$Intensity$ (a.u.)')
		# 	ax.axes.set_xlabel('$E_k$ (eV)')

		def plot_fermi(self):
			"""
			Axes hv, theta, moving Ek
			"""
			ax.clear()
			grid_hv, grid_theta = np.meshgrid(hv, theta[0])

				#grid_hv, B = np.meshgrid(hv, theta[0])


			if self.mainPlot != None:
				self.cmap = self.mainPlot.cmap

			print(type(grid_hv.T[0]), type(grid_theta.T[0]))
			print(type(grid_hv.T), type(grid_theta.T), type(self.current[:,self.i,:]))
			print(type(self.current[:,self.i,:][0][0]))

			self.mainPlot = ax.pcolormesh(grid_hv.T, grid_theta.T,
						  self.current[:,self.i,:],
						  cmap=self.cmap,
						  antialiased=False,
						  vmin=self.vmin, vmax=self.vmax,
						  shading = 'auto')

			ax.axes.set_xlabel(wave.axes_names[(self.mode-1)%len(wave.axes_names)])
			ax.axes.set_ylabel(wave.axes_names[(self.mode+1)%len(wave.axes_names)])

			#ax.axes.set_ylabel('$\\theta$ (DEG)')
			#ax.axes.set_xlabel(wave.axes_names[self.mode])

		def plot_fermi2(self):
			"""
			Axes Ek, hv, moving theta
			"""
			ax.clear()
			grid_Ek, grid_hv = np.meshgrid(Ek[0], hv)

				#grid_hv, B = np.meshgrid(hv, theta[0])

			print(type(Ek[0]), type(hv))

			if self.mainPlot != None:
				self.cmap = self.mainPlot.cmap

			self.mainPlot = ax.pcolormesh(grid_hv, grid_Ek,
						  self.current[:,:,self.i],
						  cmap=self.cmap,
						  antialiased=False,
						  vmin=self.vmin, vmax=self.vmax,
						  shading = 'auto')

			ax.axes.set_ylabel(wave.axes_names[(self.mode-1)%len(wave.axes_names)])
			ax.axes.set_xlabel(wave.axes_names[(self.mode+1)%len(wave.axes_names)])

			#ax.axes.set_ylabel('$E_k$ (eV)')
			#ax.axes.set_xlabel(wave.axes_names[self.mode])
			#ax.axis('equal')

		plot_function = plot_arpes

		def flip(self, event):
			self.plot_functions = [self.plot_arpes, self.plot_fermi, self.plot_fermi2]
			self.mode = (self.mode+1)%len(self.plot_functions)

			print_formatted('Mode changed',1)

			valmin = wave.axes[self.mode].min()
			valmax = wave.axes[self.mode].max()
			try:
				dval = wave.axes[self.mode][1] - wave.axes[self.mode][0]
			except:
				dval = 1
			self.current_axe = wave.axes[self.mode]
			self.current_axe_name = wave.axes_names[self.mode]

			# if self.mode == 0:
			# 	valm = len(hv)-1
			# 	self.current_axe = hv
			# 	self.current_axe_name = wave.axes_name[self.mode]
			# elif self.mode == 2:
			# 	valm = len(theta[0])-1
			# 	self.current_axe = theta[0]
			# 	self.current_axe_name = wave.axes_name[self.mode]
			# elif self.mode == 1:
			# 	valm = len(Ek[0])-1
			# 	self.current_axe = Ek[0]
			# 	self.current_axe_name = 'Ek'

			slider1.val = valmin
			slider1.valmin = valmin
			slider1.valmax = valmax
			slider1.valstep = dval
			slider1.ax.set_xlim(slider1.valmin, slider1.valmax)
			print('New valmax = '+str(slider1.valmax)+'\n')
			self.plot_function = self.plot_functions[self.mode]

		def change_contrast(self, event):
			self.vmin = sliders[0].val
			self.vmax = sliders[1].val
			self.plot_function()

		def switch_angular(self, event):
			self.state[0] = not(self.state[0])
			self.plot_function()

		def update(self, event):
			self.i = int((np.argmin((slider1.val-self.current_axe )**2)))
			print(self.current_axe_name+' : '+str(self.current_axe[self.i]))
			self.plot_function()

	callback = Index()
	button1.on_clicked(callback.flip)
	button2.on_clicked(callback.switch_angular)
	slider2.on_changed(callback.update)
	slider1.on_changed(callback.update)
	sliders[0].on_changed(callback.change_contrast)
	sliders[1].on_changed(callback.change_contrast)
	plt.show()

#----------------------------------------Saving tools

def save_plot(wave, tlim, elim, pathresults, sigma = 5, 
	diff = laplace, kinetic = False, angular = False, ren = False, derived = False, state = np.array([False, False, False, False]), 
	vmin = 0, vmax = False, cmap = cm.gray, save = True, resolution = [5,5], title = False, filetype = 'png', show = False):
	"""
	A function that plots a spectrum alone according to the state wished.
	
	Parameters:
		wave (Wave):
			2D spectrum data + metadata
		ax (matplotlib.axes.Axes):
			The matplotlib axe object where the plot is drawn
		state (list):
			Defines the state of the plot
		diff (function):
			function applied on signal (e.g. second derivative)
		vmin,vmax (float):
			minimum and maximum values for color scale
		cmap (cmap):
			default color scale
		save (bool):
			saving image
		resolution (list):
		title (str):
			name of the spectrum
		filetype (str):
			extension for saving
		show (bool):
			showing image
	Returns:
		mainPlot (matplotlib.axes.Axes.pcolormesh)

	Notes:
		```
		state[0] controls the energy range (kinetic - True, Binding - False) overriden by _kinetic_
		state[1] controls the whether it is angular or k space (k - False, Angular - True) overriden by _angular_
		state[2] controls the whether the bare or renormalised data is used (bare - False, renormalised - True) overriden by _ren_
		state[3] controls the whether the second derived data (with gaussian smoothing sigma) is used (bare - False, derived - True) overriden by _derived_
		```
	"""

	state = [kinetic, angular, ren, derived]

	def set_Axes(ax, state):
		ax.tick_params(direction='in', top=True, right=True)
		ax.ticklabel_format(style='sci', scilimits=(-2,3))

		if state[0]*meta.kinetic == True :
			ylabel = '$E_k$ (eV)'
			ax.axes.set_ylabel('$E_k$ (eV)')
		else :
			#ax[0].invert_yaxis()
			ylabel = '$E-E_f$ (eV)'
			ax.axes.set_ylabel('$E-E_f$ (eV)')

		if state[1]*meta.angular == True :
			xlabel = '$\\theta$ (DEG)'
			ax.axes.set_xlabel('$\\theta$ (DEG)')
		else :
			xlabel = '$k_{||}$ ('+r'$\AA^{-1}$'+')'
			ax.axes.set_xlabel('$k_{||}$ ('+r'$\AA^{-1}$'+')')

	df = wave.data
	meta = wave.metadata[0]

	try:
		Eb, k = ang_to_k(meta.Ekin,
					 meta.angle,
					 meta.Rz,
					 meta.Rz_offset,
					 meta.phi,
					 meta.tilt,
					 meta.work,
					 meta.hv,
					 0,
					 meta.Ef)
	except FloatingPointError :
		print('There was an error in wave vector coordinates calculation, please check kinetic and angular metadatas with "detail" command\n')
		Eb,k = np.meshgrid(meta.Ekin, meta.angle)

	Ekin, theta = np.meshgrid(meta.Ekin, meta.angle)
	kinetic = [-Eb, Ekin]
	ang = [k, theta]

	try:
		ren = [df.T, renormalise(df).T]	#Transposition is necessary to match meshgrid
	except:
		ren = [df.T, df.T]
	deriv =	[diff(gaussian_filter(df, sigma), wave.axes[:2]).T, diff(gaussian_filter(ren[1].T, sigma), wave.axes[:2]).T]
	graph = [ren, deriv]
	
	size = resolution

	vmax = int(vmax)+np.max(graph[state[3]][state[2]])*int(not(vmax))
	if vmin > vmax :
		cmap = cmap.reversed()
		vmin, vmax = np.min(vmin, vmax), np.max(vmin, vmax)

	fig, ax = plt.subplots(1,1, tight_layout = True, figsize = size)
	mainPlot = ax.pcolormesh(ang[state[1]],
							kinetic[state[0]],
							graph[state[3]][state[2]],
							cmap=cmap,
							antialiased=False,
							vmin=vmin, 
							vmax=vmax,
							shading = 'auto')
	ax.set_xlim(tlim)
	ax.set_ylim(elim)
	set_Axes(ax, state)
	configure_plot(ax, size)
	norm = colors.Normalize()
	#fig.colorbar(mappable = cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax)

	if title :
		#title = meta.title
		ax.axes.set_title(meta.sample+meta.title)

	if save :
		i = 0
		name = pathresults+meta.title+'_'+diff.__name__+'_sigma_'+'%.1f'%(sigma)+'_index_'+str(i).zfill(3)
		name = name.replace('\n','__').replace('.','dot')
		while os.path.exists(name):
			i+=1
			name = pathresults+meta.title+'_'+diff.__name__+'_sigma_'+'%.1f'%(sigma)+'_index'+str(i).zfill(3)
			name = name.replace('\n','__').replace('.','dot')
			print(i)
		fig.savefig(name, format = filetype)
		ax.axis('off')
		fig.savefig(name+'_no_border', format = filetype, bbox_inches='tight')
		#tex_pic = to_tex(name+'_pfg', xlabel = 'xlabel', ylabel = 'ylabel', lims = [tlim,elim])
		plt.close(fig)

	if show:
		plt.show()
	plt.close(fig)

def plot_bands(wave, bands, state = np.array([False, False]), filetype = 'png', lims = [], add = '', show = True, **kwargs):
	
	df = wave.data
	meta = wave.metadata[0]

	if 'size' in kwargs:
		size = kwargs['size']
	else:
		size = 5

	if 'figsize' in kwargs:
		figsize = kwargs['figsize']
	else:
		figsize = [5,5]

	figure = plt.figure(figsize = figsize, tight_layout = True)
	axe = figure.add_subplot()

	axe.axes.set_title(meta.title+' FIT ')

	if state[0]*meta.angular == True :
		axe.axes.set_ylabel('$E_k$ (eV)')
	else :
		#ax[0].invert_yaxis()
		axe.axes.set_ylabel('$E-E_f$ (eV)')
	if state[1]*meta.angular == True :
		axe.axes.set_xlabel('$\\theta$ (DEG)')		
	else :
		axe.axes.set_xlabel('k ('+r'$\AA^{-1}$'+')')

	axe.tick_params(direction='in', top=True, right=True)
	axe.ticklabel_format(style='sci', scilimits=(-2,3))
	axe.grid()

	try :
		Eb, k = ang_to_k(meta.Ekin,
						 meta.angle,
						 meta.Rz,
						 meta.Rz_offset,
						 meta.phi,
						 meta.tilt,
						 meta.work,
						 meta.hv,
						 0,
						 meta.Ef)
		Ekin, theta = np.meshgrid(meta.Ekin, meta.angle)
		kinetic = [-Eb, Ekin]
	except FloatingPointError:
		Ekin, theta = np.meshgrid(meta.Ekin, meta.angle)
		Eb, k = np.meshgrid(meta.Ekin, meta.angle)

		kinetic = [Eb, Ekin]
	
	ang = [k, theta]

	plt.pcolor(ang[not(meta.angular)],kinetic[not(meta.kinetic)], df.T, cmap = cm.plasma)

	if lims != []:
		elims = -meta.Ef + np.array(lims['elim'])
		plt.ylim(elims)

	N = len(bands)
	count = 0

	for band in bands :
		if len(band[band.columns[0]]) > 0.05*len(meta.angle):
			for col, color, marker, linestyle in ['band', ((1-count/N),0,0), 'x', 'none'], ['fit',(0,(1-count/N),(1-count/N)),'.', 'none']:
				if col in band.columns:
					try :
						E, ky = ang_to_k(band[col],
									 band['axis'],
									 meta.Rz,
									 meta.Rz_offset,
									 meta.phi,
									 meta.tilt,
									 meta.work,
									 meta.hv,
									 0,
									 meta.Ef,
									 'simple')

						band_kinetic = [-E,band[col]]
						band_ang = [ky,band['axis']]
					except:
						band_kinetic = [band[col],band[col]]
						band_ang = [band['axis'],band['axis']]

					axe.plot(band_ang[not(meta.angular)][::5],
							 band_kinetic[not(meta.kinetic)][::5],
							 marker = marker, markersize = 1,
							 color = color, label = col, linestyle = linestyle)
		count +=1
	configure_plot(axe, size)
	savepath = meta.title+'fit'+add
	figure.savefig(savepath, format = filetype)
	#figure.savefig(meta.title+'fit', format ='svg')
	if show :
		plt.show()
	else :
		plt.close(figure)

def save_hv(dfs, metas):
	hv = np.array([meta.hv for meta in metas])
	Ek = np.array([i.Ekin for i in metas])
	theta = np.array([i.angle for i in metas])

	size = 5

	for i in range(len(dfs)):
		figure, ax = plt.subplots(figsize = (size,size), tight_layout = True)
		figure2, ax2 = plt.subplots(figsize = (size,size), tight_layout = True)

		ax.pcolormesh(theta[i], Ek[i],
					  dfs[i,:,:],
					  cmap=cm.hot,
					  antialiased=False,
					  shading = 'auto')
		ax.axes.set_xlabel('$\\theta$ (DEG)')
		ax.axes.set_ylabel('$E_k$ (eV)')
		ax.axes.text(theta[i][0], Ek[i][-1]*0.90, r'$h\nu =$'+str(metas[i].hv), color = 'r')
		configure_plot(ax, size)

		df = integ_angle(dfs[i,:,:])
		df_s = gaussian_filter(df,4)
		#ddf_s = np.gradient(df_s)
		#ax2.plot(Ek[i],df)
		#ax2.plot(Ek[i],ddf_s/max(ddf_s), 'g--')
		ax2.plot(Ek[i],df_s, 'g--')
		ax2.axes.set_ylabel('Intensity (a.u.)')
		ax2.axes.set_xlabel('$E_k$ (eV)')
		ax2.axes.text(Ek[i][0], np.max(df)*0.90, r'$h\nu =$'+str(metas[i].hv), color = 'r')
		configure_plot(ax2, size)

		DF = pd.DataFrame({'E':Ek[i],'I':df_s, 'hv':metas[i].hv})
		DF.to_csv('hv_dos_'+str(i).zfill(3)+'.csv')

		#figure.savefig('hv_arpes_'+str(i).zfill(3))
		#figure2.savefig('hv_dos_'+str(i).zfill(3))
		#plt.show()
		plt.close('all')

def save_FS_cut(wave, points, pathresults, 
	cmap = cm.gray, save = True, resolution = [5, 5]):

	def set_Axes(ax):

		ax.tick_params(direction='in', top=True, right=True)
		ax.ticklabel_format(style='sci', scilimits=(-2,3))

		ax.axes.set_ylabel('$E-E_f$ (eV)')
		ax.axes.set_xlabel(r'$\Delta k($'+r'$\AA^{-1})$')

	dfs = wave.data
	meta = wave.metadata[0]

	#gridTheta, gridRz = np.meshgrid(meta.angle, Rz)
	Eb,kX,kY=ang_to_k3D(meta.Ekin,
						meta.angle,
						meta.axis3,
						meta.Rz_offset,
						meta.phi,
						meta.tilt,
						meta.work,
						meta.hv,
						0,
						meta.Ef)

	#interpolant = generate_interpolant(dfs, Rz, metas)
	interpolant = wave.interpolant()
	cuts = make_FS_cut(interpolant, meta, points, slices = 150)

	fig, ax = plt.subplots(1,1, constrained_layout=True, figsize = resolution)

	for cut in cuts:
		mainPlot = ax.pcolormesh(cut['axis'],
								meta.Ekin-meta.Ef,
								cut['spectrum'],
								cmap=cmap,
								antialiased=False,
								shading = 'auto')
	set_Axes(ax)
	norm = colors.Normalize()
	fig.colorbar(mappable = cm.ScalarMappable(norm=norm, cmap=cmap), ax=ax)	
	ax.axes.set_title(meta.sample+meta.title)
	if save :
		i = 0
		name = pathresults+meta.title+'_'+str(i).zfill(3)+'.png'
		while os.path.exists(name):
			i+=1
			name = pathresults+meta.title+'_'+str(i).zfill(3)+'.png'
			print(i)
		fig.savefig(name)
	plt.show()
	plt.close(fig)
