"""
formatting.py

Contains all functions to have a nicer interface on the terminal
"""

from time import sleep
import os

#---------------------------------Printing functions

def print_table(table):
	if ('__len__' in table.__class__.__dict__.keys() 
				and table.__class__ != str):
		toprint='{init},{end},{step},{len}'.format(init = '%.3f'%(table[0]),
													   end = '%.3f'%(table[-1]),
													   len = len(table),
													   step ='%.3f'%((table[-1]-table[0])/len(table)))
	else:
		toprint = 'Not a table'
	print(toprint)

def print_table_(table):
	try:
		if ('__len__' in table.__class__.__dict__.keys() 
					and table.__class__ != str):
			toprint='{init},{end},{step},{len}'.format(init = '%.3f'%(table[0]),
														   end = '%.3f'%(table[-1]),
														   len = len(table),
														   step ='%.3f'%((table[-1]-table[0])/len(table)))
		else:
			toprint = table.__str__()
	except TypeError:
		toprint = table.__str__()
	return toprint

def print_c(string, fill = ''):
	try:
		WINDOWS_SIZE = os.get_terminal_size()[0]
	except (OSError,ValueError):
		WINDOWS_SIZE = 80
	print('{:{fill}^{number}}'.format(string,number=WINDOWS_SIZE,fill=fill))

def print_formatted(string, importance):
	formats = {1:[30,'_'],
			   2:[40,'_'],
			   3:[60,'_']}
	print('\n'+string.ljust(*formats[importance])+'\n')

#---------------------------------Appareance

def clear():
	#print(os.name)
	os.system('cls' if os.name=='nt' else 'clear')

def splash_screen(program_name, version, git, logo):
	clear()
	from datetime import date
	year = date.today().year

	splashscreen = [program_name,
					version,
					'\n\n',
					'2019 - '+str(year)+' Raphael Salazar',
					'\n\n',
					 git]

	for line in logo.splitlines():
		print_c(line,' ')
	sleep(1)
	for i in splashscreen:
		print_c(i,'')
		sleep(0.5)
	sleep(1.5)
	clear()
	
def subprogram_start(program_name):
	try:
		WINDOWS_SIZE = os.get_terminal_size()[0]
	except (OSError,ValueError):
		WINDOWS_SIZE = 80

	splashscreen = ['\n',
					'{:_^{number}}'.format(program_name,number=WINDOWS_SIZE),'\n']
	for i in splashscreen:
		print(i)
	sleep(0.1)
