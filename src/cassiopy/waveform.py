"""
waveform.py
Contains all functions to define the "algebra" on waveforms

* class definitions : Meta, Wave and Experiments
* coordinate changes : angle <-> wave vector, Ek <-> Eb...
* wave algebra : merging, substract, add spectra
* save wave
"""

import pandas as pd
import json
import copy
from scipy.interpolate import RegularGridInterpolator as RGI
from cassiopy.formatting import print_table, print_table_
from cassiopy.coord_transform import *

#----------------------------------------Custom Classes

class Meta():
	"""
	Metadata storing class
	"""
	def __init__(self, metadata, axis):

		equivalences = {'Ekin':['Ekin','E','Eb','KE','BE'],
		 				'angle':['angle', 'k'],
		 				'work':['work'],		 				
		 				'hv':['Excitation Energy'],		 				
		 				'title':['title', 'Spectrum Name'],	
		 				'sample':['sample'],				
		 				'Ef':['Ef'],
		 				'Rz_offset':['Rz_offset'],
		 				'Temp':['Temp','T','temperature'],
		 				'comments':['Comments','comments', 'User Comment'],
		 				'all':['all'],	 				
		 				'V':['V','voltage'],
		 				'phi':['phi','phi (deg) '],		 				
		 				'tilt':['tilt','tilt (deg) '],		 				
		 				'Rz':['theta','Rz','theta (deg) '],		 				
		 				'x':['x','x (mm) '],
		 				'y':['y','y (mm) '],
		 				'z':['z','z (mm) '],
		 				'angular':['angular'],
		 				'kinetic':['kinetic'],
		 				'Number of Sweeps':['Number of Sweeps', 'Number of Scans'],
		 				'PE':['PE','Pass Energy'],
		 				'axes_names':['axes names', 'axes_names']
		 				}
		errors = {}
		for key in equivalences :
			for eqkey in equivalences[key]:
					errors[key] = True
					try:
						if str(metadata[eqkey]).isnumeric():
							try:
								self.__setattr__(key,
												 float(metadata[eqkey].replace('\t',''))
												 )
							except (ValueError, AttributeError):
								self.__setattr__(key,float(metadata[eqkey]))
						else:
							self.__setattr__(key,metadata[eqkey])
						errors[key] = False
						if not(errors[key]):
							break
					except KeyError:
						errors[key] = True

		for key in errors:
			if errors[key]:
				if key == 'title':
					self.__setattr__(key, 'Unknown_spectrum')
				if key == 'comments':
					self.__setattr__(key, '')
				if key == 'Ekin':
					if type(axis[0]) == np.ndarray:
						self.__setattr__(key, axis[0])
					else:
						pass
						#self.__setattr__(key, axis[0][0].to_numpy(float))
				if key == 'angle':
					if type(axis[1]) == np.ndarray:
						self.__setattr__(key, axis[1])
					else:
						pass
						#self.__setattr__(key, axis[1][0].to_numpy(float))
				if key == 'Rz_offset' :
					self.__setattr__(key, 20)
				if key == 'Temp':
					self.__setattr__(key, 300)
				if key in ('x','y','z','tilt', 'phi', 'Ef', 'work', 'Rz','hv'):
					self.__setattr__(key, 0)
				if key =='sample':
					self.__setattr__(key,'unknown')
				if key =='angular' or key =='kinetic':
					self.__setattr__(key,True)
				if key =='PE':
					self.__setattr__(key,'unknown')
				if key =='Number of Sweeps':
					self.__setattr__(key,'unknown')
		for key in 'tilt','Rz','x','y','z':
			if key in self.__dict__.keys():
				self.__setattr__(key,float(self.__getattribute__((key))))
				#print('DEBUG Meta: Rz', float(self.__getattribute__((key))) )

		if 'Region Name' in metadata.keys():
			self.title = str(self.title) + '\n' + str(metadata['Region Name'])
		
		self.axes = [self.Ekin, self.angle]
		self.axis3 = False
		self.all = metadata

	def __len__(self):
		return 0

	def get_attr_to_print(self, keys):
		toprint = ''
		for i in keys:
			if i in self.__dict__.keys():
				if ('__len__' in self.__dict__[i].__class__.__dict__.keys() 
					and self.__dict__[i].__class__ != str):
					#We look for the method 'len' inside the current object methods
					#objects with length 0 (numbers) will be printed entirely
					#objects that are not strings with length > 0  (arrays)
					#will have be printed only their first and last members

					toprint+= i + ' = ' 
					toprint+='{init},{end},{len}\n'.format(init = '%.3f'%(self.__dict__[i][0]),
														   end = '%.3f'%(self.__dict__[i][-1]),
														   len = len(self.__dict__[i]))
				else:
					toprint+= i + ' = ' +str(self.__dict__[i]).replace('\n','; ')+'\n'
		return toprint

	def __str__(self):

		toprint =  ''
		keys = ['title','sample']
		toprint+=self.get_attr_to_print(keys)

		if self.axis3 == False:
			for i,n in zip(self.axes, self.axes_names):
				toprint += '{name} = '.format(name = n)+print_table_(i)+'\n'

		toprint+='\nPHOTONS:\n'
		keys = ['hv', 'Ef']
		if all([key in self.__dict__.keys() for key in keys]) :
			toprint += 'hv:{hv}, Ef:{Ef}\n'.format(hv = self.__dict__['hv'], 
												  Ef = self.__dict__['Ef'])

		toprint+='\nMANIPULATOR:\n'
		keys = ['x', 'y', 'z']
		if all([key in self.__dict__.keys() for key in keys]) :
			toprint += 'x:{x},y:{y},z:{z}\n'.format(x = self.__dict__['x'],
													y = self.__dict__['y'], 
													z = self.__dict__['z'])
		keys = ['Rz_offset', 'tilt', 'phi']
		if all([key in self.__dict__.keys() for key in keys]) :
			toprint += 'theta:{th},tilt:{ti},phi:{phi}\n'.format(th = self.__dict__['Rz'], 
																 ti = self.__dict__['tilt'], 
																 phi = self.__dict__['phi'])
		keys = ['Temp']
		if all([key in self.__dict__.keys() for key in keys]) :
			toprint += 'Temp: {T}K\n'.format(T = self.__dict__['Temp'])

		toprint+='\nANALYZER:\n'

		keys = ['Number of Sweeps','PE']
		if all([key in self.__dict__.keys() for key in keys]) :
			toprint += 'Number of Sweeps:{N}\n'.format(N = self.__dict__['Number of Sweeps'])
			toprint += 'Pass Energy:{N}\n'.format(N = self.__dict__['PE'])

		toprint+='\nDATA:\n'

		keys = ['kinetic','angular','comments']
		toprint+=self.get_attr_to_print(keys)+'\n'


		for i in self.all:
			toprint+= '{Variable} = {Value}\n'.format(Variable = i, Value = print_table_(self.all[i]))

		#toprint+='\nALL:\n'+self.all.__str__()
		return toprint

class Wave():
	"""
	Data storing class
	"""
	def __init__(self, metadata, data, axis = None, mode = 'FS', axes_names = []):
		if len(metadata) == 0:
			self.metadata = [metadata]
		else :
			self.metadata = metadata

		self.data = data
		self.mode = mode
		self.axes_names = np.array(axes_names)
		self.title = self.metadata[0].title

		if type(axis) != type(None):
			#axis = np.array(axis, dtype=np.ndarray)
			# if axis.shape != len(self.axes_names):
			# 	#self.axes = axis
			#  	self.axes = [axis,
			#  				 self.metadata[0].Ekin,
			#  				 self.metadata[0].angle]
			# else :
			# 	print('DEBUG Wave def axis3 > 1')
			# 	self.axes = [axis,
			# 				 self.metadata[0].Ekin,
			# 				 self.metadata[0].angle]

			self.axes = np.array(axis, dtype=np.ndarray)

		else :
			self.axes = [self.metadata[0].Ekin, self.metadata[0].angle]

		#self.axes = np.array(self.axes, dtype=np.ndarray)

	def __len__(self):
		return len(self.data)

	def save(self, path):
		print('Saving to :\n'+path+' (jexp)')
		#file = open(path,'w')
		copyself = copy.deepcopy(self)
		DICT = naturalise(copyself)
		#json.dump(DICT, file)
		SERIES = pd.Series(DICT).to_json(path+'.jexp')
		#file.close()

	def save_txt(self, path, **kwargs):
		count = 0
		print('Saving to :\n'+path+str(count)+' (txt)')

		if not('index' in kwargs.keys()):
			_to_igortxt(path+str(count), self.axes[-2], self.axes[-1], self.data)
			if self.metadata[0].angular == True:
				count+=1
				n_df,n_meta = change_spectrum_coordinates(self.data, self.metadata[0])
				_to_igortxt(path+str(count)+'_k', n_meta.Ekin, n_meta.angle, n_df)
		else :
			index = kwargs['index']
			n_df, kx, ky = change_slice_coordinates(self, index)
			_to_igortxt(path+str(count), kx, ky, n_df)
		
	def interpolant(self):
		#if self.metadata.axis3 != []:
		if len(self.data.shape) == 3:
			if self.mode == 'FS':
				axes = [self.axes[0]-self.metadata[0].Rz_offset, 
						self.axes[1],
						self.axes[2]-self.metadata[0].tilt]
			else :
				axes = [self.axes[0], 
						self.axes[1],
						self.axes[2]-self.metadata[0].tilt]
		if len(self.data.shape) == 2:
			axes = [self.axes[0], self.axes[1]-self.metadata[0].tilt]
		return RGI(axes, self.data, bounds_error = False, fill_value = np.nan)

	def differentiate(self, method, smoothing):
		from scipy.ndimage import gaussian_filter

		new_data = []
		if len(self.metadata) == 1:
			meta = self.metadata[0]
			data = self.data
			new_data = method(gaussian_filter(data, smoothing), [meta.Ekin, meta.angle])
		else:
			for data, meta in zip(self.data, self.metadata):
				new_data.append(method(gaussian_filter(data, smoothing), [meta.Ekin, meta.angle]))

		self.og_data = self.data
		self.data = np.array(new_data)

	def apply_Ef(self, method):
		Efs = method(self.axes[-2])
		for metadata, Ef in zip(self.metadata, Efs):
			metadata.Ef = Ef
		self.Efs = Efs

	def reduce_dim(self, indexes):
		if type(indexes) != int:
			indexes = tuple(indexes)
		self.data = np.sum(self.data, axis = indexes)
		self.axes = np.delete(self.axes, indexes)
		self.axes_names = np.delete(self.axes_names, indexes)

	def __str__(self):
		toprint = ''
		if self.mode != 'single':
			toprint +='EXPERIMENT\n'
			toprint +='mode :{mode}\n'.format(mode = self.mode)
			toprint +='data shape:'+str(self.data.shape)+'\n'
			for i,n in zip(self.axes, self.axes_names):
				toprint += '{name} = '.format(name = n)+print_table_(i)+'\n'

			toprint +='\n'

			str_meta = str(self.metadata[0])		
		else:
			toprint +='data shape:'+str(self.data.shape)+'\n'
			for i,n in zip(self.axes, self.axes_names):
				toprint += '{name} = '.format(name = n)+print_table_(i)+'\n'
			str_meta = str(self.metadata)

		toprint +=str_meta
		
		return toprint

class Experiment():
	"""
	Experiment storing class
	"""
	def __init__(self, mode, name, path, waves, axes):
		self.waves = waves
		self.axes = axes #[energy, angle, axis3]
		self.name = name
		self.path = path
		self.mode = mode

	def __len__(self):
		return len(self.waves)

	def __repr__(self):
		string = 'Experiment name: '+self.name+'\n'
		string +='Location: '+self.path+'\n'
		string +='Experiment type/mode: '+self.mode+'\n'
		string +='N° of waves: '+str(len(self.waves))+'\n'

		return string

#----------------------------------------Class conversion

def naturalise(obj):
	"""
	Function that converts a class object to python native types 
	to allow conversion to json recursively

		ndarray -> list
		object -> dict
		scalar -> scalar
		string -> string

	Parameters:
		obj : object

	Returns:
		obj : dict or list or string or any scalar value
		Last iteration will always return a dict

	Examples:
		class SubObjType():
			self.value = 1
		
		class ObjType1():
		 	def __init__(self):
				self.table = numpy.linspace(0,5,6)
				self.SubObj = SubObjType()
		
		foo = ObjType1()

		naturalise(foo)
		>> {'table':[0,1,2,3,4,5], 'SubOjb':{value:1}}

	"""
	if '__dict__' in obj.__dir__():
		obj = obj.__dict__
		for i in obj:
			#print('dict naturalise')
			#print(i)
			obj[i] = naturalise(obj[i])
	if type(obj) == np.ndarray or type(obj) == list:
		if type(obj) == np.ndarray :
			obj = obj.tolist()
		for i in range(len(obj)):
			#print(obj[i])
			if str(obj[i]).isnumeric():
				#print(str(obj[i]).isnumeric())
				pass
			else :
				obj[i] = naturalise(obj[i])
	else:
		pass
	return obj

def _to_igortxt(path: str, axis1, axis2, df):
	"""
	Exports data to simple igor text format

	Parameters:
		path
		saving path
	axis1, axis2: array-like
		image axes
	df: array-like
		2D array
	"""
	file = open(path, 'w')
	file.write('title\n\t\t')
	for i in range(df.shape[1]):
		file.write('\t')
	file.write('\n\t\t')
	str_axis = str(list(axis2))
	str_axis = str_axis.replace(', ','\t').replace('[','').replace(']','')
	file.write(str_axis)

	for i in range(df.shape[0]):
		str_line = str(list(df[i]))
		str_line = str_line.replace(', ','\t').replace('[','').replace(']','')
		file.write('\n\t'+str(axis1[i])+'\t')
		file.write(str_line)
	file.close()

def to_tex(filename, xlabel, ylabel, lims):

	txt='\n\\begin{tikzpicture}\
				\n\t\\begin{axis}[\n\t\tenlargelimits=false,\
								  \n\t\taxis on top,\
								  \n\t\txlabel={xlabel},\
								  \n\t\tylabel={ylabel},\
								  \n\t\txticklabel pos=lower\n\t\t]\
								  \n\t\t\\addplot graphics \
								  [\n\t\t\txmin={xmin},xmax={xmax},\
								   \n\t\t\tymin={ymin},ymax={ymax}\n\t\t] \
								  {filename};\
				\n\t\\end{axis}\
		\n\\end{tikzpicture}\n'.format( xlabel = xlabel, 
										ylabel=ylabel, 
										xmin = min(lims[0]), 
										xmax = max(lims[0]),
										ymin = min(lims[1]),
										ymax = max(lims[1]),
										filename = '{'+filename+'}',
										tikzpicture = '{tikzpicture}',
										axis = '{axis}')

	return txt
#----------------------------------------Wave combination

def substract_spectra(dfs, *_):
	"""

	"""
	if dfs[0].shape == dfs[1].shape:
		new_df = dfs[0]/np.max(dfs[0])-dfs[1]/np.max(dfs[1])
		return new_df*1000

	else:
		raise ValueError('Incompatible shapes :'+str(dfs[0].shape) +'and' +str(dfs[1].shape))

def _heal_discontinuity(x,y,points):
	for i in points:
		if i == len(y):
			DI = (y[i-1]-y[i-2])
		elif i-1 < 0:
			DI = (y[i+1]-y[i])
		elif i+1 == len(y) :
			DI = (y[i]-y[i-1])
		else :
			DI = (y[i+1]-y[i-1])
		y = y - np.heaviside(x-x[i],0)*DI
	return y

def _locate_discontinuities(y):
	from scipy.signal import find_peaks
	dy1 = y[1:]-y[:-1]
	dy2 = np.gradient(y)
	peaks1 = find_peaks(dy1)
	peaks2 = find_peaks(dy2)
	peaks = list(peaks1[0])+list(peaks2[0])
	return [i+j for i in peaks for j in range(-4,4)]

def merge(waves):
	"""
	Parameters:
	
	Returns:
	
	"""

	dfs = [wave.data for wave in waves]
	metas = [wave.metadata[0] for wave in waves]
	axes = [wave.axes for wave in waves]

	print('Shapes :')
	print(dfs[0].shape)
	print(dfs[1].shape)
	if True:
		#Angular axis
		axes = [meta.angle -meta.tilt for meta in metas]
		bounds = [(min(axe),max(axe)) for axe in axes]
		print(bounds)
		bounds = np.reshape(bounds, (1,np.shape(bounds)[0]*np.shape(bounds)[1]))[0]
		#Reshaping the bounds in a line
		l = bounds[np.argmax(bounds)]-bounds[np.argmax(bounds)-1]
		#l is the length of a spectra
		#For indexing in increasing tilt order
		tilts = [meta.tilt for meta in metas]
		max_tilt = np.argmax(tilts)
		min_tilt = np.argmin(tilts)

		if max_tilt == min_tilt:
			max_tilt = 1
			min_tilt = 0


		#Energy axis
		eks = [meta.Ekin for meta in metas]
		bounds_ek = np.array([(min(axe),max(axe)) for axe in eks]).T
		N = int(np.ceil((min(bounds_ek[1]) - min(bounds_ek[0]))/(metas[0].Ekin[1]-metas[0].Ekin[0])))
		Ekin = np.linspace(max(bounds_ek[0]),
						   min(bounds_ek[1]),
						   N)
		
		waves =  [Wave(i[0], i[1]) for i in zip(metas, dfs)]
		interps = [wave.interpolant() for wave in waves]

		#Calculating L the length of the new spectra to conserve the initial step
		L = (np.max(bounds)-np.min(bounds))*len(metas[0].angle)/l
		L = int(np.floor(L))+1

		new_axe = np.linspace(np.min(bounds),np.max(bounds), L)
		new_df = []

		bounds = sorted(bounds)

		#D0 and D1 are the discontinuities at the first and last overlap points
		D0 = (interps[max_tilt]((Ekin,bounds[1]))/np.max(dfs[max_tilt])/2
			 -interps[min_tilt]((Ekin,bounds[1]))/np.max(dfs[min_tilt])/2
			 )

		D1 = (interps[min_tilt]((Ekin,bounds[2]))/np.max(dfs[min_tilt])/2
			 -interps[max_tilt]((Ekin,bounds[2]))/np.max(dfs[max_tilt])/2 
			 )

		A = (D1-D0)/(bounds[2]-bounds[1])

		for angle in new_axe:
			if (angle>=bounds[1])&(angle<=bounds[2]):
				#print('Zone 2')
				#print(angle)
				new_line = ( interps[max_tilt]((Ekin,angle))/np.max(dfs[max_tilt])
							+interps[min_tilt]((Ekin,angle))/np.max(dfs[min_tilt])
						   )/2
				new_line += A*(angle - bounds[1]) + D0 		#Adding the linear correction

			elif (angle>=bounds[0])&(angle<bounds[1]):
				#print('Zone 1')
				#print(angle)
				new_line = interps[max_tilt]((Ekin,angle))/np.max(dfs[max_tilt])
			elif (angle>bounds[2])&(angle<=bounds[3]):
				#print('Zone 3')
				#print(angle)
				new_line = interps[min_tilt]((Ekin,angle))/np.max(dfs[min_tilt])
			
			new_df.append(list(new_line))

		#print(new_df)
		maximum = max(np.max(dfs[0]), np.max(dfs[1]))
		new_df = np.array(new_df).T*maximum
		new_meta = copy.deepcopy(metas[0])

		new_meta.title += '__EXTENDED'
		new_meta.tilt = 0
		new_meta.comments = ''
		new_meta.angle = new_axe
		new_meta.Ekin = Ekin

		return Wave(new_meta, new_df)

	else:
		raise ValueError('Incompatible shapes :'+str(dfs[0].shape) +'and' +str(dfs[1].shape))

def extend_spectra(waves):
	wv = copy.deepcopy(waves)
	i=0
	while len(wv)!= 1:
		wave = merge([wv[i],wv[i+1]])
		wv[i] = wave
		trash = wv.pop(i+1)
		from matplotlib import pyplot as plt
		plt.pcolormesh(wave.metadata[0].angle, wave.metadata[0].Ekin, wave.data)
		plt.show()

	return wave

def concatenate_spectra(waves):

	data = [i.data for i in waves]
	angles = np.array([i.metadata[0].angle for i in waves])

	for i in range(1,len(angles)):
		angles[i] = angles[i] + angles[i-1][-1]

	new_data = np.concatenate(data, axis = 1)
	new_angle = np.concatenate(angles)

	new_meta = copy.deepcopy(waves[0].metadata[0])
	new_meta.title +='_CONCAT'
	new_meta.angle = new_angle

	return Wave(new_meta, new_data)

def create_ND_wave(waves, attribute_names, mode):

	print('DEBUG create_ND_wave')

	try:
		axis3 = [np.array([getattr(waves[i].metadata[0], attribute) for i in range(len(waves))]) for attribute in attribute_names]
		print('DEBUG axis3 ', axis3)
	except:
		for attribute_name in attribute_names:
			print('Attribute ',attribute_name,' not found')
		
		print('Arbitrary index used instead')
		axis3 = np.arange(len(waves))

	dfs = [waves[i].data for i in range(len(waves))]
	n_meta = [waves[i].metadata[0] for i in range(len(waves))]

	print('DEBUG data shape ',len(dfs), len(n_meta), len(axis3))

	if len(attribute_names) == 1:
		axis3 = axis3[0]
		print('DEBUG In single attribute list')
		try:
			dfs = np.array([x for _,x in sorted(zip(axis3,dfs))])
			n_meta = np.array([x for _,x in sorted(zip(axis3,n_meta))])
			axis3 = np.array(sorted(axis3))

		except (ValueError, TypeError):
			axis3 = np.arange(len(waves))
			dfs = np.array(dfs)
			n_meta = np.array(n_meta)

		print('DEBUG data shape ',dfs.shape, n_meta.shape, axis3.shape)

		Ascending = all(axis3[:-1] < axis3[1:])
		if not(Ascending):
			print('Warning : Wave axis is not stritly ascending')
			print('Arbitrary index used instead')
			axis3 = np.arange(len(waves))

		axes = [axis3, waves[0].metadata[0].Ekin, waves[0].metadata[0].angle]
	else:
		print('DEBUG In multi attribute list')
		dfs = np.array(dfs)
		n_meta = np.array(n_meta)

		axis3_order = order_axes(axis3)

		print('DEBUG axis3_order, ', axis3_order)

		N2, N1 = find_map_dims(axis3_order[True], axis3_order[False])

		axis3 = make_grid(axis3_order[True], axis3_order[False])
		if len(dfs) < N1*N2:
			dfs = [waves[i].data for i in range(len(waves))]+[np.random.rand(*waves[0].data.shape)*1.5 for i in range(N1*N2-len(waves))]
			n_meta = [waves[i].metadata[0] for i in range(len(waves))]+[waves[0].metadata[0] for i in range(N1*N2-len(waves))]

		dfs = np.array(dfs).reshape([N2,N1]+list(waves[0].data.shape))
		n_meta = np.array(n_meta)

		axes = [*axis3, waves[0].metadata[0].Ekin, waves[0].metadata[0].angle]

	wave = Wave(n_meta, dfs, axis= axes, axes_names = [*attribute_names, *waves[0].axes_names], mode = mode)

	return wave

#-----------------------

def _generate_interpolant(dfs, ax, grid):
	"""
	Generate the function to be interpolated
	
	Parameters:
	
		- dfs : stack of spectra
		- ax : array containing the value changing at each spectrum
		- grid : points where the data is valued, [gridx, gridy]
			gridx : 2D array
			gridy : 2D array
	
	Returns:
	
		interpolating function
	"""
	dfs = np.array(dfs)
	grid = [ax, grid[0][0], grid[1].T[0]]
	return RGI(grid, dfs, bounds_error = False, fill_value = 0)

#----------------------------------------4D waves

def order_axes(axes):
	order = {}

	for axe in axes:
		order[_count_crossings(axe)] = axe

	order = {i==np.max(list(order.keys())):order[i] for i in order}

	return order

def _count_crossings(axe, c = 0.03):
	from scipy.interpolate import interp1d
	from scipy.signal import find_peaks
	from scipy.ndimage import gaussian_filter
	from scipy.optimize import fsolve

	norm_ax = (axe-np.mean(axe))/(np.max(axe)-np.min(axe))
	index = range(len(axe))
	f = interp1d(index, gaussian_filter(norm_ax, c*len(axe)), bounds_error = False, fill_value = np.max(norm_ax))
	u = fsolve(f, f.x[:-1] + 0.1*np.random.rand(len(index)-1))
	u = u[(u>=0)*(u<=len(axe))]
	u = np.array([float('%.0f'%(val)) for val in u])

	return len(set(u))

def _optimize_order(axes):

	from matplotlib import pyplot as plt

	tab =[]

	s = np.linspace(0.001, 0.3, 50)

	for axe,name in zip(axes,['$axe_1$','$axe_2$']):
		tab.append([])
		for i in s:
			tab[-1].append(count_crossings(axe,i))
		plt.semilogx(s, tab[-1], '--*',label = name)
	
	plt.legend()
	plt.xlabel('Smoothing $\sigma$ (%length of axis)')
	plt.ylabel('N° of origin crossing')
	plt.show()

def find_map_dims(axe2, axe1, threshold = 0.05):
	"""
	Performs the derivative of a ndarray, the number of peaks (characterised by  being superior to the threshold) is the number of steps
	"""
	from scipy.signal import find_peaks

	diff = np.abs(axe1[:-2]-axe1[1:-1])
	N = len(diff[diff>threshold]) + 1

	diff = axe2[1:]-axe2[:-1]

	n = np.argmax(np.abs(diff))
	sign = diff[n]/np.abs(diff[n])

	diff = sign*diff 	#Rectifying differences to have up pointing peaks
	threshold = np.mean(diff)

	diff[diff < threshold] = 0
	peaks = find_peaks(diff)[0]

	N2 = peaks[0]+1
	N1 = len(peaks)+1

	if N1 == N:
		print('Successful dimension reconstruction')
	else:
		print('DEBUG reconstruction')

	return N2, N1

def make_grid(x, z):#, Ntot):
	"""
	Creates a grid from x and z, respacing the points on a regular lattice.
	
	Parameters:
		dfs : 1d ndarray like object containing all spectra. Spectra are 2d ndarray like objects.
	x : 1d ndarray like object containing all x positions
	z : 1d ndarray like object containing all z positions
	Ntot : a integer object with whose value should equate to Nx*Nz
	
	Returns:
		grid_x, grid_z : 2 1d darray objects containing the span of x and z
	"""

	Nx, Nz = find_map_dims(x,z)

	zi = z[0]									#because the graph of Z is a succession of step functions
	zf = z[-1]
	dz = (zf - zi)/Nz
	new_z = np.linspace(zi, zf, Nz)
	
	xi = x[0]
	xf = max(abs(x))
	dx = (xf - xi)/Nz							#because the number of files (len(intensity)) should be equal to Nx*Nz
	new_x = np.linspace(xi, xf, Nx)

	#X, Z = np.meshgrid(grid_x, grid_z)

	#print('dx : '+str(dx)+' Nx: '+str(Nx))
	#print('dz : '+str(dz)+' Nz: '+str(Nz))
	#print('x :')
	#print(x)
	#print('grid_x :')
	#print(grid_x)
	#print('z :')
	#print(z)
	#print('grid_z :')
	#print(grid_z)

	return new_x, new_z

def make_4D_wave(wave):

	x,z,E,angle = wave.axes
	x,z = make_grid(x,z, len(x))

	axes = [x,z]
	axes_names = ['x','z','E','angle']

	data = wave.data.reshape((len(x),len(z),len(E),len(angle)))

	metadata = np.reshape(wave.metadata,(len(x),len(z)))

	return Wave(metadata, data, axis = axes)

def make_3D_wave(wave):
	"""
	Transforms a 4D map*ARPES wave into a 3D map*XPS wave
	"""

	from copy import deepcopy
	x,z,E,angle = wave.axes

	axes = x
	axes_names = ['z','E','x']

	data = wave.data#wave.data.reshape((len(z),len(x),len(E),len(angle)))
	data = np.sum(data, 2)

	#data = np.moveaxis(data,2,1)

	n_meta = []

	for i in z:
		c_meta = deepcopy(wave.metadata[0])
		c_meta.angle = x
		c_meta.Rz = i
		c_meta.Rz_offset = 0
		c_meta.kinetic = True
		c_meta.angular = False
		c_meta.Ekin = z

		n_meta.append(c_meta)

	return Wave(n_meta, data, axis = axes, mode = 'FS', axes_names = axes_names)

#----------------------------------------Related to coordinate changes

def change_spectrum_coordinates(df,meta):

	interpolant = RGI([meta.Ekin,meta.angle], df, bounds_error = False, fill_value = np.nan)

	Eb, k = ang_to_k(meta.Ekin,
					 meta.angle,
					 meta.Rz,
					 meta.Rz_offset,
					 meta.phi,
					 meta.tilt,
					 meta.work,
					 meta.hv,
					 0,
					 meta.Ef)
	k_ = np.linspace(np.min(k),np.max(k),len(meta.angle))
	E_ = -Eb[0]
	#meta.angle = k_
	#meta.Ekin = E_
	new_df = np.array([[interpolant(k_to_ang(E,q,Ef=meta.Ef,tilt=meta.tilt,Rz_offset=meta.Rz_offset)) for q in k_] for E in E_])
	new_meta = copy.deepcopy(meta)

	new_meta.angular = False
	new_meta.kinetic = False
	new_meta.angle = k_
	new_meta.Ekin = E_

	return new_df,new_meta

def change_slice_coordinates(wave, index):

	interpolant = wave.interpolant()

	Ekin = wave.axes[-2][index]
	theta = wave.axes[-1]
	Rz = wave.axes[0]
	Rz_offset = wave.metadata[0].Rz_offset
	phi = wave.metadata[0].phi
	tilt = wave.metadata[0].tilt
	Ef = wave.metadata[0].Ef

	Eb,kx,ky = ang_to_k3D(Ekin,theta,Rz,Rz_offset,phi,tilt,0,0,0,Ef)#,mode = 'simple')

	E_ = Ekin
	k_x = np.linspace(np.min(kx),np.max(kx),len(kx))
	k_y = np.linspace(np.min(ky),np.max(ky),len(ky))

	new_df = np.array([[interpolant(k3D_to_ang(E_,q2,q1,Ef=0,tilt=0,Rz_offset=0)) for q1 in k_y] for q2 in k_x])

	return new_df, k_x, k_y

def change_hv_to_kz(wave, index, V0 = 13):

	interpolant = wave.interpolant()

	hv = wave.axes[0]
	Ekin = wave.axes[1] - hv[0]
	Efs = wave.Efs
	data = wave.data[:,:,index]

	HV, EKIN = np.meshgrid(hv, Ekin)

	KZ = ARPES_CONST*np.sqrt(EKIN + hv + V0)

	EB = -(Efs - EKIN - hv)

	return data.T, EB, KZ