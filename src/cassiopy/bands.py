"""
bands.py

Contains all functions to perform a band extraction from a spectra

	fit functions definition
	estimation functions
	extraction functions
	band reorganisation functions
"""

import numpy as np
import pandas as pd
from scipy.signal import convolve, wiener, find_peaks
from scipy.optimize import curve_fit, newton, brentq, OptimizeWarning
from scipy.interpolate import interp1d
from scipy.ndimage import gaussian_laplace, gaussian_filter

#---------------------Related to fit

def gaussian_peak(x, A, m, s, B):
	return A*np.exp(-((x-m)/(np.sqrt(2)*s))**2)+B

def sinf(x,w,t,A,B):
	return A*np.sin(w*x+t)+B

def estimate_FWHM(energy, df, m, ratio):
	"""
	"""
	indexmax = np.argmin(np.abs(m-energy))
	i = indexmax
	while df[i]>ratio*df[indexmax] and i>=0:			#Descending from the right
		i-=1
	widthLeft = np.abs(energy[indexmax]-(energy[i]+energy[i+1])/2)

	while df[i]>ratio*df[indexmax] and i<len(df)-1:		#Descending from the left
		i+=1
	widthRight = np.abs(energy[indexmax]-(energy[i]+energy[i-1])/2)

	return 2*max(widthLeft, widthRight)

def estimate_m(energy, df):
	"""
	Applies a Wiener filter to the ydata then compute a finite difference approximation of derivative
	Roots of the derivative is found between given emin, emax for whom f(emin) and f(emax) should have different signs.
	
	Garanteed in smooth data because of proeminent peaks.
	Garanteed in noisy data since the derivative is equally noisy and then has a definite maximum
	The method should find the biggest peak.

	Parameters:
		energy : 1d ndarray like object containing the energy range
		df : 1d ndarray like object containing the energy distribution curve
	Returns:
		en : float like value, energy of the peak

	"""
	Ekin = energy[:]
	#w = wiener(df)						#Applies a Wiener filter to the ydata
	diff = np.gradient(df)					#Finite difference approximation of derivative
	interpWDiff = interp1d(energy, diff)  #Interpolated version of the derivative
	emin = energy[np.argmin(diff)]
	emax = energy[np.argmax(diff)]
	en = brentq(interpWDiff, emin, emax)
	#Finds the point where the derivative cancels.
	#f(emin) and f(emax) should have different signs.
	#Garanteed in smooth data because of proeminent peaks.
	#Garanteed in noisy data since the derivative is equally noisy.
	#The method should find the biggest peak.

	return en

def estimate_params(energy, df, method = 'Max'):
	"""
	Find a first estimation of optimisation Parameters: (gaussian fit)
	Parameters:
		energy : 1d ndarray like object containing the energy range
		df : 1d ndarray like object containing the energy distribution curve
		method : 'Max' or 'Deriv', the scheme used to detect the biggest peak
	Returns:
		A, m, s, B : Gaussian Parameters:
		reducedDf, reducedEnerg : 2 1d ndarray which are used as a restriction of the fitting procedure to a certain range of energy
	"""
	if method == 'Max':
		m = energy[np.argmax(df)]
	if method == 'Deriv':
		m = estimate_m(energy, df)

	ratio= 1/2
	FWHM = estimate_FWHM(energy, df, m, ratio)
	s = FWHM/3

	window = (energy<m+2*s)*(energy>m-2*s)
	reducedDf = df[window]
	reducedEnerg = energy[window]

	B = (reducedDf[0]+reducedDf[-1])/2
	A = np.max(reducedDf)-B

	return A, m, s, B, reducedDf, reducedEnerg

# def test_fit(energy, df):
# 	A, m, s, B, ydata, xdata = estimate_params(energy, df)
# 	popt, cov = curve_fit(gaussian_peak, xdata, ydata, p0=[A,m,s,B])
# 	plt.plot(energy, df, '.--', label='measured')
# 	plt.plot(xdata, gaussian_peak(xdata, A, m, s, B), '.--', label='first guess')
# 	plt.plot(xdata, gaussian_peak(xdata, popt[0], popt[1], popt[2], popt[3]), label='Regression')

def create_partition(x, N):
	xmin = min(x)
	xmax = max(x)
	dx = (xmax-xmin)/N
	intervals = []
	partition = []
	for i in range(N):
		intervals.append([xmin+i*dx, xmin+(i+1)*dx])

	for part in intervals :
		partition.append((part[0]<x)*(x<=part[1]))
	return partition

def find_plateaux(data):
	"""
	Find all plateaux in data

	input :
		data : 1d array like object
	output :
		firsts : indexes of beginning of plateaux
		lenghts : lenght of given plateaux
	"""
	isplateau = False
	firsts = []
	lengths = []
	for i in range(1,len(data)):
		if (data[i]-data[i-1] == 0) and not(isplateau):	#no data changes and state not plateau
			isplateau = True
			firsts.append(i-1)
			lengths.append(0)
		if (data[i]-data[i-1] == 0) and isplateau :	#no data changes and state is plateau
			lengths[-1] += 1
		if (data[i]-data[i-1] != 0) and isplateau : #data changes and state is plateau
			isplateau = False
			#print('Plateau N°'+str(len(firsts)))
			#print('Began at index '+str(firsts[-1]))
			#print('Length '+str(lengths[-1]))
		if data[i] == 0:	#stops the function if the value of the last plateau is zero
			break

	return firsts, lengths

def count_peaks_threshold(df, meta, sigma = 3, step = 1, tlim = 'Full', elim = 'Full', diff = laplace):
	"""
	Parameters:
		df :
		meta :
		sigma :
		step : percentage of maximal value
		tlim :
		elim :
		diff :
	outputs:
		opt
	"""

	#from matplotlib import pyplot as plt

	deriv = diff(gaussian_filter(df, sigma), meta, 1)
	if tlim == 'Full':
		signal = signal =  integ_angle(deriv, meta)
	else:
		signal =  integ_angle(deriv, meta, tlim[0], tlim[1])
	n_of_peaks = []
	opt = 0

	if elim == 'Full':
		elim = [min(meta.Ekin),max(meta.Ekin)]
	part = (min(elim)<meta.Ekin)*(meta.Ekin<=max(elim))

	dy = signal[part].max()*step/100
	#print('Current maximum :'+str(signal[part].max()))
	thresholds = np.arange(0,signal[part].max(),dy)

	for threshold in thresholds :
		cuttedSignal = signal[part]
		cuttedSignal[signal[part]<threshold] = threshold
		peaks = find_peaks(cuttedSignal)[0]
		n_of_peaks.append(len(peaks))

	firsts, lengths = find_plateaux(n_of_peaks)
	try:
		if len(lengths) == 1:
			iopt = np.argmax(lengths)
		else:
			iopt = np.argmax(lengths[:-1])
		#plt.figure(5)
		#plt.plot(lengths, '.')
		opt = [thresholds[firsts[iopt]], n_of_peaks[firsts[iopt]]]	#Definition of opt
	except ValueError:
		opt = [np.min(signal[part]), 0]
	cuttedSignal = signal[part]
	cuttedSignal[signal[part]<opt[0]] = opt[0]
	peaks = find_peaks(cuttedSignal)[0]

	return opt, [thresholds, n_of_peaks, peaks]

#---------------------Related to band extraction

def EDC_MDC(df, meta, sigma = 3, a = 0.01, tlim = 'Full', elim = 'Full', diff = laplace):
	"""
	Parameters:
		df :
		meta :
		sigma :
		step : percentage of maximal value
		tlim :
		elim :
		diff :
	outputs:
		opt
	"""

	#from matplotlib import pyplot as plt

	def remove_peak(x, y, m, s):
		y[(x>m-4*s)*(x<m+4*s)] = 0
		return y

	deriv = diff(gaussian_filter(df, sigma), meta, 1)	#Filtered signal to find bands
	deriv[meta.Ekin>meta.Ef,:] = 0						#Remove all noise above fermi level

	if elim == 'Full':
		elim = [min(meta.Ekin),max(meta.Ekin)]
	partE = (min(elim)<=meta.Ekin)*(meta.Ekin<=max(elim))

	if tlim == 'Full':
		tlim = [min(meta.angle),max(meta.angle)]
	partT = (min(tlim)<=meta.angle)*(meta.angle<=max(tlim))

	EDC = []
	L = len(meta.angle[partT])
	init = np.argmin(np.abs(meta.angle-meta.angle[partT][0]))

	for i in range(init,init+L):
		current = deriv[partE,i]
		peaks, properties = find_peaks(current)
		#plt.plot(meta.Ekin[peaks], deriv[peaks,i],'x')
		#plt.plot(meta.Ekin, deriv[:,i])
		#plt.show()
		band = []
		if i%100 == 0 :
			print('STEP '+str(i-init)+'/'+str(L))
		for peak in peaks:
			A, m, s, B, ydata, xdata = estimate_params(meta.Ekin[partE], current, method = 'Max')
			#plt.plot(meta.Ekin, current)
			#plt.plot(meta.Ekin, gaussian_peak(meta.Ekin, A, m, s, B))
			#plt.savefig(str(i)+'_'+str(peak)+'.png')
			#plt.close()
			#plt.show()
			band.append(m)
			current = remove_peak(meta.Ekin[partE], current, m, s)
		EDC.append(band)

	return pd.DataFrame(EDC)

def organise_bands(bands):
	ex = False
	for i in range(bands.shape[1]) :
		for j in range(1,bands.shape[0]):
			contimatrix = [np.abs(bands[j,i]-bands[j-1,k]) for k in range(bands.shape[1])]
			contimatrix = [u if not(np.isnan(u)) else 1000 for u in contimatrix]
			index = np.argmin(contimatrix)
			
			if np.abs(bands[j,index]-bands[j-1,index])>contimatrix[index]:
				ex = ex or (index != i)
				c = bands[j,i]
				bands[j,i] = bands[j,index]
				bands[j,index] = c
	return bands

def split_band(band):
	mask = np.isnan(band).astype(int)+1
	firsts, lengths = find_plateaux(mask)
	lengths.insert(firsts[0],0)
	firsts.insert(0,0)
	bands = []
	#print('ORIGINAL BAND')
	#print(band)
	for j in range(0,len(firsts)):
		bands.append(np.ones(len(mask))*np.nan)
		bands[-1][firsts[j]:firsts[j]+lengths[j]+1] = band[firsts[j]:firsts[j]+lengths[j]+1]
		#print('EXTRACTED BAND')
		#print(bands[-1])

	return [k for k in bands if not(np.isnan(k).all())]

def extract_bands(df, meta, sigma = 3, a = 0.01, lims = []):

	bands = []
	for lim in lims:
		rawbands = EDC_MDC(df, meta, sigma, tlim = lim['tlim'], elim = lim['elim'])
		orgbands = organise_bands(rawbands.values)
		splittedbands = []
		for band in np.transpose(orgbands):
			splittedbands+=split_band(band)
		finalbands = organise_bands(np.transpose(splittedbands))

		if lim['tlim'] == 'Full':
			lim['tlim'] = [min(meta.angle),max(meta.angle)]
		part = (min(lim['tlim'])<=meta.angle)*(meta.angle<=max(lim['tlim']))

		for band in finalbands.T :
			bands.append(pd.DataFrame({'band':band,'axis':meta.angle[part]}))

	return bands

def fit_band(axis, band, deg = 2):

	reduced_axis = axis[~(np.isnan(band))]
	reduced_band = band[~(np.isnan(band))]
	coeffs = np.polyfit(reduced_axis, reduced_band, deg = deg)
	xdata = [np.ones(len(reduced_axis))*reduced_axis**(deg-i) for i in range(len(coeffs))]
	result = np.dot(coeffs, xdata)

	return pd.DataFrame('coeffs':coeffs, 'axis':reduced_axis, 'band':reduced_band, 'fit':result)

def fit_band_sin(axis, band, *_):

	reduced_axis = axis[~(np.isnan(band))]
	reduced_band = band[~(np.isnan(band))]
	try :
		p, cov = curve_fit(sinf, reduced_axis, reduced_band, p0 = [1/30, 0, 0.8, 40])
		results = sinf(reduced_axis,p[0],p[1],p[2],p[3])
	except (TypeError, RuntimeError):
		p = []
		results = []
		reduced_axis = []
		reduced_band = []

	return p, reduced_axis, results, reduced_band