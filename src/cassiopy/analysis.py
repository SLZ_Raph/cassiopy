"""
analysis.py

Contains all functions to perform the analysis

	integration functions : performs the sum of an array along an axis and boundaries
	reductions functions : transform a 2D table into a scalar, useful to compactify a spatial map of spectra
	image analysis : operators acting on spectra (curvature, second derivative, normalization...)
	band extraction : complex functions that allow band extracting
	Fermi Surface exploration : geometrical functions to extract cuts from fermi surfaces, trace paths in reciprocal space
"""

import copy
import pandas as pd
from typing import Union

from cassiopy.fit_tools import *
from cassiopy.waveform import *

#---------------------------------------Related to integration

def integ_angle(data, axis = None, lims = None):
	"""
	Performs the integration (summation) of all values in the range defined by lims (tmin, tmax)
	if tmin and tmax are not specified the integration is done on the whole range of theta values

	
	Parameters:
		data (ndarray): 
			2D spectrum data
		axis (array): 
			axis along which the integration will be performed
		lims (list): 
			Boundaries of the integration [tmin,tmax]

	Returns:
		result (array): 
			1D integrated array
	"""
	if axis is not None:
		if min(lims) != max(lims):
			sig = data[:,(min(lims)<axis)*(max(lims)>=axis)]
			result = sig.sum(axis = 1)
		else :
			sig = data[:,np.argmin(np.abs(axis-min(lims)))]
			result = sig
	else :
		sig = data
		result = sig.sum(axis = 1)
	return result

def integ_energ(data, axis = None, lims = None):
	"""
	Performs the integration (summation) of all values in the range defined by lims (Emin, Emax)
	if Emin and Emax are not specified the integration is done on the whole range of theta values
	
	Parameters:
		data (ndarray):
			2D spectrum data
		axis (array):
			axis along which the integration will be performed
		lims (list):
			Boundaries of the integration [Emin,Emax]

	Returns:
		result (array):
			1D integrated array
	"""
	if axis is not None :
		if min(lims) != max(lims):
			sig = data[(min(lims)<axis)*(max(lims)>=axis),:]
			result = sig.sum(axis = 0)
		else :
			sig = data[np.argmin(np.abs(axis-min(lims))),:]
			result = sig
	else :
		sig = data
		result = sig.sum(axis = 0)
	return result

def integzone(data, mask):
	"""
	Sum all values in the area defined by a mask

	Parameters:
		data (ndarray)
			2D spectrum data
		mask (ndarray):
			2D array of booleans containing the area to be summed upon, same dimensions as data
	
	Returns:
		spec (array)
			1D integrated array (depends of the energy)
	"""
	spec = [np.sum(data[i][mask]) for i in range(data.shape[0])]
	return spec

def integrale(data):
	"""
	Sums all values of data on the two axis

	Parameters:
		data (ndarray)
			2D spectrum data
	Returns:
		value (float):
			Total integrated signal
	"""
	return np.sum(np.sum(data, axis =2), axis = 1)

def slice_vertical(wave, angle, sigma = 3):
	"""
	Performs the averaging (summation) of all values in the range defined by angle.
	Uses the interpolated spectrum instead of the direct values (compare to integ_angle).

	Parameters:
		wave (Wave):
			2D wave spectrum data + metadata
		angle (float or array):
			position/range over which the data will be sliced and averaged
		sigma (float):
			gaussian smoothing parameter

	Returns:
		df (DataFrame):
			contains the result of the computation,
			columns : E,data,lap

	Notes:
		df.E : energy axis
		df.data : sliced data
		df.lap : vertical sliced of second derivative data
	"""
	Ek = wave.metadata[0].Ekin
	axis = wave.metadata[0].angle
	try :
		len(angle)
		values = axis[(min(angle)<axis)*(max(angle)>=axis)]
	except TypeError:
		values = [angle]

	inter = wave.interpolant()
	data_slice = np.mean([inter((Ek,ang)) for ang in values], axis = 0)

	wavediff = copy.deepcopy(wave)
	wavediff.differentiate(laplace, sigma)
	interdiff = wavediff.interpolant()
	data_slice_lap = np.mean([interdiff((Ek,ang)) for ang in values], axis = 0)

	df = pd.DataFrame({'E':Ek,'data':data_slice,'lap':data_slice_lap})
	df.to_csv('./'+wave.metadata[0].title+'.csv', index = None)

	return df

def slice_horizontal(wave, energy, sigma = 3):
	"""
	Performs the averaging (summation) of all values in the range defined by angle.
	Uses the interpolated spectrum instead of the direct values (compare to integ_angle).

	Parameters:
		wave (Wave):
			2D wave spectrum data + metadata
		energy (float or array):
			position/range over which the data will be sliced and averaged
		sigma (float):
			gaussian smoothing parameter

	Returns:
		df (DataFrame):
			contains the result of the computation,
			columns : angle,data,lap

	Notes:
		df.angle : angle axis
		df.data : sliced data
		df.lap : vertical sliced of second derivative data
	"""
	Ek = wave.metadata[0].Ekin
	axis = wave.metadata[0].angle
	try :
		len(energy)
		values = Ek[(min(energy)<Ek)*(max(energy)>=Ek)]
	except TypeError:
		values = [energy]

	inter = wave.interpolant()
	data_slice = np.mean([inter((E,axis)) for Ek in values], axis = 0)

	wavediff = copy.deepcopy(wave)
	wavediff.differentiate(laplace, sigma)
	interdiff = wavediff.interpolant()
	data_slice_lap = np.mean([interdiff((E,axis)) for E in values], axis = 0)

	df = pd.DataFrame({'angle':axis,'data':data_slice,'lap':data_slice_lap})
	df.to_csv('./'+wave.metadata[0].title+'.csv', index = None)

	return df

#----------------------------------------Related to reduction functions
# Reduction functions are used to condense the information of a 4D map
# into a 2D map.
# They connect a M_(nxm) space to R+
#
# They must follow the following prototype to be compatible with plotXZ_map
# 	function(data, axes, limits, *_)

def reduction_absolute_d(data, axes, limits, sigma = 5):
	"""
	Performs the integration (summation) of the absolute variation of the LoG filtered signal in the given limits.
	Functional from M_nxm -> R

	Parameters:
		data : 2d ndarray
		Spectrum data
	energy : ndarray or list
		axis along which the integration will be performed
	lims : list
		Boundaries of the integration
	sigma : float
		Sigma arameter of the gaussian filter
	
	Returns:
		result : float

	"""
	Emin, Emax, AngMin, AngMax = limits
	if Emin == 0 and Emax == 0:
		Emin = np.min(axes[0])
		Emax = np.max(axes[0])

	Ekin = axes[0]
	angle = axes[1]

	reduced = integ_angle(laplace(gaussian_filter(data, sigma), axes), angle, [AngMin,AngMax])
	diff = np.abs(np.gradient(reduced))

	#return reduced[(51<energy)*(energy<52.5)].sum()
	return diff[(Emin<Ekin)*(Ekin<Emax)].sum()

def reduction_peak(data, axes, limits, *_):
	"""
	Performs the integration (summation) of the signal in the given limits (only energy limits matters)
	Normalised by the total intensity
	
	Parameters:
		data : 2d ndarray
		Spectrum data
	energy : ndarray or list
		axis along which the integration will be performed
	lims : list
		Boundaries of the integration [Emin, Emax, AngMin, AngMax]
	
	Returns:
		result : float
	"""
	Emin, Emax, AngMin, AngMax = limits
	if Emin == 0 and Emax == 0:
		Emin = np.min(axes[0])
		Emax = np.max(axes[0])

	#print('axe1',axes[1])

	reduced = integ_angle(data, axes[1], [AngMin,AngMax])

	#return reduced[(51<energy)*(energy<52.5)].sum()
	return reduced[(Emin<axes[0])*(axes[0]<Emax)].sum()/integ_angle(data).sum()

def reduction_fit(data, axes, energy, *_):
	"""
	Performs the integration (summation) of the signal in the given limits (only energy limits matters)
	Normalised by the total intensity
	
	Parameters:
		data : 2d ndarray
		Spectrum data
	energy : ndarray or list
		axis along which the integration will be performed
	
	Returns:
		result : float
	"""
	ADC = integ_angle(data)
	A, m, s, B, ydata, xdata = estimate_params_g(axes[0], ADC, method = 'Deriv')
	try:
		popt, cov = curve_fit(gaussian_peak, xdata, ydata, p0=[A,m,s,B])
		perr = np.sqrt(np.diag(cov))
		result = np.sum(popt/perr)
	except (OptimizeWarning, RuntimeWarning, RuntimeError):
		result = 0
	if np.isnan(intensity):
		result = 0
	return result

def condense_maps(function, dfs):
	"""
	Performs the reduction of the maps according to a specified function.
	The function shoud have values in real numbers coming from a N*M matrix
	eg: np.max, np.sum...

	
	Parameters:
		dfs : list
		list of all spectra
	function : function
		function used to compute the intensity table from spectra. 
		reduc_func : (R**(n+m)) -> R+
		For all spectra in associates a positive real value
	
	Returns:
		intensity : 1d ndarray 
		Array containing the intensities calculated with a given reduction function
	"""
	intensity = []
	for df in dfs :
		intensity.append(function(df))
	return np.array(intensity)

#---------------------------------------- Related to XZ map generation
# Tools used to create a 2D map compatible with pcolormesh

# def find_delta(data, nblock = None):
# 	"""
# 	A function that gives back the mean step between a series of

# 	Parameters:
# 		data : 1D ndarray
	
# 	Returns:
# 		mean : float
# 		mean step between values
# 	"""

# 	clean = np.array(sorted(tuple(set(data))))
# 	diff = clean[:-2]-clean[1:-1]
# 	if nblock == None:
# 		mean = abs(np.sum(diff))/len(diff)
# 	else :
# 		mean = abs(np.sum(diff))/nblock
# 	return mean

def create_map(wave, limits, sigma, function):
	"""
	Computes a XZ map with compatible grid using the specified reduction fonction

	Parameters:
		wave : Wave object 
		4D wave containing spectra of the map
	limits : list
		Boundaries of the integration for the reduction fonction [Emin,Emax,Angmin,Angmax]
	sigma : float
		Gaussian smoothing parameter
	function : function
		Reduction function used. Must follow prototype f(data, axes, limits, *_))

	Returns:
		gridX : ndarray
		2D grid for X positions
	gridZ : ndarray
		2D grid for Z positions
	I : ndarray
		2D intensity map

	"""
	
	z = np.array([float(i.z) for i in wave.metadata])
	x = np.array([float(i.x) for i in wave.metadata])
	Ntot = len(wave.metadata)

	xx,zz = make_grid(x, z)#, Ntot)
	xx = xx-np.mean(xx[1:]-xx[:-1])/2		#centering the grid on middle of points (for pcolormesh)
	zz = zz-np.mean(zz[1:]-zz[:-1])/2		#centering the grid on middle of points (for pcolormesh)
	gridX, gridZ = np.meshgrid(xx,zz)

	reduc_func = lambda data : function(data, wave.axes, limits, sigma)
	I = condense_maps(reduc_func, wave.data)

	try:
		I = I.reshape(gridX.shape)
	except ValueError:
		padding = np.NaN*np.ones(Ntot-len(I))
		I = np.concatenate((I,padding))

	return gridX, gridZ, I

#----------------------------------------Related to image analysis operators on spectra

def normalise_background(data, axis, lims, sigma = 0):
	"""
	Performs normalisation as defined by spectrum/background
	The background is the integrated data inside the boundaries defined by lims
	It accounts for the detector background in a low intensity data

	Parameters:
		data : 2d ndarray 
		Spectrum data
	axis : ndarray or list
		axis along which the integration will be performed
	lims : list
		Boundaries of the integration
	sigma : float
		variance of the gaussian smoothing

	Returns:
		result : array
		integrated array
	"""
	background = integ_energ(data, axis, lims)

	#pd.DataFrame(background).to_csv('./file_integ.csv')

	if sigma == 0:
		pass
	else:
		background = gaussian_filter(background, sigma)
	#pd.DataFrame(background).to_csv('./file_integ_smooth.csv')

	M = np.max(data)

	return 1000*data/background*M

def normalise_background_binning(data, axis, lims, sigma = 0):

	def moving_average(a, n=3) :
		ret = np.cumsum(a, dtype=float)
		ret[n:] = ret[n:] - ret[:-n]
		ret =  ret[n - 1:] / n

		x = range(0,len(ret))
		x_new = np.arange(0,len(ret),len(a))

		interp_ret = interp1d(x, ret)

		return interp_ret(x_new)

	background = integ_energ(data, axis, lims)
	if sigma == 0:
		pass
	else:
		background = moving_average(background, int(sigma))
	return 1000*data/background

def normalise_stack(data, axis, lims, sigma = 0):
	#FS (ky, energy, kx)
	for i in range(len(data)):
		data[i] = normalise_background(data[i],
										axis,
										lims, sigma)
	return data

def normalise_stack_inPlane(data, axis, lims, sigma = 0):
	#FS (ky, energy, kx)
	min_e = np.argmin(np.abs(axis-min(lims)))
	max_e = np.argmin(np.abs(axis-max(lims)))

	background = np.mean(data[:,min_e:max_e,:], axis = 1)
	#print(data.shape)
	#print(background.shape)
	if sigma == 0:
		pass
	else:
		background = gaussian_filter(background, sigma)

	from cassiopy.plotting import plot3D
	X,Y = np.meshgrid(np.arange(data.shape[0]),np.arange(data.shape[2]))
	plot3D(X,Y,background.T)

	M = np.max(data)

	for i in range(data.shape[1]):
		data[:,i,:] = data[:,i,:]/background
		data[:,i,:] = 1000*data[:,i,:]/np.mean(data[:,i,:])*M

	return data

def renormalise(data, *_):
	"""
	Performs normalisation as defined by spectrum/(EDC*ADC)

	Parameters:
		data (ndarray):
			2D spectrum data

	Returns:
		new_data (ndarray):
			2D spectrum data, renormalised spectrum data
	"""
	EDC = integ_angle(data)
	ADC = integ_energ(data)
	normal_map = np.tensordot(EDC, ADC, axes = 0)
	try:
		new_data = data/normal_map
	except RuntimeWarning:
		new_data = new_data
		print('Division not performed, zero in denominator')
	return new_data

def mean_curvature(data, axes, a = 1,*_):
	"""
	Computes the (2D) mean curvature of a spectrum. All the values under 0 are truncated.
	The function is generalised for inhomogenous coordinates.

	Parameters:
		data (ndarray):
			2D spectrum data
		axes (list):
			Axes of the data, [energy, angle]
		a (float)
			free parameter controling finesse of the treatment
	
	Returns:
		C (ndarray):
			2D Mean curvature of the spectrum data
	"""
	D = np.gradient(data)		#[Dx, Dy]
	Dx = np.gradient(D[0]) 	#[Dxx, Dxy]
	Dy = np.gradient(D[1]) 	#[Dyx, Dyy]
	H = np.array([[Dx[0],Dx[1]],[Dy[0],Dy[1]]])	#Hessian matrix
	Cy = (axes[0][1]-axes[0][0])**2 #inhomogeneous coefficient for y values
	Cx = (axes[1][1]-axes[1][0])**2 #inhomogeneous coefficient for x values
	#Cx = 10
	#Cy = 1
	C =(
		(a+Cx*D[0]**2)*Cy*H[1,1] 
		-Cy*Cx*D[0]*D[1]*(H[0,1]+H[1,0])
		+(a+Cy*D[1]**2)*Cx*H[0,0])*(a+Cx*D[0]**2+Cy*D[1]**2)**(-3/2)	#Curvature
	C = -C


	if np.isnan(data).any():		#Case for masked arrays	
		m = np.min(data[~np.isnan(data)])
		M = np.max(data[~np.isnan(data)])
		if m >= 0:
			C = np.array([[0 if not(np.isnan(j)) and j<0 else j for j in i] for i in C])
			MC = np.max(C[~np.isnan(C)])
			C = C/MC*M

	else:							#Case for normal arrays
		M = np.max(data)
		if np.min(data) >= 0:
			C[C<0] = 0
		C = C/np.max(C)*M

	return C

def identity(data,*_):
	"""
	Returns the data untouched

	Parameters:
		data (ndarray):
			2D spectrum data
	
	Returns:
		data (ndarray):
			2D spectrum data
	"""
	return data

def laplace(data, axes, *_):
	"""
	Computes the laplacian of a spectrum. All the values under 0 are truncated.
	The function is generalised for inhomogenous coordinates.

	Parameters:
		data (ndarray):
			2D spectrum data
		axes (list):
			Axes of the data, [energy, angle]
	
	Returns:
		C (ndarray):
			2D Laplacian of the 2D spectrum data
	"""
	D = np.gradient(data)		#[Dx, Dy]
	Dx = np.gradient(D[0]) 	#[Dxx, Dxy]
	Dy = np.gradient(D[1]) 	#[Dyx, Dyy]
	H = np.array([[Dx[0],Dx[1]],[Dy[0],Dy[1]]]) #Hessian matrix
	Cy = (axes[0][1]-axes[0][0])**2 #inhomogeneous coefficient for y values
	Cx = (axes[1][1]-axes[1][0])**2 #inhomogeneous coefficient for x values

	L = -Cx*H[0,0] - Cy*H[1,1]

	if np.isnan(data).any():		#Case for masked arrays
		#print('For masked arrays')
		
		m = np.min(data[~np.isnan(data)])
		M = np.max(data[~np.isnan(data)])

		if m >= 0:
			L = np.array([[0 if not(np.isnan(j)) and j<0 else j for j in i] for i in L])
			ML = np.max(L[~np.isnan(L)])
			L = L/ML*M

	else:							#Case for normal arrays
		M = np.max(data)
		if np.min(data) >= 0:
			L[L<0] = 0
		L = L/np.max(L)*M
	
	return L

def curvature(data, axes, a = 1, *_):
	"""
	Computes the (1D) curvature of a spectrum. All the values under 0 are truncated.
	The function is generalised for inhomogenous coordinates.

	Parameters:
		data (ndarray):
			2D spectrum data
		axes (list):
			Axes of the data, [energy, angle]
		a (float):
			free parameter controling finesse of the treatment
	
	Returns:
		C (ndarray):
			1D curvature of the 2D spectrum data
	"""
	C = np.zeros(data.shape)
	for i in range(data.shape[0]):
		D = np.gradient(data[i])
		H = np.gradient(D)	#second derivative
		C0 = a*np.abs(np.max(D))
		C[i]=H*(C0+D**2)**(-3/2)
	C = -C

	if np.isnan(data).any():		#Case for masked arrays	
		m = np.min(data[~np.isnan(data)])
		M = np.max(data[~np.isnan(data)])
		if m >= 0:
			C = np.array([[0 if not(np.isnan(j)) and j<0 else j for j in i] for i in C])
			MC = np.max(C[~np.isnan(C)])
			C = C/MC*M

	else:							#Case for normal arrays
		M = np.max(data)
		if np.min(data) >= 0:
			C[C<0] = 0
		C = C/np.max(C)*M

	return C

def laplace_all(dfs, axes, sigma):
	"""
	Calculates the laplacian for a stack
	
	Parameters:
		dfs (ndarray):
			stack of 2D spectrum data
		axes (list):
			Axes of the data, [energy, angle]
		sigma (float):
			gaussian smoothing parameter
	
	Returns:
		results (ndarray):
			Stack of laplacian of 2D spectrum data
	"""
	CDfs = []
	for i in dfs:
		CDfs.append(laplace(gaussian_filter(i,sigma), axes))
	return np.array(CDfs)

def curvature_all(dfs, a, sigma):
	"""
	Calculates the 1D curvature for a stack
	
	Parameters:
		dfs (ndarray):
			stack of 2D spectrum data
		a (float):
			free parameter controling finesse of the treatment
		sigma (float):
			gaussian smoothing parameter
	
	Returns:
		results (ndarray):
			Stack of 1D curvature of 2D spectrum data
	"""
	CDfs = []
	for i in dfs :
		CDfs.append(curvature(gaussian_filter(i,sigma), None, a))
	return np.array(CDfs)

def renormalise_all(dfs):
	"""
	Performs normalisation for a stack, see method renormalise
	
	Parameters:
		dfs (ndarray):
			stack of 2D spectrum data
	
	Returns:
		results (ndarray):
			stack of renormalised 2D spectrum data
	"""
	renDfs = []
	for i in dfs :
		renDfs.append(renormalise(i))
	return np.array(renDfs)

#----------------------------------------Related to topological/geometric detection

def _create_partition(x_range, N):
	"""
	Cuts a x_range interval in N sub ranges

	Parameters:
		x_range : 1D ndarray
		Sequence of values of an axis
	N : integer
		Number of splits
	
	Returns:
		partition : list
		x_range splitted in N parts
	"""
	xmin = min(x_range)
	xmax = max(x_range)
	dx = (xmax-xmin)/N
	intervals = []
	partition = []
	for i in range(N):
		intervals.append([xmin+i*dx, xmin+(i+1)*dx])

	for part in intervals :
		partition.append((part[0]<x)*(x<=part[1]))
	return partition

def _find_plateaux(data):
	"""
	Find all plateaux in data for staircase like functions

	Parameters:
		data : 1d ndarray
	
	Returns:
		firsts : list
		indexes of beginning of plateaux
	lenghts : list
		lenght of given plateaux
	"""
	isplateau = False
	firsts = []
	lengths = []
	for i in range(1,len(data)):
		if (data[i]-data[i-1] == 0) and not(isplateau):	#no data changes and state not plateau
			isplateau = True
			firsts.append(i-1)
			lengths.append(0)
		if (data[i]-data[i-1] == 0) and isplateau :	#no data changes and state is plateau
			lengths[-1] += 1
		if (data[i]-data[i-1] != 0) and isplateau : #data changes and state is plateau
			isplateau = False
			#print('Plateau N°'+str(len(firsts)))
			#print('Began at index '+str(firsts[-1]))
			#print('Length '+str(lengths[-1]))
		if data[i] == 0:	#stops the function if the value of the last plateau is zero
			break

	return firsts, lengths

def _count_peaks_threshold(df, meta, sigma = 3, step = 1, tlim = 'Full', elim = 'Full', diff = laplace):
	"""
	Functions that shows how many peaks are detected applying a threshold to the detection

	Parameters:
		df :
		meta :
		sigma :
		step : percentage of maximal value
		tlim :
		elim :
		diff :
	
	Returns:
		opt
	"""

	#from matplotlib import pyplot as plt

	deriv = diff(gaussian_filter(df, sigma), meta, 1)
	if tlim == 'Full':
		signal = signal =  integ_angle(deriv, meta)
	else:
		signal =  integ_angle(deriv, meta, tlim[0], tlim[1])
	n_of_peaks = []
	opt = 0

	if elim == 'Full':
		elim = [min(meta.Ekin),max(meta.Ekin)]
	part = (min(elim)<meta.Ekin)*(meta.Ekin<=max(elim))

	dy = signal[part].max()*step/100
	#print('Current maximum :'+str(signal[part].max()))
	thresholds = np.arange(0,signal[part].max(),dy)

	for threshold in thresholds :
		cuttedSignal = signal[part]
		cuttedSignal[signal[part]<threshold] = threshold
		peaks = find_peaks(cuttedSignal)[0]
		n_of_peaks.append(len(peaks))

	firsts, lengths = find_plateaux(n_of_peaks)
	try:
		if len(lengths) == 1:
			iopt = np.argmax(lengths)
		else:
			iopt = np.argmax(lengths[:-1])
		#plt.figure(5)
		#plt.plot(lengths, '.')
		opt = [thresholds[firsts[iopt]], n_of_peaks[firsts[iopt]]]	#Definition of opt
	except ValueError:
		opt = [np.min(signal[part]), 0]
	cuttedSignal = signal[part]
	cuttedSignal[signal[part]<opt[0]] = opt[0]
	peaks = find_peaks(cuttedSignal)[0]

	return opt, [thresholds, n_of_peaks, peaks]

#----------------------------------------Related to band extraction

def edc_mdc_bandscan(wave, sigma = 3, a = 0.01, tlim = 'Full', elim = 'Full', diff = laplace):
	"""
	
	Parameters:
		wave (Wave):
		sigma (float):
			gaussian smoothing parameter
		step (float): percentage of maximal value
		tlim (list):
			angular boundaries
		elim (list):
			energy boundaries
		diff (function):
			derivation operator used

	Returns:
		EDC (dataframe):
	"""

	#from matplotlib import pyplot as plt

	def remove_peak(x, y, m, s):
		y[(x>m-4*s)*(x<m+4*s)] = 0
		return y

	meta = wave.metadata[0]

	deriv = diff(gaussian_filter(wave.data, sigma), wave.axes, a)	#Filtered signal to find bands
	deriv[meta.Ekin>meta.Ef,:] = 0						#Remove all noise above fermi level

	if elim == 'Full':
		elim = [min(meta.Ekin),max(meta.Ekin)]
	partE = (min(elim)<=meta.Ekin)*(meta.Ekin<=max(elim))

	if tlim == 'Full':
		tlim = [min(meta.angle),max(meta.angle)]
	partT = (min(tlim)<=meta.angle)*(meta.angle<=max(tlim))

	EDC = []
	L = len(meta.angle[partT])
	init = np.argmin(np.abs(meta.angle-meta.angle[partT][0]))

	for i in range(init,init+L):
		current = deriv[partE,i]
		peaks, properties = find_peaks(current)
		#plt.plot(meta.Ekin[peaks], deriv[peaks,i],'x')
		#plt.plot(meta.Ekin, deriv[:,i])
		#plt.show()
		band = []
		for peak in peaks:
			A, m, s, B, ydata, xdata = estimate_params_g(meta.Ekin[partE], current, method = 'Max')
			#plt.plot(meta.Ekin, current)
			#plt.plot(meta.Ekin, gaussian_peak(meta.Ekin, A, m, s, B))
			#plt.savefig(str(i)+'_'+str(peak)+'.png')
			#plt.close()
			#plt.show()
			band.append(m)
			current = remove_peak(meta.Ekin[partE], current, m, s)
		EDC.append(band)

	return pd.DataFrame(EDC)

def organise_bands(bands):
	"""
	Exchanges the values inside bands to make them the most continuous possible

	Parameters:
		bands (ndarray):
			Initial bands

	Returns:
		bands (ndarray):
			Reorganised bands
	
	Notes:
		Can struggle with band crossings
	"""
	ex = False
	for i in range(bands.shape[1]) :
		for j in range(1,bands.shape[0]):
			contimatrix = [np.abs(bands[j,i]-bands[j-1,k]) for k in range(bands.shape[1])]
			contimatrix = [u if not(np.isnan(u)) else 1000 for u in contimatrix]
			index = np.argmin(contimatrix)
			
			if np.abs(bands[j,index]-bands[j-1,index])>contimatrix[index]:
				ex = ex or (index != i)
				c = bands[j,i]
				bands[j,i] = bands[j,index]
				bands[j,index] = c
	return bands

def split_band(band):
	"""
	Cuts a band in whenever are NaN values into smaller continuous bands,
	deletes empty bands

	Parameters:
		band (ndarray):

	Returns:
		bands (list):
			list of dataframe containing new bands without discontinuities
	"""
	mask = np.isnan(band).astype(int)+1			#find all the nan values
	firsts, lengths = find_plateaux(mask)		#try to find the length of the nan zones

	lengths.insert(firsts[0],0)
	firsts.insert(0,0)
	bands = []
	for j in range(0,len(firsts)):
		bands.append(np.ones(len(mask))*np.nan)
		bands[-1][firsts[j]:firsts[j]+lengths[j]+1] = band[firsts[j]:firsts[j]+lengths[j]+1]
		#This part splits whenever a Nan zone is attained

	return [k for k in bands if not(np.isnan(k).all())] 	#All the NaN bands are filtered out

def extract_bands(wave, sigma = 3, a = 0.01, lims = [{'tlim':'Full','elim':'Full'}], method = laplace):
	"""


	"""
	data = wave.data
	meta = wave.metadata[0]

	bands = []
	for lim in lims:
		rawbands = edc_mdc_bandscan(wave, sigma, a, tlim = lim['tlim'], elim = lim['elim'], diff = method)
		orgbands = organise_bands(rawbands.values)
		splittedbands = []
		for band in np.transpose(orgbands):
			splittedbands+=split_band(band)
		finalbands = organise_bands(np.transpose(splittedbands))

		if lim['tlim'] == 'Full':
			lim['tlim'] = [min(meta.angle),max(meta.angle)]
		part = (min(lim['tlim'])<=meta.angle)*(meta.angle<=max(lim['tlim']))

		for band in finalbands.T :
			bands += [pd.DataFrame({'band':band,'axis':meta.angle[part]})]

	return bands

def bands_ang_to_k(bands, meta):
	"""
	Change the coordinate system of bands measured on a Ek,angular grid

	Parameters:
		bands (DataFrame):
			columns = 'band' and 'axis', bands in the measurement coordinate system

	Returns:
		bandsk (DataFrame):
			bands in the binding energy, wave vector coordinate system	
	"""
	bandsk = []
	for band in bands:
		E, k = ang_to_k(band['band'],
						band['axis'],
						meta.Rz,
						meta.Rz_offset,
						meta.phi,
						meta.tilt,
						0,
						meta.hv,
						0,
						meta.Ef,
						'simple')
		bandsk += [pd.DataFrame({'band':-E,'axis':k})]

	return bandsk

def fit_bands(bands, deg = 2):
	"""
	Performs a polynomial fit up to order deg of all bands 

	Parameters:
		bands (DataFrame):
			columns = 'band' and 'axis', bands in the measurement coordinate system
		deg (integer):
			order of the fitting polynomial

	Returns:
		fits (DataFrame):
			columns 'band','axis','fit'
		coeffs (ndarray):
			polynomial coefficients in decreasing order (deg, deg-1,...,0)
	"""

	fits = []
	coeffs = []
	deg = int(deg)
	for band in bands :

		reduced_axis = band['axis'][~(np.isnan(band['band']))]			#removes all NaN in the band
		reduced_band = band['band'][~(np.isnan(band['band']))]			#removes all NaN in the axis
		if len(reduced_band) > deg:
			#print(reduced_band)
			try:
				#coeff = np.polyfit(reduced_axis, reduced_band, deg = deg)
				#coeff = constrained_polyfit(reduced_axis, reduced_band, deg, [[4,-np.inf,0],[3,-np.inf,0]])
				f = interp1d(reduced_axis,reduced_band)
				coeff = taylor(f,0, deg, 0.4).coeffs

				result = np.polyval(coeff, reduced_axis)
				fits += [pd.DataFrame({'axis':reduced_axis, 'band':reduced_band, 'fit':result})]
				coeffs += [coeff]
				print(fits[-1])
			except ValueError:
				pass

	return fits, coeffs

def _fit_band_sin(axis, band, *_):

	reduced_axis = axis[~(np.isnan(band))]
	reduced_band = band[~(np.isnan(band))]
	try :
		p, cov = curve_fit(sinf, reduced_axis, reduced_band, p0 = [1/30, 0, 0.8, 40])
		results = sinf(reduced_axis,p[0],p[1],p[2],p[3])
	except (TypeError, RuntimeError):
		p = []
		results = []
		reduced_axis = []
		reduced_band = []

	return p, reduced_axis, results, reduced_band

#----------------------------------------Related to Fermi Surface exploration

def generate_path(init,final,N):
	"""
	Samples point on a path from init (inital point) to the final point (final)
	in N steps

	Parameters:
	 	init (array):
 			a couple of points (x_1,y_1)
		final (array):
 			a couple of points (x_N,y_N)
		N (int):
			Number of steps
		
	Returns:
	 	angles (ndarray):
 			Array containing all intermediate points : [[x1,y1],[x2,y2]...[x_N,y_N]]
	"""
	t = np.linspace(0,1,N)
	angles =  np.tensordot(init,(1-t),axes=0)+np.tensordot(final,t,axes=0)
	return angles.T

def generate_fermi_cut_ang(interpolant, meta, angles):
	new_df = np.array([interpolant((angle[1],meta.Ekin,angle[0])) for angle in angles])
	new_meta = copy.deepcopy(meta)
	new_meta.angle = angles.T[0]
	new_meta.Rz = angles.T[1]
	new_meta.tilt = (angles.T[1][0]+angles.T[1][-1])/2

	pathDist = np.diff(angles.T)
	curvilinDists = np.sqrt(pathDist[0]**2+pathDist[1]**2)
	curvilinPath = np.array([np.sum(curvilinDists[:i]) for i in range(len(curvilinDists)+1)])
	new_meta.angle = curvilinPath

	return new_df.T, new_meta

def generate_fermi_cut(interpolant, meta, kpath):
	new_df = np.array([[interpolant(k3D_to_ang(E,k[0],k[1],Ef=0,tilt=0,Rz_offset=0)) for k in kpath] for E in meta.Ekin])
	new_meta = copy.deepcopy(meta)
	new_meta.Ekin = meta.Ekin #- meta.Ef
	pathDist = np.diff(kpath.T)
	curvilinDists = np.sqrt(pathDist[0]**2+pathDist[1]**2)
	curvilinPath = np.array([np.sum(curvilinDists[:i]) for i in range(len(curvilinDists)+1)])
	new_meta.angle = curvilinPath
	new_meta.angular = False
	new_meta.kinetic = True
	return new_df, new_meta

def make_FS_cut(interpolant, meta, points, slices = 150):
	if len(points) != 2:
		points.append(points[0])

	offset = 0
	#fig2, ax2 = plt.subplots()
	print('List of selected points:')
	print(points)

	cuts = []

	for i in range(1,len(points)):

		print('init: '+repr(points[i-1]))
		print('final: '+repr(points[i]))

		path = generate_path(points[i-1],points[i], slices)
		pathDist = np.diff(path.T)
		curvilinDists = np.sqrt(pathDist[0]**2+pathDist[1]**2)
		curvilinPath = np.array([np.sum(curvilinDists[:i]) for i in range(len(curvilinDists)+1)])

		#print(curvilinPath)

		new_cut, new_meta = generate_fermi_cut(interpolant, meta, path)
		#print(new_cut.shape)
		#print(new_meta.angle.shape)
		#ax2.pcolormesh(curvilinPath+offset,metas[0].Ekin-metas[0].Ef,new_cut)
		cuts.append({'axis':curvilinPath+offset,'spectrum':new_cut, 'offset':offset})
		offset += curvilinPath[-1]
		print(offset)

	return cuts