"""
user_interpret.py

Contains functions interpreting user queries
"""

import re
import numpy as np
from cassiopy.exceptions import *
#------------------------Recognised user queries

QUIT = re.compile('(q|quit)',re.I)
ACCEPT = re.compile('(y|o|yes|oui|haan|han|ji|si|sim)',re.I)
REFUSE = re.compile('(n|no|non|nahin? ?(sir)?)',re.I)
XZ = re.compile('(xz|map|carte)', re.I)
FS = re.compile('(fs|fermi)', re.I)
HV = re.compile('(hv|kz)', re.I)
FERMI = re.compile('(Ef|fermi)', re.I)
SCIENTA = re.compile('(scienta)',re.I)
PEEM = re.compile('(.?peem)',re.I)
SPECS = re.compile('(.?specs|prodigy)',re.I)
MBS = re.compile('(mbs|krx)', re.I)
RAMAN = re.compile('(raman|pl|photolum*)',re.I)
SINGLE = re.compile('(view|single*)', re.I)
MULTI = re.compile('(multi)', re.I)
DETAIL = re.compile('(detail)', re.I)
LOAD = re.compile('(load|charger)', re.I)
CREATE = re.compile('(create|creer|créer)',re.I)
PEAK = re.compile('(peak|pic)', re.I)
EDIT = re.compile('(edit)', re.I)
BAND = re.compile('(band)', re.I)
NORM = re.compile('(norm|nm)', re.I)
DICHRO = re.compile('(dichro)', re.I)
EXTEND = re.compile('(extend|merge|fusion)',re.I)
SAVE = re.compile('(save|sauvegarde)', re.I)
HELP = re.compile('(help|aide)', re.I)

#---------------------------------Input functions and interpretation

def input_float(string, cast_function = float):
	try :
		number = cast_function(input_safe(string))
	except :
		print('\tInput is not a number')
		number = False
	return number

def input_safe(string):
	query = input(string)
	if QUIT.findall(query):
		raise UserQuittingError
	return query

def to_indexlist(query):
	"""
	Transforms a query into a list of indexes according to a regular expression.
	
	Parameters:
		query (str):
			Query to be transformed

	Returns:
		indexlist (list):
			Corresponding list of index

	Examples:
		```
		to_indexlist('1,2, 5:8')
		> [1,2,5,6,7,8]
		```
	"""
	LISTS = re.compile('(\\d+)(?=[ ,;]|$)|(\\d+):(\\d+)')
	indexes = LISTS.findall(query)
	indexlist = []
	for i in indexes :
		if i[0]!= '':
			indexlist.append(int(i[0]))
		else :
			indexlist+=list(range(int(i[1]), int(i[2])+1))
	return indexlist

def collect_entry(metadata, keys):
	return {key:getattr(metadata, key) for key in keys}

def input_entry(entries, size = 1):
	expr0 = re.compile('([a-z_]+ ?[= ]+ ?[-0-9.,a-z/\\_:+]+)', re.I)
	expr1 = re.compile(' ?[= ]+ ?')
	expr_incr = re.compile('([-.0-9]+):([-.0-9]+)\+\+')

	keys = list(entries.keys())
	changed_keys = []

	print('Modifiable attributes :')
	labels = keys
	for label_0, label_1, label_2 in zip(labels[0::3], labels[1::3], labels[2::3]) :
		print('\t- '+label_0+', '+label_1+', '+label_2)
	
	query = input_safe('Please enter edits... (Syntax: attribute1 = value1 attribute2 = value2 attribute3 = value3:step++...)\n')
	edits =expr0.findall(query)

	for edit in edits:
		a = expr1.split(edit)
		for key in keys :
			expr2 = re.compile(key, re.I)
			if expr2.match(a[0]):
				changed_keys.append(key)
				if expr_incr.match(a[-1]):
					match = expr_incr.match(a[-1])
					init = float(match[1])
					step = float(match[2])
					end = init + size*step
					entries[key] = list(np.arange(init, end, step))
				else:
					try:
						entries[key] = float(a[-1])
					except ValueError:
						entries[key] = a[-1].replace('/n','\n')
					print('	Matched '+key+ ' with '+a[0]+' with value '+a[-1])

	return entries, changed_keys