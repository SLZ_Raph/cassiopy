"""
CASSIOpy - Release 02/10/2024
Raphael Salazar

CASSIOpy.py
"""

import sys
#sys.path.insert(1, './src/')

from cassiopy.UI import *

PROGRAM_NAME = 'CASSIOpy'
VERSION = '8.1.1'
GIT_ADRESS = 'https://gitlab.com/SLZ_Raph/cassiopy/'
LOGO ="""
:::::::::::::::::::::::::::::::::::::::::::::::::
::███::███:::███::███:::::███::::::::::::::::::::
:█::::█:::█:█:::██:::█:::█:::█*::::::::::::::::::
:█::::█:::█:█::::█:::::█:█:::█:::██::█::█::::::::
:█::::█:::█::███::███::█:█:::█::█::█::█:█::::::::
:█::::█████:::::█::::█:█:█:::█::█::█::█:█::::::::
:█::::█:::█:█:::██:::█:█:█:::█::███::::██::::::::
::███:█:::█::███::███::█::███:::█:::::::█::::::::
::::::::::::::::::::::::::::::::█:█:::::█::::::::
::::::::::::::::::::::::::::::::█,,██████::::::::
"""

#------------------------Related to initialisation

params = {'path':'./examples/',
		  'mode':'singles',
		  'flatpath' : './src/cassiopy/flatfield/MCP_2022_05_02.tif'}

for i in range(1,len(sys.argv)) :
	keys = list(params.keys())
	params[keys[i-1]] = sys.argv[i]

path = params['path']
if params['path'][-1] != '/' and not(os.path.isfile(params['path'])):
	params['path']+='/'


#------------------------Related to start

splash_screen(PROGRAM_NAME, VERSION, GIT_ADRESS, LOGO)

#------------------------Loading of data

print('PATH:\n\t'+params['path'])
print('MODE:\n\t'+params['mode'])

subprogram_start('Loading files')

try :

	# if os.path.exists(params['path']): and os.path.isdir(params['path']):
	# 	experiments = [load_exp(params, params)]
	# 	exp = experiments[0]
	# elif os.path.exists(params['path'] and os.path.isfile(params['path'])): 		 \
		#  and not(os.path.isdir(params['path'][:-1])) \
		#  and 										 \
		# (   RAMAN.match(params['mode'])				 \
		#  or SPECS.match(params['mode'])				 \
		#  or MBS.match(  params['mode'])
		#  or SCIENTA.match(params['mode'])):			 \

		#params['path'] = params['path'][:-1]
	if os.path.exists(params['path']):
		experiments = [load_exp(params, params)]
		exp = experiments[0]
	else :
		print('Path error, does not exist or not a folder')

		print_c('Back to menu',' ')
		print_c('_','_')
		print('\n')

		experiments = []
		exp = None
#------------------------Analysis
	query = ''
	modified = False

	while not(QUIT.findall(query)):
		if FS.findall(query) :
			subprogram_start('Fermi surface mode')
			fermi_mode(exp)
		if HV.findall(query) :
			subprogram_start('hv mode')
			hv_mode(exp, modified)
			modified = True
		if XZ.findall(query):
			subprogram_start('XZ map mode')
			map_mode(exp)
		if SINGLE.findall(query):
			subprogram_start('View mode')
			view_mode(exp)
		if MULTI.findall(query):
			subprogram_start('View (multi) mode')
			multiview_mode(exp)
		if DICHRO.findall(query):
			subprogram_start('Dichroism mode')
			dichroism_mode(exp)
		if EXTEND.findall(query):
			subprogram_start('Merge mode')
			extended_mapping_mode(exp)
		if DETAIL.findall(query):
			subprogram_start('Detailed data mode')
			detail_view_mode(exp)
		if NORM.findall(query):
			subprogram_start('Normalise spectrum mode')
			normalise_mode(exp)
		if PEAK.findall(query):
			subprogram_start('Peak count mode')
			peak_mode(exp)
		if BAND.findall(query):
			subprogram_start('Band structure extraction mode')
			band_mode(exp)
		if EDIT.findall(query):
			subprogram_start('Experiment metadata edit mode')
			edit_mode(exp)
		if SAVE.findall(query):
			subprogram_start('Experiment save mode')
			save_mode(exp)
		if LOAD.findall(query):
			subprogram_start('Experiment load mode')
			exp, experiments, params = load_mode(exp, experiments, params)
		if CREATE.findall(query):
			subprogram_start('Experiment creation mode')
			exp, experiments, params = create_exp(exp, experiments, params)
		if HELP.findall(query):
			subprogram_start('Help mode')
			help()

		print_c('Available functions\n')
		#print_c('quit map fermi/hv/view/norm/dichro/extend/peak/band/load/edit/save/help')
		print_c('||  view  * multi *   hv   * fermi  * map  ||')
		print_c('||  edit  * norm  * dichro * merge  * band ||')
		print_c('|| detail * load  *  save  * create * help ||')
		print_c('||        *       *  quit  *        *      ||')
		query = input_safe('\nWhat is next ? ')
except (KeyboardInterrupt, UserQuittingError):
	print('\n')
	print_c('Program terminated')
	#sys.exit()
