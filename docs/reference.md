## Project Overview

This section of the documentation is dedicated to the technical implementation
of the methods in Cassiopy package.

It is organized by alphabetical order because the namespace is a mess where too many functions are public. This should change, hopefully soon.

:::cassiopy.UI