This site contains the project documentation for the
`Cassiopy`, a package and suite for photoemission data analysis.

## Table Of Contents

The documentation consists of four separate parts:

1. [Tutorials](tutorials.md)
2. [How-To Guides](how-to-guides.md)
3. [Reference](reference.md)
4. [Explanation](explanation.md)

Quickly find what you're looking for depending on
your use case by looking at the different pages.

## Project Overview

