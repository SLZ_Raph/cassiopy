﻿//Title  Load_Cassiopy_txt
//Raphael Salazar

#pragma TextEncoding = "UTF-8"
#pragma rtGlobals=3     // Use modern global access method and strict wave access.

// ReadTextFile(pathName, filePath, outputWaveName, maxNumberOfLines)
// Reads all of the lines of text from the file into a new tet wave.
// Returns 0 if OK or a non-zero error code.
// Read the comments for details.
Function Load_Cassiopy_txt()

 //File loading section--------------------------------------------
 
    String pathName = ""         // Symbolic path name or "" if filePath is full path. Can also be "" if filePath is "" to get Open File dialog.
    String filePath = ""             // File name, partial path or full path or "" to get Open File dialog.
    String outputWaveName = "new_wave"          // NOTE: Overwrites existing wave, if any!
   
    Variable refNum
    Variable maxNumberOfLines = 10000   //Reasonably high number
    Variable err = 0
   
    if (strlen(filePath) == 0)        // Want Open File dialog?
        Open/D/R/P=$pathName refNum as filePath
        if (strlen(S_fileName) == 0)
            return -1              // User canceled.
        endif
        filePath = S_fileName      // This is a full path.
    endif
   
    Open/R/P=$pathName refNum as filePath
    if (GetRTError(1))
        return -2                  // Error. Probably file does not exist.
    endif
   
    Make/O/T/N=(maxNumberOfLines) $outputWaveName
    if (GetRTError(1))
        Close refNum
        return -3                  // Error. Probably there is already a numeric wave with this name.
    endif
    
    Variable NbBranchesPath = 0
    NbBranchesPath = ItemsInList(S_fileName, ":")
    String OutputName = Stringfromlist(NbBranchesPath-1,S_fileName,":")
    
   //File parsing--------------------------------------------
  
    String dataline, point
    Variable NbCol = 0, NbRow = 0
    Variable i = 0, j = 0
    Wave/T textwave = $outputWaveName
    
    //Ignoring the first two lines
    FReadLine refNum, dataline  
    FReadLine refNum, dataline

    //Counting number of columns
    FReadLine refNum, dataline
    NbCol = ItemsInList(dataline, "\t")
    Make/O/N=(NbCol-2)/D axis_0   //first axis
    for(j=2;j<NbCol;j+=1)
        point=Stringfromlist(j,dataline,"\t")
        axis_0[j-2]=str2num(point)
    endfor
    NbCol -= 2
    
    //Counting number of rows
    do
        FReadLine refNum, dataline    // Terminates on CR, LF or CRLF.
        textwave[NbRow] = dataline
        NbRow += 1
    while (strlen(dataline) != 0)   
    NbRow -= 1
    Close refNum
    
    Redimension/N=(NbRow) textwave
    
    Make/O/N=(NbRow)/D axis_1
    Make/O/N=(NbCol,NbRow)/D Wave_out
    Make/O/N=(NbCol)/T current_line
    
    for(i=0;i<NbRow;i+=1)              //Putting values into the final waves
        for(j=0;j<NbCol;j+=1)
             current_line[j]= Stringfromlist(j,textwave[i],"\t")
        endfor
        axis_1[i] = str2num(current_line[1])
        
        for(j=0;j<NbCol-2;j+=1)   
            point=current_line[j+2]
            Wave_out[j+2][i]=str2num(point)
        endfor
    endfor
    
    SetScale/I x axis_0[0],axis_0[numpnts(axis_0) - 1],"", Wave_out
    SetScale/I y axis_1[0],axis_1[numpnts(axis_1) - 1],"", Wave_out

    Rename Wave_out, $OutputName
    
    KillWaves $outputWaveName, axis_1, axis_0, current_line
      
    return err                    // 0=success
End