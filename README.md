<!-- PROJECT LOGO -->
<br />
<div align="center">
  <a>
    <img src="cassiopy_logo.png" alt="Logo" width="80" height="80">
  </a>

  <h1 align="center">CASSIOPY</h1>
General purpose angular resolved photoelectron photoemission spectroscopy (ARPES) analysis tool
</div>

## Table of Contents
1. [General Info](## General Info)
2. [Installation](## Installation)
3. [Usage](## Usage)
4. [Future developments](## Future developments)
5. [Acknowledgements](## Acknowledgement)

## General Info

Cassiopy is a  Python package dedicated to batch treatment and analysis of multidimensional ARPES data. It supports a **mixed command line/GUI** software for quick analysis in beamtime conditions. 

It uses state of the art techniques to take a maximum advantage of beamtimes and the data obtained by electron spectroscopies techniques in synchrotron facilities or laboratory. The architecture of Cassiopy is made to accomodate easily new parsers/loaders from the numerous detectors in current usage. The analysis methods range from basic image analysis with standard filters to more complex processing schemes.

Initially optimized for the exploitation of data from the [Cassiopee beamline](https://www.synchrotron-soleil.fr/en/beamlines/cassiopee) (Synchrotron Soleil) the aim is to have the broadest compatibility with other beamlines. This can prove difficult since the ARPES community has yet to decide on a common file format and each experimental station has its own specificities.  


## Installation

### Requirements

:warning: *The requirements are automatically installed by pip.*

Required packages:
  -  matplotlib  (< 3.9.0 for Windows, any for Linux)
  -  numpy
  -  pandas
  -  scipy
  -  psutil
  -  PyQt5
  -  igor2

Optional packages:
  -  [krxloader](https://gitlab.com/SLZ_Raph/krxloader) (necessary to load .krx files)
  -  h5py (necessary to load .nxs files, not supported yet)

### Linux (Ubuntu)

For local installation:
```console
cd /path/to/virtual/environment/
python -m venv ./                                     #optional only for virtual environment
git clone https://gitlab.com/SLZ_Raph/cassiopy.git    #optional only for virtual environment
python ./bin/activate                                 #optional only for virtual environment
cd ./cassiopy-main/
pip install -e .
```
Cassiopy will be installed in the folder you extracted it

or

For "site-package" installation:
```console
cd /path/to/virtual/environment/ #optional only for virtual environment
python -m venv ./                #optional only for virtual environment
./bin/activate                   #optional only for virtual environment
pip install git+https://gitlab.com/SLZ_Raph/cassiopy.git
```
Cassiopy will be installed in "lib/python3.XX/site-packages".
### Windows

Make sure that Python is installed, otherwise get it from [the official website](https://www.python.org/downloads/windows/).

Install Python 3.11 or below.

Download the [source code](https://gitlab.com/SLZ_Raph/cassiopy/-/archive/main/cassiopy-main.zip) and save it to your desired location. Extract the archive.

```console
cd /path/to/installation/folder/cassiopy-main/
python -m venv ./   #optional only for virtual environment
./Scripts/activate  #optional only for virtual environment
pip install -e .
```

### Anaconda

## Usage

### User Manual :book:
The detailed operation manual Cassiopy is available in english [manual (en)](Cassiopy_Manuel.pdf).

### Documentation
The documentation of the code can be found in the reference section of Cassiopy's [webpage](https://slz_raph.gitlab.io/cassiopy/).

### Compatible beamlines

| Facility/Institute               | Beamline/Laboratory | Branch     | Analyzer          | File format | 3D compatible format | Supported |
|----------------------------------|---------------------|------------|-------------------|-------------|---------------|-----------|
| Synchrotron SOLEIL               | [Cassiopee](https://www.synchrotron-soleil.fr/en/beamlines/cassiopee)           | ARPES      | Scienta R4000     | .txt        | yes           | yes       |
| Synchrotron SOLEIL               | [Cassiopee](https://www.synchrotron-soleil.fr/en/beamlines/cassiopee)           | Spin-ARPES | MBS A1            | .krx        | yes           | yes       |
| Synchrotron SOLEIL               | [Antares](https://www.synchrotron-soleil.fr/en/beamlines/antares)             | ARPES      | MBS A1            | .nxs        | yes           | no        |
| Synchrotron SOLEIL               | [Tempo](https://www.synchrotron-soleil.fr/en/beamlines/tempo)               | ARPES      | MBS A1            | .krx        | yes           | yes       |
| SOLARIS                          | [URANOS](https://synchrotron.uj.edu.pl/en_GB/linie-badawcze/uranos)              | ARPES      | Scienta DA30-L    | .txt, .ibw  | yes           | yes       |
| MAX IV                           | [Bloch](https://www.maxiv.lu.se/beamlines-accelerators/beamlines/bloch/)               | ARPES      | Scienta DA30-L    | -           | -             |-        |
| MAX IV                           | [Bloch](https://www.maxiv.lu.se/beamlines-accelerators/beamlines/bloch/)                | Spin-ARPES | SPECS Phoibos 150 | .itx        | yes           | yes       |
| NTC - University of West Bohemia | [RAM - SARPES Lab](https://www.ntc.zcu.cz/en/Research/Research_topics/Advanced_Materials.html)    | Spin-ARPES | SPECS Phoibos 150 | .itx        | yes           | yes       |
| NTC - University of West Bohemia | [RAM](https://www.ntc.zcu.cz/en/Research/Research_topics/Advanced_Materials.html)    | Calculation one-step model | [SPRKKR](https://www.ebert.cup.uni-muenchen.de/old/index.php?option=com_content&view=article&id=8%3Asprkkr&catid=4%3Asoftware&Itemid=7&lang=en) | .spc        | no           | yes       |
| Attolab                          | FAB10               | Spin-ARPES | SPECS Phoibos 150 | .itx        | yes           | yes       |
| Elettra Sincrotrone              | [BaDElPh](https://www.elettra.eu/elettra-beamlines/badelph.html)             | ARPES      | MBS 1A | .krx       | yes            | yes       |
| Elettra Sincrotrone              | [NanoESCA](https://www.elettra.eu/elettra-beamlines/nanoesca.html)            | PEEM       | Focus NanoESCA  | .tif        | no            | yes       |
| CEA Saclay                       | [LENSIS](https://iramis.cea.fr/spec/lensis/)              | PEEM       | Focus NanoESCA  | .tif        | no            | yes       |


### Example viewer

<img src="imgs/Example1.gif"  width="500" height="600">

### Example 3D viewer

<img src="imgs/Example2.gif"  width="500" height="600">

## Future developments

- ![70%](https://progress-bar.xyz/70) Machine learning tools  :bar_chart: :chart_with_upwards_trend:
- ![60%](https://progress-bar.xyz/60) Spin-ARPES procedures integration :arrow_up: :arrow_down:
- ![50%](https://progress-bar.xyz/50) Consistent scripting with Cassiopy :bookmark_tabs:
- ![50%](https://progress-bar.xyz/50) Documentation and tutorials 🎯 :muscle:
- ![0%](https://progress-bar.xyz/00) PyQT interface :computer: :sparkles:
- ![0%](https://progress-bar.xyz/00) Jupyter Lab compatibility :ledger: :pencil2:

## Publications



## Acknowledgement

The author acknowledge funding from ANR “CORNFLAKE” (ANR-18-CE24-0015-01).

This work was supported by the project TWISTnSHINE, funded as project No. LL2314 by Programme ERC CZ.

<img src="imgs/anrlogo.png"  width="160" height="120">
<img src="imgs/uelogo.png"  width="320" height="120">
